<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */

$routes[] = [
'method' => 'post', 
'route' => '/ping', 
'handler' => ['Controllers\ExampleController', 'pingAction']
];


$routes[] = [
'method' => 'post', 
'route' => '/test/{id}', 
'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
'method' => 'post', 
'route' => '/skip/{name}', 
'handler' => ['Controllers\ExampleController', 'skipAction'],
'authentication' => FALSE
];


// $routes[] = [
//     'method' => 'post',
//     'route' => '/map/marker/upload',
//     'handler' => ['Controllers\MapController', 'uploadPicsAction'],
//     'authentication' => FALSE
// ];

// $routes[] = [
//     'method' => 'post',
//     'route' => '/map/create/info',
//     'handler' => ['Controllers\MapController', 'saveMapMarkerAction'],
//     'authentication' => FALSE
// ];



//rainier routers

/**
PAGES
 */

$routes[] = [
'method' => 'post',
'route' => '/pages/saveimage/{filename}',
'handler' => ['Controllers\PagesController', 'saveimageAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/pages/listimages',
'handler' => ['Controllers\PagesController', 'listimageAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post',
'route' => '/pages/create',
'handler' => ['Controllers\PagesController', 'createPagesAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/pages/managepage/{num}/{off}/{keyword}', 
'handler' => ['Controllers\PagesController', 'managePagesAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post',
'route' => '/pages/updatepagestatus/{status}/{pageid}/{keyword}',
'handler' => ['Controllers\PagesController', 'pageUpdatestatusAction'],
'authentication' => FALSE
];


$routes[] = [
'method' => 'get',
'route' => '/page/pagedelete/{pageid}',
'handler' => ['Controllers\PagesController', 'pagedeleteAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/page/pageedit/{pageid}',
'handler' => ['Controllers\PagesController', 'pageeditoAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post',
'route' => '/pages/saveeditedpage',
'handler' => ['Controllers\PagesController', 'saveeditedPagesAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/page/getpage/{pageslugs}',
'handler' => ['Controllers\PagesController', 'getPageAction'],
'authentication' => FALSE
];


$routes[] = [
'method' => 'get',
'route' => '/menu/main',
'handler' => ['Controllers\MenuController', 'menuMainAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/menu/sub/{id}',
'handler' => ['Controllers\MenuController', 'showSubAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post',
'route' => '/upload/banner',
'handler' => ['Controllers\PagesController', 'uploadBannerAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/list/banner',
'handler' => ['Controllers\PagesController', 'listimageAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/pages/changestatus/{id}/{status}',
'handler' => ['Controllers\PagesController', 'changestatusAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/slug/title/{slug}',
'handler' => ['Controllers\MenuController', 'parentTitleAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/kaslaawan/titlepage/{slug}',
'handler' => ['Controllers\MenuController', 'tittlesubPagesAction'],
'authentication' => FALSE
];


/**
PAGES FRONT END
 */
$routes[] = [
'method' => 'get',
'route' => '/parent/menu',
'handler' => ['Controllers\MenuController', 'menuMainAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/submenu/menu/{slug}',
'handler' => ['Controllers\MenuController', 'menuSubAction'],
'authentication' => FALSE
];


$routes[] = [
'method' => 'get',
'route' => '/pbe/frontend/{slug}/{pages}/{others}',
'handler' => ['Controllers\MenuController', 'viewPagesAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/pbe/frontend_subpag/{slug}/{pages}/{others}',
'handler' => ['Controllers\MenuController', 'viewsubPagesAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/main/redirect/{slug}',
'handler' => ['Controllers\MenuController', 'checkpagesAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/sub/redirect/{sub}',
'handler' => ['Controllers\MenuController', 'checksubAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/slug/pagestitle/{slug}',
'handler' => ['Controllers\MenuController', 'tittlesubPagesAction'],
'authentication' => FALSE
];

/**
NEWS
 */

$routes[] = [
'method' => 'post',
'route' => '/news/saveimage/{filename}',
'handler' => ['Controllers\NewsController', 'saveimageAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/news/listimages',
'handler' => ['Controllers\NewsController', 'listimageAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/news/listcategory',
'handler' => ['Controllers\NewsController', 'listcategoryAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post',
'route' => '/news/create',
'handler' => ['Controllers\NewsController', 'createNewsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post',
'route' => '/newscenter/create',
'handler' => ['Controllers\NewsController', 'createNewsCenterAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/option/center',
'handler' => ['Controllers\CenterController', 'centeroptionAction'],
'authentication' => FALSE
];


/**
 USERS
 */
 /* Validation for username exist */
 $routes[] = [
 'method' => 'get',
 'route' => '/validate/username/{name}',
 'handler' => ['Controllers\UserController', 'userExistAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/validate/eusername/{name}',
 'handler' => ['Controllers\UserController', 'EuserExistAction'],
 'authentication' => FALSE
 ];
 /* Validation for email exist */
 $routes[] = [
 'method' => 'get',
 'route' => '/validate/useremail/{email}',
 'handler' => ['Controllers\UserController', 'emailExistAction'],
 'authentication' => FALSE
 ];
 /* Submit User Regester */

 $routes[] = [
 'method' => 'post',
 'route' => '/cmsuser/register',
 'handler' => ['Controllers\UserController', 'cmsRegisterUserAction'],
 'authentication' => FALSE
 ];

 $routes[] = [
 'method' => 'post',
 'route' => '/user/register',
 'handler' => ['Controllers\UserController', 'registerUserAction'],
 'authentication' => FALSE
 ];

 /* List all User */
 $routes[] = [
 'method' => 'get',
 'route' => '/user/list/{num}/{off}/{keyword}',
 'handler' => ['Controllers\UserController', 'userListAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/user/listadmin/{num}/{off}/{keyword}',
 'handler' => ['Controllers\UserController', 'userListAdminAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/user/listmanager/{num}/{off}/{keyword}',
 'handler' => ['Controllers\UserController', 'userListManagerAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/userstudent/list/{num}/{off}/{keyword}',
 'handler' => ['Controllers\UserController', 'userstudentListAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/userteacher/list/{num}/{off}/{keyword}/{usertype}',
 'handler' => ['Controllers\UserController', 'userteacherListAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/usermentor/list/{num}/{off}/{keyword}/{usertype}',
 'handler' => ['Controllers\UserController', 'userteacherListAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/pending/list/{num}/{off}/{keyword}',
 'handler' => ['Controllers\UserController', 'userPendingListAction'],
 'authentication' => FALSE
 ];


 $routes[] = [
 'method' => 'get',
 'route' => '/addedusers/list/{num}/{off}/{keyword}',
 'handler' => ['Controllers\UserController', 'userNewaddedListAction'],
 'authentication' => FALSE
 ];


 $routes[] = [
 'method' => 'get',
 'route' => '/listall/list/{num}/{off}/{keyword}',
 'handler' => ['Controllers\UserController', 'ListAllEUserAction'],
 'authentication' => FALSE
 ];
 /* User Info */
 $routes[] = [
 'method' => 'get',
 'route' => '/user/info/{id}',
 'handler' => ['Controllers\UserController', 'userInfoction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'post',
 'route' => '/user/update',
 'handler' => ['Controllers\UserController', 'userUpdateAction'],
 'authentication' => FALSE
 ];
 /* DELETE User */
 $routes[] = [
 'method' => 'get',
 'route' => '/user/delete/{id}',
 'handler' => ['Controllers\UserController', 'deleteUserAction'],
 'authentication' => FALSE
 ];

 $routes[] = [
 'method' => 'post',
 'route' => '/user/activation',
 'handler' => ['Controllers\UserController', 'activationAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/user/login/{username}/{password}',
 'handler' => ['Controllers\UserController', 'loginAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/euser/elogin/{username}/{password}',
 'handler' => ['Controllers\UserController', 'eloginAction'],
 'authentication' => FALSE
 ];



 $routes[] = [
 'method' => 'get',
 'route' => '/user/signin/{username}/{password}',
 'handler' => ['Controllers\UserController', 'signinAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/center/info/{userid}',
 'handler' => ['Controllers\UserController', 'centerinfoAction'],
 'authentication' => FALSE
 ];

 $routes[] = [
 'method' => 'get',
 'route' => '/user/changestatus/{id}/{status}',
 'handler' => ['Controllers\UserController', 'changestatusAction'],
 'authentication' => FALSE
 ];

//SIMULA NG ROTA ng FORGOT PASSWORD
 $routes[] = [
 'method' => 'post', 
 'route' => '/forgotpassword/send/{email}', 
 'handler' => ['Controllers\UserController', 'forgotpasswordAction'],
 'authentication' => FALSE
 ];

 $routes[] = [
 'method' => 'post', 
 'route' => '/checktoken/check/{email}/{token}', 
 'handler' => ['Controllers\UserController', 'checktokenAction'],
 'authentication' => FALSE
 ];

 $routes[] = [
 'method' => 'post', 
 'route' => '/updatepassword/token', 
 'handler' => ['Controllers\UserController', 'updatepasswordtokenAction'],
 'authentication' => FALSE
 ];

// E Learning Community
 $routes[] = [
 'method' => 'post', 
 'route' => '/confrim/user', 
 'handler' => ['Controllers\EuserController', 'confirmAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'post', 
 'route' => '/complete/profile', 
 'handler' => ['Controllers\EuserController', 'userUpdateAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/user/verify/{userid}/{code}',
 'handler' => ['Controllers\EuserController', 'verifyAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'post', 
 'route' => '/usrupdate/profile', 
 'handler' => ['Controllers\EuserController', 'EuserUpdateAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'post', 
 'route' => '/chngepass/profile', 
 'handler' => ['Controllers\EuserController', 'chngePassAction'],
 'authentication' => FALSE
 ];

 $routes[] = [
 'method' => 'get', 
 'route' => '/list/school', 
 'handler' => ['Controllers\EuserController', 'listSchoolAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/user/log/{userid}',
 'handler' => ['Controllers\EuserController', 'loguserAction'],
 'authentication' => FALSE
 ];

 $routes[] = [
 'method' => 'get',
 'route' => '/euser/login/{userid}',
 'handler' => ['Controllers\EuserController', 'loginUserAction'],
 'authentication' => FALSE
 ];
 $routes[] = [
 'method' => 'get',
 'route' => '/euser/logout/{userid}',
 'handler' => ['Controllers\EuserController', 'logoutUserAction'],
 'authentication' => FALSE
 ];
/** 
TESTIMONIALS > Ryanjeric
 */

$routes[] = [
'method' => 'post',
'route' => '/createtestimonials/testimonialsave',
'handler' => ['Controllers\TestimonialsController', 'savetestimonialAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/manage/list/{num}/{off}/{keyword}',
'handler' => ['Controllers\TestimonialsController', 'listtestimonialsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/manage/delete/{id}',
'handler' => ['Controllers\TestimonialsController', 'testimonialdeleteAction'],
'authentication' => FALSE
];



//IMAGE UPLOAD AND LIST
$routes[] = [
'method' => 'post',
'route' => '/createtestimonials/saveimage',
'handler' => ['Controllers\TestimonialsController', 'saveimageAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/createtestimonials/listimages',
'handler' => ['Controllers\TestimonialsController', 'listimageAction'],
'authentication' => FALSE
];
//END OF IMAGE UPLOAD AND LIST

//EDIT TESTIMONIAL
$routes[] = [
'method' => 'get',
'route' => '/manage/edit/{edittestimonialid}',
'handler' => ['Controllers\TestimonialsController', 'testimonialeditAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post',
'route' => '/manage/update',
'handler' => ['Controllers\TestimonialsController', 'updatetestimonialAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/testimonials/changestatus/{id}/{status}',
'handler' => ['Controllers\TestimonialsController', 'changestatusAction'],
'authentication' => FALSE
];


//START OF FEATURED TESTIMONIALS ROUTES//

$routes[] = [
'method' => 'get',
'route' => '/fe/testimoniallist/pbs',
'handler' => ['Controllers\TestimonialsController', 'pbstestimonialsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/fe/testimoniallist/foradults',
'handler' => ['Controllers\TestimonialsController', 'foradultstestimonialsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/fe/testimoniallist/forfamilies',
'handler' => ['Controllers\TestimonialsController', 'forfamiliestestimonialsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/fe/testimoniallist/featuredforadults',
'handler' => ['Controllers\TestimonialsController', 'foradultsfeaturedtestimonialsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/fe/testimoniallist/featuredforfamilies',
'handler' => ['Controllers\TestimonialsController', 'forfamiliesfeaturedtestimonialsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/fe/testimoniallist/featured',
'handler' => ['Controllers\TestimonialsController', 'featuredtestimonialsAction'],
'authentication' => FALSE
];
//END OF FEATURED TESTIMONIALS ROUTES//

/////END OF ALL TESTIMONIALS ROUTES///

/** 
Center PAges
 */

$routes[] = [
'method' => 'get',
'route' => '/getuser/userlist',
'handler' => ['Controllers\CenterController', 'userListAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/state/list',
'handler' => ['Controllers\CenterController', 'stateListAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/city/info/{state}',
'handler' => ['Controllers\CenterController', 'centerInfoAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post',
'route' => '/centerpage/uploadbanner',
'handler' => ['Controllers\CenterController', 'uploadcenterBannerAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/centerbanner/bannerlist',
'handler' => ['Controllers\CenterController', 'centerbannerAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post',

'route' => '/save/center',
'handler' => ['Controllers\CenterController', 'savecenterAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/validate/title/{centername}',
'handler' => ['Controllers\CenterController', 'validateTitleAction'],
'authentication' => FALSE
];



$routes[] = [
'method' => 'get', 
'route' => '/centers/list/{num}/{off}/{keyword}', 
'handler' => ['Controllers\CenterController', 'manageCenterAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/fecenters/list/{num}/{off}', 
'handler' => ['Controllers\CenterController', 'felistcenterAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/center/delete/{pageid}',
'handler' => ['Controllers\CenterController', 'centerDeleteAction'],
'authentication' => FALSE
];


$routes[] = [
'method' => 'get',
'route' => '/center/infos/{centerid}',
'handler' => ['Controllers\CenterController', 'centerdataAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post',
'route' => '/update/center',
'handler' => ['Controllers\CenterController', 'updatecenterAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post',
'route' => '/update/mycenter',
'handler' => ['Controllers\CenterController', 'updatemycenterAction'],
'authentication' => FALSE
];


$routes[] = [
'method' => 'get',
'route' => '/fecenter/getinfo/{slug}',
'handler' => ['Controllers\CenterController', 'feInfocenterAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/fecenter/manager/{userid}',
'handler' => ['Controllers\CenterController', 'centerManagerAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/center/changestatus/{id}/{status}',
'handler' => ['Controllers\CenterController', 'changestatusAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/centers/eventlist', 
'handler' => ['Controllers\CenterController', 'eventcenterCenterAction'],
'authentication' => FALSE
];
/** 
Center Slider
 */
$routes[] = [
'method' => 'get',
'route' => '/centerpage/slider/{path}/{centerid}',
'handler' => ['Controllers\CenterController', 'uploadcenterSliderAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/centerbanner/sliderlist/{centerid}',
'handler' => ['Controllers\CenterController', 'centersliderAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/slider/delete/{imgid}',
'handler' => ['Controllers\CenterController', 'deletesliderAction'],
'authentication' => FALSE
];
/** 
Center Director
 */
$routes[] = [
'method' => 'post',
'route' => '/center/director',
'handler' => ['Controllers\DirectorController', 'dirMsgAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/director/message/{centerid}',
'handler' => ['Controllers\DirectorController', 'getMsgAction'],
'authentication' => FALSE
];
/** 
Center Events
 */
$routes[] = [
'method' => 'post', 
'route' => '/center/event',
'handler' => ['Controllers\CentereventController', 'addeventAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/calendar/listview', 
'handler' => ['Controllers\CentereventController', 'listviewAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/calendar/manageactivity', 
'handler' => ['Controllers\CentereventController', 'viewcalendarAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/fecalendar/manageactivity/{centerids}', 
'handler' => ['Controllers\CentereventController', 'centercalendarAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/calendar/editactivity/{eventid}',
'handler' => ['Controllers\CentereventController', 'activityinfoAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post', 
'route' => '/calendar/updateactivity', 
'handler' => ['Controllers\CentereventController', 'activityUpdateAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/activity/delete/{id}',
'handler' => ['Controllers\CentereventController', 'activitydeleteAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/globalevent/list/{num}/{off}/{keyword}', 
'handler' => ['Controllers\CentereventController', 'eventlistCenterAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/update/eventlocation/{lat}/{lon}/{eventid}',
'handler' => ['Controllers\CentereventController', 'eventlocAction'],
'authentication' => FALSE
];

/** 
Center Maps
 */
$routes[] = [
'method' => 'get', 
'route' => '/save/map/{lat}/{lon}/{centerid}',
'handler' => ['Controllers\CentermapController', 'centerlocAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/get/map/{centerid}',
'handler' => ['Controllers\CentermapController', 'getlocAction'],
'authentication' => FALSE
];
/** 
 NEWS AND EVENTS RSS
 */
 $routes[] = [
 'method' => 'get', 
 'route' => '/latest/rss',
 'handler' => ['Controllers\RSSController', 'getrssAction'],
 'authentication' => FALSE
 ];
/** 
Center News
 */

$routes[] = [
'method' => 'get',
'route' => '/centervalidate/title/{title}',
'handler' => ['Controllers\CenternewsController', 'Validatenewstitlesction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post',
'route' => '/center/news',
'handler' => ['Controllers\CenternewsController', 'centerNewsction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/centers/news/{num}/{off}/{keyword}/{centerid}', 
'handler' => ['Controllers\CenternewsController', 'manageNewsAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/centerNews/delete/{newsid}',
'handler' => ['Controllers\CenternewsController', 'newsDeleteAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/centerNews/edit/{newsid}',
'handler' => ['Controllers\CenternewsController', 'newseditAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post',
'route' => '/news/update',
'handler' => ['Controllers\CenternewsController', 'centerupdatection'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/fe/news/{num}/{off}/{keyword}/{centerid}', 
'handler' => ['Controllers\CenternewsController', 'feNewsAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/author/news/{offset}/{page}/{authorid}', 
'handler' => ['Controllers\CenternewsController', 'authorNewsAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/list/author',
'handler' => ['Controllers\CenternewsController', 'listauthorAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/list/center',
'handler' => ['Controllers\CenternewsController', 'listcenterAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/read/news/{slugnews}',
'handler' => ['Controllers\CenternewsController', 'readnewsAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/centernews/changestatus/{id}/{status}',
'handler' => ['Controllers\CenternewsController', 'changestatusAction'],
'authentication' => FALSE
];
/** 
Center Featured Banner
 */
$routes[] = [
'method' => 'post',
'route' => '/news/featuredbanner',
'handler' => ['Controllers\CenterController', 'featuredBannerAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/news/featbannerlist',
'handler' => ['Controllers\CenterController', 'listfeaturedbannerAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/featurebanner/delete/{imgid}',
'handler' => ['Controllers\CenterController', 'deletefeaturedimageAction'],
'authentication' => FALSE
];

/** 
Center News Embed Video
 */
$routes[] = [
'method' => 'post',
'route' => '/save/embed',
'handler' => ['Controllers\CenternewsController', 'addembedAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/featvid/list/{centerid}',
'handler' => ['Controllers\CenternewsController', 'featvideoAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/video/delete/{vidid}',
'handler' => ['Controllers\CenternewsController', 'deletevideoAction'],
'authentication' => FALSE
];

/** 
Center News Tags
 */

$routes[] = [
'method' => 'get', 
'route' => '/news/managetags/{num}/{off}/{keyword}', 
'handler' => ['Controllers\CenternewstagsController', 'managetagsAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post', 
'route' => '/news/savetags', 
'handler' => ['Controllers\CenternewstagsController', 'createtagsAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/news/tagsdelete/{id}', 
'handler' => ['Controllers\CenternewstagsController', 'tagsdeleteAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post', 
'route' => '/news/updatetags',
'handler' => ['Controllers\CenternewstagsController', 'updatetagsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/news/listtags',
'handler' => ['Controllers\CenternewstagsController', 'listtagsAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/list/tags/{num}/{off}/{keyword}/{category}', 
'handler' => ['Controllers\CenternewstagsController', 'felisttagsAction'],
'authentication' => FALSE
];
/** 
Center Class Schedule
 */

$routes[] = [
'method' => 'post', 
'route' => '/save/schedule',
'handler' => ['Controllers\CenterscheduleController', 'classaddction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post', 
'route' => '/update/schedule',
'handler' => ['Controllers\CenterscheduleController', 'classupdateaction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/get/schedule/{centerid}',
'handler' => ['Controllers\CenterscheduleController', 'classlistaction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/get/scheduleedit/{schedid}',
'handler' => ['Controllers\CenterscheduleController', 'classgetaction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/delete/schedule/{schedid}/{userid}',
'handler' => ['Controllers\CenterscheduleController', 'classdeleteaction'],
'authentication' => FALSE
];
/** 
Canter News Category
 */
$routes[] = [
'method' => 'get', 
'route' => '/news/managecategory/{num}/{off}/{keyword}', 
'handler' => ['Controllers\CenternewscategController', 'managecategoryAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/validate/categ/{categ}', 
'handler' => ['Controllers\CenternewscategController', 'validatecategAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get', 
'route' => '/validate/tags/{tags}', 
'handler' => ['Controllers\CenternewscategController', 'validatetagsAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post', 
'route' => '/news/savecategory', 
'handler' => ['Controllers\CenternewscategController', 'createcategoryAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/news/categorydelete/{id}', 
'handler' => ['Controllers\CenternewscategController', 'categorydeleteAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'post', 
'route' => '/news/updatecategorynames',
'handler' => ['Controllers\CenternewscategController', 'updatecategoryAction'],
'authentication' => FALSE
];
$routes[] = [
'method' => 'get',
'route' => '/news/listcategory',
'handler' => ['Controllers\CenternewscategController', 'listcategoryAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/list/categ/{num}/{off}/{keyword}/{category}', 
'handler' => ['Controllers\CenternewscategController', 'felistcategoryAction'],
'authentication' => FALSE
];


/** 
Request Info
 */

$routes[] = [
'method' => 'get', 
'route' => '/requestinfo/managerequestinfo/{num}/{off}/{keyword}/{center}', 
'handler' => ['Controllers\RequestinformationController', 'listrequestAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/request/requestdelete/{id}',
'handler' => ['Controllers\RequestinformationController', 'requestdeleteAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/request/listreplies/{id}', 
'handler' => ['Controllers\RequestinformationController', 'listreplyAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/request/view/{id}', 
'handler' => ['Controllers\RequestinformationController', 'viewreplyAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post', 
'route' => '/request/reply', 
'handler' => ['Controllers\RequestinformationController', 'replyrequestAction'],
'authentication' => FALSE
];

//END OF R.I ROUTES//
/** 
APPOINTMENT ROUTES
 */
$routes[] = [
'method' => 'get', 
'route' => '/appointment/manageappointment/{num}/{off}/{keyword}/{center}', 
'handler' => ['Controllers\ScheduleappointmentController', 'listappointmentAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get',
'route' => '/appointment/appointmentdelete/{id}',
'handler' => ['Controllers\ScheduleappointmentController', 'appointmentdeleteAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/appointment/listreplies/{id}', 
'handler' => ['Controllers\ScheduleappointmentController', 'listreplyAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'get', 
'route' => '/appointment/view/{id}', 
'handler' => ['Controllers\ScheduleappointmentController', 'viewappointmentAction'],
'authentication' => FALSE
];

$routes[] = [
'method' => 'post', 
'route' => '/appointment/reply', 
'handler' => ['Controllers\ScheduleappointmentController', 'replyappointmentAction'],
'authentication' => FALSE
];


//END OF APPOINTMENT ROUTES//

/** 
    GET STARTED
 */
    $routes[] = [
    'method' => 'post',
    'route' => '/fe/getstarted/schedule',
    'handler' => ['Controllers\GetstartedformController', 'sendappointmentAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/fe/getstarted/request',
    'handler' => ['Controllers\GetstartedformController', 'sendinforequestAction'],
    'authentication' => FALSE
    ];

//END GET STARTED FORM //

/** 
    AUTHOR - Simula ng ROTA ni Ryanjeric
 */
    $routes[] = [
    'method' => 'post',
    'route' => '/createauthor/authorsave',
    'handler' => ['Controllers\AuthorController', 'saveauthorAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/list/authorimage',
    'handler' => ['Controllers\AuthorController', 'listimageAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'post',
    'route' => '/upload/authorimage',
    'handler' => ['Controllers\AuthorController', 'saveimageAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/manage/listauthor/{num}/{off}/{keyword}',
    'handler' => ['Controllers\AuthorController', 'listauthorAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/manage/authordelete/{id}/{user}',
    'handler' => ['Controllers\AuthorController', 'authordeleteAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/manage/authoredit/{editauthorid}',
    'handler' => ['Controllers\AuthorController', 'authoreditAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'post',
    'route' => '/manage/authorupdate',
    'handler' => ['Controllers\AuthorController', 'updateauthorAction'],
    'authentication' => FALSE
    ];
//DELETE IMAGE == Rota ni Ryan jeric
    $routes[] = [
    'method' => 'get',
    'route' => '/authorimage/delete/{imgid}/{user}',
    'handler' => ['Controllers\AuthorController', 'deleteimageAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/testimoinialimage/delete/{imgid}',
    'handler' => ['Controllers\TestimonialsController', 'deleteimageAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/centernewsimage/delete/{imgid}',
    'handler' => ['Controllers\CenterController', 'deleteimageAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/bannerimage/delete/{imgid}',
    'handler' => ['Controllers\PagesController', 'deleteimageAction'],
    'authentication' => FALSE
    ];

/** 
    Settings - Simula ng ROTA ni Ryanjeric
 */

    $routes[] = [
    'method' => 'get',
    'route' => '/settings/managesettings',
    'handler' => ['Controllers\SettingsController', 'managesettingsAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/settings/maintenanceon',  
    'handler' => ['Controllers\SettingsController', 'maintenanceonAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/settings/maintenanceoff',  
    'handler' => ['Controllers\SettingsController', 'maintenanceoffAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'post',
    'route' => '/settings/uploadlogo',
    'handler' => ['Controllers\SettingsController', 'uploadlogoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/settings/logolist',
    'handler' => ['Controllers\SettingsController', 'listlogoAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/settings/delete/{img}',
    'handler' => ['Controllers\SettingsController', 'deletelogoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/settings/savedefaultlogo',
    'handler' => ['Controllers\SettingsController', 'savedefaultlogoAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'post', 
    'route' => '/settings/googleanalytics',  
    'handler' => ['Controllers\SettingsController', 'scriptAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get', 
    'route' => '/settings/loadscript',  
    'handler' => ['Controllers\SettingsController', 'loadscriptAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get', 
    'route' => '/settings/script',  
    'handler' => ['Controllers\SettingsController', 'displaytAction'],
    'authentication' => FALSE
    ];


/** 
    Main Page Slider - Simula ng ROTA ni Ryanjeric
 */

    $routes[] = [
    'method' => 'get',
    'route' => '/list/mainpageslider',
    'handler' => ['Controllers\MainpagesliderController', 'mainpagesliderAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'post',
    'route' => '/mainpage/saveslider',
    'handler' => ['Controllers\MainpagesliderController', 'uploadimageAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/mainpageslider/delete/{img}',
    'handler' => ['Controllers\MainpagesliderController', 'deleteimageAction'],
    'authentication' => FALSE
    ];

/** 
    DASHBOARD- ROTA ni Ryanjeric
 */

    $routes[] = [
    'method' => 'get',
    'route' => '/dashboard/api',
    'handler' => ['Controllers\DashboardController', 'dashboardAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/dashboard/notification/list/{id}',
    'handler' => ['Controllers\DashboardController', 'notificationAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/dashboard/notification/delete/{id}',
    'handler' => ['Controllers\DashboardController', 'deletenoficationAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/dashboard/notification/changestat/{id}/{stat}',
    'handler' => ['Controllers\DashboardController', 'changestatusAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/notification/list/{num}/{off}/{keyword}/{id}',
    'handler' => ['Controllers\DashboardController', 'listnotifyAction'],
    'authentication' => FALSE
    ];



/** 
    AUDIT LOGS- ROTA ni Ryanjeric
 */

    $routes[] = [
    'method' => 'get',
    'route' => '/auditlogs/list/{n}/{p}/{k}',
    'handler' => ['Controllers\AuditlogController', 'listauditslogAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/auditlogs/delete',
    'handler' => ['Controllers\AuditlogController', 'deleteauditslogAction'],
    'authentication' => FALSE
    ];

/** 
    SEO- ROTA ni Ryanjeric
 */
    $routes[] = [
    'method' => 'get',
    'route' => '/seo/pages/{page}/{submenu}/{subpage}',
    'handler' => ['Controllers\PagesController', 'seopageAction'],
    'authentication' => FALSE
    ];  

/** 
    VISITOR- ROTA ni Ryanjeric
 */
    $routes[] = [
    'method' => 'get',
    'route' => '/visitor/save/{ip}',
    'handler' => ['Controllers\VisitorController', 'savevisitorAction'],
    'authentication' => FALSE
    ];    

    $routes[] = [
    'method' => 'get',
    'route' => '/visitor/count',
    'handler' => ['Controllers\VisitorController', 'countvisitorAction'],
    'authentication' => FALSE
    ]; 
/** 
    BLOGS FEATURED VIDEO
 */
    $routes[] = [
    'method' => 'post',
    'route' => '/blogvid/embed',
    'handler' => ['Controllers\BlogController', 'BlogVideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/blogvid/list',
    'handler' => ['Controllers\BlogController', 'ListVideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/blogvid/delete/{vidid}',
    'handler' => ['Controllers\BlogController', 'deletevideoAction'],
    'authentication' => FALSE
    ];
/** 
    BLOGS FEATURED BANNER
 */
    $routes[] = [
    'method' => 'get',
    'route' => '/blog/banner',
    'handler' => ['Controllers\BlogController', 'ListBannerAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/blogbanner/save',
    'handler' => ['Controllers\BlogController', 'BlogBannerAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/blogbanner/delete/{imgid}',
    'handler' => ['Controllers\BlogController', 'DeleteBannerAction'],
    'authentication' => FALSE
    ];
/** 
    SAVE BLOG
 */
    $routes[] = [
    'method' => 'post',
    'route' => '/newblog/save',
    'handler' => ['Controllers\BlogController', 'SaveBlogAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/blog/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\BlogController', 'manageBlogAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/eblog/list',
    'handler' => ['Controllers\BlogController', 'eListBlogAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/editblog/list/{id}',
    'handler' => ['Controllers\BlogController', 'EditBlogAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/blog/update',
    'handler' => ['Controllers\BlogController', 'updateBlogAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/blog/delete/{id}',
    'handler' => ['Controllers\BlogController', 'DeletBlogAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/blog/changestatus/{id}/{status}',
    'handler' => ['Controllers\BlogController', 'changestatusAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/blog/categories',
    'handler' => ['Controllers\BlogController', 'CategoryListAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/blog/tags',
    'handler' => ['Controllers\BlogController', 'TagsListAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/blog/archives',
    'handler' => ['Controllers\BlogController', 'ArchiveListAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get', 
    'route' => '/validate/blogcateg/{categ}', 
    'handler' => ['Controllers\BlogController', 'validatecategAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/blog/savecategory', 
    'handler' => ['Controllers\BlogController', 'createcategoryAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/blog/listcategory',
    'handler' => ['Controllers\BlogController', 'listcategoryAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/feblog/list/{offset}/{page}',
    'handler' => ['Controllers\BlogController', 'FEListBlogAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/read/blog/{id}',
    'handler' => ['Controllers\BlogController', 'readBlogAction'],
    'authentication' => FALSE
    ];


    $routes[] = [
    'method' => 'get',
    'route' => '/feblog/category/{cat}/{offset}/{page}',
    'handler' => ['Controllers\BlogController', 'fecategoryAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/feblog/tag/{tagsslugs}/{offset}/{page}',
    'handler' => ['Controllers\BlogController', 'listBlogbyTagAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/feblog/archives/{date}',
    'handler' => ['Controllers\BlogController', 'felistarchieveAction'],
    'authentication' => FALSE
    ];

/** 
    BLOG TAGS
 */
    $routes[] = [
    'method' => 'get', 
    'route' => '/blog/managetags/{num}/{off}/{keyword}', 
    'handler' => ['Controllers\BlogtagsController', 'managetagsAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get', 
    'route' => '/blogvalidate/tags/{tags}', 
    'handler' => ['Controllers\BlogtagsController', 'validatetagsAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/blog/savetags', 
    'handler' => ['Controllers\BlogtagsController', 'createtagsAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get', 
    'route' => '/blog/tagsdelete/{id}', 
    'handler' => ['Controllers\BlogtagsController', 'tagsdeleteAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/blog/updatetags',
    'handler' => ['Controllers\BlogtagsController', 'updatetagsAction'],
    'authentication' => FALSE
    ];
/** 
    BLOG CATEGORY
 */
    $routes[] = [
    'method' => 'get', 
    'route' => '/blog/managecategory/{num}/{off}/{keyword}', 
    'handler' => ['Controllers\BlogcategController', 'managecategoryAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get', 
    'route' => '/blogvalidate/categ/{categ}', 
    'handler' => ['Controllers\BlogcategController', 'validatecategAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/blog/savecategory', 
    'handler' => ['Controllers\BlogcategController', 'createcategoryAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get', 
    'route' => '/blog/categorydelete/{id}', 
    'handler' => ['Controllers\BlogcategController', 'categorydeleteAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/blog/updatecategorynames',
    'handler' => ['Controllers\BlogcategController', 'updatecategoryAction'],
    'authentication' => FALSE
    ];
/** 
    E-Learning School
 */
    $routes[] = [
    'method' => 'get', 
    'route' => '/school/list/{num}/{off}/{keyword}', 
    'handler' => ['Controllers\SchoolController', 'managetagsAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get', 
    'route' => '/validate/school/{tags}', 
    'handler' => ['Controllers\SchoolController', 'validatetagsAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/school/save', 
    'handler' => ['Controllers\SchoolController', 'createtagsAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get', 
    'route' => '/school/delete/{id}', 
    'handler' => ['Controllers\SchoolController', 'tagsdeleteAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post', 
    'route' => '/school/update/{tagname}/{id}',
    'handler' => ['Controllers\SchoolController', 'updatetagsAction'],
    'authentication' => FALSE
    ];
    


/** 
    MENTORS VIDEO
 */
    $routes[] = [
    'method' => 'post',
    'route' => '/mentors/embed',
    'handler' => ['Controllers\MentorsController', 'SaveVideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/mentors/list',
    'handler' => ['Controllers\MentorsController', 'ListVideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/mentors/delete/{vidid}',
    'handler' => ['Controllers\MentorsController', 'deletevideoAction'],
    'authentication' => FALSE
    ];
/** 
    LEADERS VIDEO
 */
    $routes[] = [
    'method' => 'post',
    'route' => '/mentors/embed',
    'handler' => ['Controllers\LeadersController', 'SaveVideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/mentors/list',
    'handler' => ['Controllers\LeadersController', 'ListVideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/mentors/delete/{vidid}',
    'handler' => ['Controllers\LeadersController', 'deletevideoAction'],
    'authentication' => FALSE
    ];
/** 
    E-Gallery FEATURED 
 */
    $routes[] = [
    'method' => 'get',
    'route' => '/egallery/banner',
    'handler' => ['Controllers\EGalleryController', 'ListBannerAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/egalleryfe/banner/{id}',
    'handler' => ['Controllers\EGalleryController', 'ListBannerFeaAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/egallery/save',
    'handler' => ['Controllers\EGalleryController', 'BlogBannerAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/egalleryfe/save',
    'handler' => ['Controllers\EGalleryController', 'BlogBannerFEAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/egallery/video',
    'handler' => ['Controllers\EGalleryController', 'ListEvideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/egalleryfe/video/{id}',
    'handler' => ['Controllers\EGalleryController', 'ListEvideoFeAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/evideo/save',
    'handler' => ['Controllers\EGalleryController', 'EvideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/evideofe/save',
    'handler' => ['Controllers\EGalleryController', 'EvideoFEAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/egallery/update',
    'handler' => ['Controllers\EGalleryController', 'UBlogBannerAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'post',
    'route' => '/evideo/update',
    'handler' => ['Controllers\EGalleryController', 'UeVideoAction'],
    'authentication' => FALSE
    ];
    $routes[] = [
    'method' => 'get',
    'route' => '/egallery/delete/{imgid}',
    'handler' => ['Controllers\EGalleryController', 'DeleteBannerAction'],
    'authentication' => FALSE
    ];

    $routes[] = [
    'method' => 'get',
    'route' => '/evideo/delete/{imgid}',
    'handler' => ['Controllers\EGalleryController', 'DeleteVideoAction'],
    'authentication' => FALSE
    ];


/** 
   Training  Reinforcement
 */
   $routes[] = [
   'method' => 'get',
   'route' => '/training/list',
   'handler' => ['Controllers\TrainingController', 'ListtrainingAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/usertraining/list/{userid}/{classfor}/{state}',
   'handler' => ['Controllers\TrainingController', 'UserListtrainingAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/training/save',
   'handler' => ['Controllers\TrainingController', 'SaveTrainingAction'],
   'authentication' => FALSE
   ];

   $routes[] = [
   'method' => 'get',
   'route' => '/training/delete/{imgid}',
   'handler' => ['Controllers\TrainingController', 'DeleteTrainingAction'],
   'authentication' => FALSE
   ];

   $routes[] = [
   'method' => 'get',
   'route' => '/training/edit/{idno}',
   'handler' => ['Controllers\TrainingController', 'edittrainingAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/training/update',
   'handler' => ['Controllers\TrainingController', 'UpdateTrainingAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/training/submit',
   'handler' => ['Controllers\TrainingController', 'SubmitTrainingAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/training/getsent/{userid}/{classid}',
   'handler' => ['Controllers\TrainingController', 'getsentTrainingAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/lasttraining/submit',
   'handler' => ['Controllers\TrainingController', 'lastTrainingAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/list/visit/{usrid}',
   'handler' => ['Controllers\TrainingController', 'ListvisitAction'],
   'authentication' => FALSE
   ];
/** 
   Training  Reinforcement Media
 */
   $routes[] = [
   'method' => 'get',
   'route' => '/tmedia/list/{idno}',
   'handler' => ['Controllers\TmediaController', 'ListmediaAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/usrtmedia/list/{idno}/{usrid}/{task}/{state}',
   'handler' => ['Controllers\TmediaController', 'UsrListmediaAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/tmedia/save',
   'handler' => ['Controllers\TmediaController', 'SavemediaAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/tmediapdf/save',
   'handler' => ['Controllers\TmediaController', 'SavemediapdfAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/tmedia/delete/{imgid}',
   'handler' => ['Controllers\TmediaController', 'DeletemediaAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/edittmedia/list/{idno}',
   'handler' => ['Controllers\TmediaController', 'editmediaAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/edittmedia/read/{idno}',
   'handler' => ['Controllers\TmediaController', 'readmediaAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/pdf/delete/{idno}',
   'handler' => ['Controllers\TmediaController', 'DeletepdfAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/tmedia/update',
   'handler' => ['Controllers\TmediaController', 'UpdatemediaAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'get',
   'route' => '/efeatured/load/{idno}',
   'handler' => ['Controllers\TmediaController', 'LoadFeaturedAction'],
   'authentication' => FALSE
   ];
   $routes[] = [
   'method' => 'post',
   'route' => '/efeatured/video',
   'handler' => ['Controllers\TmediaController', 'PageFeaturedAction'],
   'authentication' => FALSE
   ];

/** 
//FRONEND ROUTES ********************************************************************************************************************
 */
/**
=>FE CHECK404
*/
$routes[] = [
'method' => 'get',
'route' => '/check/page404/{main}/{sub}',
'handler' => ['Controllers\FrontEndLoadController', 'Page404Action'],
'authentication' => FALSE
];
/**
=>FE INDEX PAGE
*/
$routes[] = [
'method' => 'get',
'route' => '/index/route/{slug}/{page}/{others}',
'handler' => ['Controllers\FrontEndLoadController', 'feIndexAction'],
'authentication' => FALSE
];

/**
=>FE PAGE TITLE
*/
$routes[] = [
'method' => 'get',
'route' => '/getpage/title',
'handler' => ['Controllers\FrontEndLoadController', 'feIndexAction'],
'authentication' => FALSE
];

/**
=>FE PAGE TITLE
*/
$routes[] = [
'method' => 'get',
'route' => '/getpage/centers/{slug}',
'handler' => ['Controllers\FrontEndLoadController', 'feCentersAction'],
'authentication' => FALSE
];
/**
=>FE TESTIMONIAL
*/
$routes[] = [
'method' => 'get',
'route' => '/getpage/testimonial',
'handler' => ['Controllers\FrontEndLoadController', 'feTestimonialAction'],
'authentication' => FALSE
];
/**
	STATIC PAGES
 */

$routes[] = [
	'method' => 'get', 
	'route' => '/pagestatic/page/{page}', 
	'handler' => ['Controllers\PagestaticController', 'pageStaticGetAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pagestatic/otherSave', 
	'handler' => ['Controllers\PagestaticController', 'otherSaveAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pagestatic/bannerSave', 
	'handler' => ['Controllers\PagestaticController', 'bannerSaveAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/pagestatic/listimages',
	'handler' => ['Controllers\PagestaticController', 'listimageAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post',
	'route' => '/pagestatic/saveimage',
	'handler' => ['Controllers\PagestaticController', 'saveimageAction'],
	'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/pagestatic/delete/{imgid}',
    'handler' => ['Controllers\PagestaticController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/pagestatic/listimg',
	'handler' => ['Controllers\PagestaticController', 'listimgAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post',
	'route' => '/pagestatic/saveimg',
	'handler' => ['Controllers\PagestaticController', 'saveimgAction'],
	'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/pagestatic/dltimg/{imgid}',
    'handler' => ['Controllers\PagestaticController', 'dltimgAction'],
    'authentication' => FALSE
];


return $routes;

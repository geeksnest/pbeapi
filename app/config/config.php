<?php
/**
 * Settings to be stored in dependency injector
 */
$settings = array(
 'database' => array(
  'adapter' => 'Mysql', 
  'host' => 'localhost',
  'username' => 'root',
  'password' => '',
  'name' => 'dbpde',
  'port' => 3306
 ),
    'application' => array(
        'baseURL' => 'http://pbesite',
        'apiURL' => 'http://pbeapi',
    ),
    'hashkey' => '4a478258bd8e11f4046d6fe49471401893d69469',
    'postmark' => array(
        'url' => 'https://api.postmarkapp.com/email',
        'token' => '016e0c8c-d974-491f-b17d-f9c89915ec0a',
        'signature' => 'admin@powerbraineducation.com'
    )
);


return $settings;
<?php

namespace Controllers;
use \Models\Users as Users;
use \Models\Pages as Pages;
use \Models\Pageimage as Pageimage;
use \Models\Menumain as Menumain;
use \Models\Menuconstant as ParentSub;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class PagesController extends \Phalcon\Mvc\Controller {

    public function createPagesAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $desc = $request->getPost('desc');
            $body = $request->getPost('body');
            $parentmenu = $request->getPost('parentmenu');
            $status = $request->getPost('status');
            @$metatags = $request->getPost('metatags');
            @$submenu = $request->getPost('menussub');
            @$banner = $request->getPost('banner');
            $menu_type = $request->getPost('menutyped');

            if($menu_type=="parentsub"){
                $menu = new ParentSub();
                $getParent = Menumain::findFirst(array("id='".$parentmenu."'")); 
                $menu->assign(array(
                    'parentmenu' =>$getParent->slug,
                    'submenu' => $slugs,
                    'title' => $title,));
                if (!$menu->save()) {
                    $errors = array();
                    foreach ($page->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $data[]=array('error' => $errors);
                } else {
                   $data[]=array('success' => "New Page has been successfully Created!");
                    //START Log
                   $audit = new CB();
                   $audit->auditlog(array(
                      "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Add Page - ".$title." ", /*// Maybe some info here (confuse) XD*/
                      ));
                    //END Audit Log
               }
           }
           $guid = new \Utilities\Guid\Guid();
           $page = new Pages();
           $page->assign(array(
            'pageid'=>$guid->GUID(),
            'title' => $title,
            'pageslugs' => $slugs,
            'shortdesc' => $desc,
            'body' => $body,
            'status' => $status,
            'metatags' =>$metatags,
            'type' => $menu_type ,
            'mainmenu'=>$parentmenu,
            'submenu'=>$submenu,
            'banner' => $banner,
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s"),
            ));

           if (!$page->save()) {
            $errors = array();
            foreach ($page->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data[]=array('error' => $errors);
        } else {
           $data[]=array('success' => "New Page has been successfully Created!");
                    //START Log
                   $audit = new CB();
                   $audit->auditlog(array(
                      "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Add Page - ".$title." ", /*// Maybe some info here (confuse) XD*/
                      ));
                    //END Audit Log
       }


   }
   echo json_encode($data);
}

public function saveimageAction($filename) {

    var_dump($filename);

    $picture = new Pageimage();
    $picture->assign(array(
        'filename' => $filename
        ));

    if (!$picture->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } else {
        $data['success'] = "Success";
        //START Log
        $audit = new CB();
        $audit->auditlog(array(
          "module" =>"Image", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Upload Image- ".$filename." ", /*// Maybe some info here (confuse) XD*/
          ));
        //END Audit Log
    }

}
//IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = Pageimage::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                 //START Log
                $audit = new CB();
                $audit->auditlog(array(
                  "module" =>"Image", /*//Examaple News, Create Center, Slider, Events etc...*/
                  "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                  "title" => "Delete Image- ".$filename." ", /*// Maybe some info here (confuse) XD*/
                  ));
                //END Audit Log
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }

public function listimageAction() {

    $getimages = Pageimage::find(array("order" => "id DESC"));
     if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        }
        echo json_encode($data);

}


public function managepagesAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Pages::find();
    } else {
        $conditions = "title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
        $Pages = Pages::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'pageid' => $m->pageid,
            'title' => $m->title,
            'metatags' => $m->metatags,
            'status' => $m->status
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}


public function pageUpdatestatusAction($status,$pageid,$keyword) {

    $data = array();
    $page = Pages::findFirst('pageid=' . $pageid . ' ');
    $title = $page->title;
    $page->status = $status;
    if (!$page->save()) {
        $data['error'] = "Something went wrong saving page status, please try again.";
    } else {
        $data['success'] = "Success";
        //START Log
        $audit = new CB();
        $audit->auditlog(array(
          "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Update Page - ".$title." ", /*// Maybe some info here (confuse) XD*/
          ));
        //END Audit Log
    }

    echo json_encode($data);
}

public function pagedeleteAction($pageid) {
    $data = $pageid;
    $pages = Pages::findFirst('pageid="'. $pageid.'"');
    $title = $pages->title;
    $menus = ParentSub::findFirst('submenu="'. $pages->pageslugs.'"');
    $title1 = $menus->title;

    $data = array('error' => 'Not Found');
    if ($pages) {
        if ($pages->delete() ) {
            $data = array('success' => '');
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Page - ".$title." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
    }
    if ($menus) {
        if ($menus->delete() ) {
            $data = array('success' => '');
              //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Page - ".$title1." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
    }
    echo json_encode($data);
}

public function pageeditoAction($pageid) {
    $data = array();

    $page = Pages::find("pageid='".$pageid."'");
    $data=array(
        'pageid'=>$page[0]->pageid,
        'title'=>$page[0]->title,
        'slugs'=>$page[0]->pageslugs,
        'desc'=>$page[0]->shortdesc,
        'body'=>$page[0]->body,
        'menutyped'=>$page[0]->type,
        'parentmenu'=>$page[0]->mainmenu,
        'menussub'=>$page[0]->submenu,
        'status'=>$page[0]->status,
        'metatags'=>$page[0]->metatags,
        'banner'=>$page[0]->banner
        );

    echo json_encode($data);
}
public function seopageAction($page,$submenu,$subpage) {
    $data = array();

    if($subpage!="page"){
      $string = str_replace("-", " ", $subpage);
      $page = Pages::find("submenu='".$submenu."'and title='".$string."'");
    }else{
      $string = str_replace("-", " ", $submenu);
      $page = Pages::find("title='".$string."'");
    }

    $data=array(
        'title'=>$page[0]->title,
        'desc'=>$page[0]->shortdesc,
        'metatags'=>$page[0]->metatags,
        );
    echo json_encode($data);
}




public function saveeditedPagesAction() {
    $request = new \Phalcon\Http\Request();
    if($request->isPost()){
        $pageid = $request->getPost('pageid');
        $title = $request->getPost('title');
        $slugs = $request->getPost('slugs');
        $desc = $request->getPost('desc');
        $body = $request->getPost('body');
        $menu_type = $request->getPost('menutyped');
        $parentmenu = $request->getPost('parentmenu');
        $status = $request->getPost('status');
        @$metatags = $request->getPost('metatags');



        @$submenu = $request->getPost('menussub');
        @$banner = $request->getPost('banner');
        //SAVE
        $usave = Pages::findFirst('pageid="' . $pageid . '"');
        $usave->title        = $title;
        $usave->pageslugs    = $slugs;
        $usave->shortdesc    = $desc;
        $usave->body         = $body;
        $usave->status       = $status;
        $usave->metatags     = $metatags;
        $usave->type         = $menu_type;
        $usave->mainmenu     = $parentmenu;
        $usave->submenu      = $submenu ;
        $usave->banner      = $banner;
        $usave->updated_at   = date("Y-m-d H:i:s");

        if(!$usave->save()){
            $errors = array();
            foreach ($usave->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data[]=array('error' => $errors);
        }else{
            $data[]=array('success' => 'User info has been successfuly updates.');
               //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Update Page - ".$title." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
    }
    echo json_encode($data);
}

 public function changestatusAction($id,$status){
        $getInfo = Pages::findFirst('pageid="'. $id .'"');
        $title = $getInfo->title;
        if($status == 'true'){
           $getInfo->status = 'false';
           $getInfo->save();
           $data=array('success' => 'Deactivated');
               //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Deactivated", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Page - ".$title." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
        else{
           $getInfo->status = 'true';
           $getInfo->save();
           $data=array('success' => 'Activated');
               //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Page", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Activated", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Page - ".$title." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
        echo json_encode($data);
    }
public function getPageAction($pageslugs) {

    $conditions = "pageslugs LIKE'" . $pageslugs . "'" ;
    echo json_encode(Pages::findFirst(array($conditions))->toArray(), JSON_NUMERIC_CHECK);

}
public function uploadBannerAction() {
    $filename = $_POST['imgfilename'];

    $picture = new Pageimage();
    $picture->assign(array(
        'filename' => $filename
        ));

    if (!$picture->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } else {
        $data['success'] = "Success";
           //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Image", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Add image - ".$filename." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
    }
    echo json_encode($data);
}

}
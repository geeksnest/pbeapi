<?php
namespace Controllers;
use \Models\Scheduleappointment as Scheduleappointment;
use \Models\Requestinformation as Requestinformation;
use \Models\Notifications as Notifications;
use \Controllers\ControllerBase as CB;
use \Models\Center as Center;
class GetstartedformController extends \Phalcon\Mvc\Controller {
    public function sendappointmentAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();
        $id = $guid->GUID();
        $center= $request->getPost('center');
        $fname= $request->getPost('fname');
        $lname= $request->getPost('lname');
        $email= $request->getPost('email');
        $datesched= $request->getPost('datesched');
        $timesched= $request->getPost('timesched');
        $content= $request->getPost('content');


        if($center =="Others"){
            $_emails = "admin@powerbraineducation.com";
        }else{
            $_emails = $_emailCenter->email;
        }


        // $g_center = array(
        //     // "Bayside"   =>"bayside@powerbraineducation.com", 
        //     // "Fairfax "  =>"fairfax@bodynbrain.com", 
        //     // "Mesa"      =>"jmoore0406@gmail.com", 
        //     // "Syosset"   =>"syosset@bodynbrain.com",
        //     // "Others"    =>"admin@powerbraineducation.com"
            
        //     "Bayside"   =>"pbebayside@mailinator.com", 
        //     "Fairfax"   =>"pbefairfax@mailinator.com", 
        //     "Mesa"      =>"pbemesa@mailinator.com", 
        //     "Syosset"   =>"pbesyosset@mailinator.com",
        //     "Others"    =>"pberequest@mailinator.com"
        //     );
        // if($g_center[$center]){
        //    $_emails = $g_center[$center];
        // }else{
        //    $_emails = "pberequest@mailinator.com";
        // }

        $_emailCenter = Center::findFirst("title='".$center."'");
        

        $add = new Scheduleappointment();
        $add->assign(array(
            'id' => $id,
            'center' => $center,
            'fname' => $fname,
            'lname' => $lname,
            'email' => $email,
            'datesched' => $datesched,
            'timesched' => $timesched,
            'content' => $content,
            'datesubmitted' => date('Y-m-d'),
            'status' => 0,
            'datecreated' => date('Y-m-d H:i:s')
            ));
                    // $add->save();
        if (!$add->save()) {
            $errors = array();
            foreach ($add->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
            $data['error'] ="!SAVE";
        } 
        else{

             $app = new CB();
            $content = '
            Power Brain Schedule an Introductory Session, 
            <br><br>
            '.$fname.' '.$lname.'<br>
            '.$email.'
            <br><br>
            Date:'.$datesched.'
            Time:'.$timesched.'
            <br><br>
            '.$content.'';
            $_rdata = $app->sendMail($_emails, 'Schedule an Introductory Session', $content);

            //SEND TO ADMIN
            $mailAdmin = new CB();
            $contentmsg = '
            Power Brain Request Information, 
            <br><br>
            '.$fname.' '.$lname.'<br>
            '.$email.'
            <br><br>
            Date:'.$datesched.'
            Time:'.$timesched.'
            <br><br>
            '.$content.'';
            $_rdata = $mailAdmin->sendMail('davebeal@powerbraineducation.com', 'Schedule an Introductory Session', $contentmsg);


            $data['success'] ="SAVE";
            $Notifications = new Notifications();
            $Notifications->assign(array(
                'id' => $id,
                'center' => $center,
                'fname' => $fname,
                'lname' => $lname,
                'email' => $email,
                'status' => 0,
                'type' =>0,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
            $Notifications->save();
            $audit = new CB();
            $audit->auditlog(array("module" =>"Appointment",  "event" => "Message", "title" => "".$fname." ".$lname." Sent an Appointment in ".date('Y-m-d')."",));
        }
        echo json_encode(array($data));
    }
    public function sendinforequestAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();


        $id = $guid->GUID();
        echo $center= $request->getPost('center');
        $fname= $request->getPost('fname');
        $lname= $request->getPost('lname');
        $email= $request->getPost('email');
        $category= $request->getPost('category');
        $content= $request->getPost('content');


        $_emailCenter = Center::findFirst("title='".$center."'");


        if($center =="Others"){
            $_emails = "admin@powerbraineducation.com";
        }else{
            $_emails = $_emailCenter->email;
        }


        // $g_center = array(
        //     // "Bayside"   =>"bayside@powerbraineducation.com", 
        //     // "Fairfax "  =>"fairfax@bodynbrain.com", 
        //     // "Mesa"      =>"jmoore0406@gmail.com", 
        //     // "Syosset"   =>"syosset@bodynbrain.com",
        //     // "Others"    =>"admin@powerbraineducation.com"
            
        //     "Bayside"   =>"pbebayside@mailinator.com", 
        //     "Fairfax"   =>"pbefairfax@mailinator.com", 
        //     "Mesa"      =>"pbemesa@mailinator.com", 
        //     "Syosset"   =>"pbesyosset@mailinator.com",
        //     "Others"    =>"pberequest@mailinator.com"
        //     );
        // if($g_center[$center]){
        //    echo  $_emails = $g_center[$center];
        // }else{
        //    echo $_emails = "pberequest@mailinator.com";
        // }

        $add = new Requestinformation();
        $add->assign(array(
            'id' => $id,
            'center' => $center,
            'fname' => $fname,
            'lname' => $lname,
            'email' => $email,
            'category' => $category,
            'content' => $content,
            'datesubmitted' => date('Y-m-d'),
            'status' => 0,
            'datecreated' => date('Y-m-d H:i:s')
            ));

        if (!$add->save()) {
            $errors = array();
            foreach ($add->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
            $data['error'] ="!SAVE";
        } 
        else{
            $app = new CB();
            $content = '
            Power Brain Request Information, 
            <br><br>
            '.$fname.' '.$lname.'<br>
            '.$email.'
            <br><br>
            Category: '.$category.'
            <br><br>
            '.$content.'';
            $_rdata = $app->sendMail($_emails, 'Request Information', $content);


            //SEND TO ADMIN
            $mailAdmin = new CB();
            $content = '
            Power Brain Request Information, 
            <br><br>
            '.$fname.' '.$lname.'<br>
            '.$email.'
            <br><br>
            Category: '.$category.'
            <br><br>
            '.$content.'';
            $_rdata = $mailAdmin->sendMail('davebeal@powerbraineducation.com', 'Request Information', $content);



            $data['success'] ="SAVE";
            $Notifications = new Notifications();
            $Notifications->assign(array(
                'id' => $id,
                'center' => $center,
                'fname' => $fname,
                'lname' => $lname,
                'email' => $email,
                'status' => 0,
                'type' => 1,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
            $Notifications->save();
            $audit = new CB();
            $audit->auditlog(array("module" =>"Request Information", "event" => "Message", "title" => "".$fname." ".$lname." Sent a Requestinformation in ".date('Y-m-d')."", ));
        }
        echo json_encode(array($data));
    }
}
<?php

namespace Controllers;
use \Models\Users as Users;
use \Controllers\ControllerBase as CB;
use \Models\Center as Center;
use \Models\School as School;
use \Models\Forgotpassword as Forgotpasswords;

class UserController extends \Phalcon\Mvc\Controller {
 public function EuserExistAction($name) {
  if(!empty($name)) {
    $user_admin  = Users::findFirst('username="' . $name . '" and (task!="Admin") and (task!="CS")');
    if($user_admin) {
      echo json_encode(array('exists' => true));
    }else {
      echo json_encode(array('exists' => false));
    }
  }
}

public function userExistAction($name) {
  if(!empty($name)) {
    $user = Users::findFirst('username="' . $name . '" and (task="Admin" or task="CS")');
    if ($user) {
      echo json_encode(array('exists' => true));
    } else {
      echo json_encode(array('exists' => false));
    }
  }
}

public function emailExistAction($email)
{
  if (!empty($email)) {
    $email = Users::findFirst('email="'. $email .'"');
    if ($email) {
      echo json_encode(array('exists' => true));
    } else {
      echo json_encode(array('exists' => false));
    }
  }
}

public function activationAction(){

  if (!empty($_POST['code'])) {
    $code = $_POST['code'];
    $user = Users::findFirst('activation_code="' . $code . '"');
    if (isset($user->activation_code)) {
      $user->activation_code = '';
      $user->status = 1;
      if($user->save()){
        echo json_encode(array('success' => 'Your account have been fully activated. You can now login.'));
      }else{
        echo json_encode(array('error' => 'Cannot update record.'));
      }
    } else {
      echo json_encode(array('error' => 'No activation code found.'));
    }
  }
}

public function cmsRegisterUserAction(){
  $seed = str_split('abcdefghijklmnopqrstuvwxyz'
     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789'); // and any other characters
    shuffle($seed); // probably optional since array_is randomized; this may be redundant
    $rand = '';
    foreach (array_rand($seed, 8) as $k) $rand .= $seed[$k];
    $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
    if($request->isPost()){
      $username   = $request->getPost('username');
      ($username ? $username : $username = '');
      $email      = $request->getPost('email');
      $password   = $request->getPost('password');
      ($password ? $password : $password = $rand);
      $userrole   = $request->getPost('userrole');
      $firstname  = $request->getPost('fname');
      ($firstname ? $firstname : $firstname = '');
      $lastname   = $request->getPost('lname');
      ($lastname ? $lastname : $lastname = '');
      $bday       = $request->getPost('bday');
      ($bday ? $bday : $bday = date("Y-m-d H:i:s"));
      $gender     = $request->getPost('gender');
      ($gender ? $gender : $gender = '');
      $banner     = $request->getPost('banner');
      ($banner ? $banner : $banner = '');
      $status     = $request->getPost('status');
      ($status ? $status : $status = 0);

      if($userrole=="teacher" ||  $userrole=="leader" ||  $userrole=="mentor" ||  $userrole==null ||  $userrole=="" ||  $userrole==undefined){
       $status = 0;
     }
     $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
     $dates = explode(" ", $bday);
     $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
     /* Register User*/
     $guid = new \Utilities\Guid\Guid();
     $userid = $guid->GUID();
     $code = $guid->GUID();
     $usave = new Users();
     $usave->id = $userid;
     $usave->username = $username;
     $usave->email = $email;
     $usave->password = sha1($password);
     $usave->task = $userrole;
     $usave->first_name = $firstname;
     $usave->last_name = $lastname;
     $usave->birthday = $d;
     $usave->gender = $gender;
     $usave->status = $status;
     $usave->review = 0;
     $usave->profile_pic_name = $banner;
     $usave->activation_code = $code;
     $usave->created_at = date("Y-m-d H:i:s");
     $usave->updated_at = date("Y-m-d H:i:s");
     if(!$usave->create()){
      $errors = array();
      foreach ($usave->getMessages() as $message) {
        $errors[] = $message->getMessage();
      }
      $data[]=array('error' => $errors);
    }else{
      $data[]=array('success' => 'You are now successfuly registered.'.$_rdata);
           //START Log
      $audit = new CB();
      $audit->auditlog(array(
        "module" =>"User", /*//Examaple News, Create Center, Slider, Events etc...*/
        "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
        "title" => "Add User : ". $username ."" /*// Maybe some info here (confuse) XD*/
        ));
            //END Audit Log
    }
  }else{
    $data[]=array('error' => 'No post data.');
  }
  echo json_encode($data);
}


public function registerUserAction(){
  $seed = str_split('abcdefghijklmnopqrstuvwxyz'
     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789'); // and any other characters
    shuffle($seed); // probably optional since array_is randomized; this may be redundant
    $rand = '';
    foreach (array_rand($seed, 8) as $k) $rand .= $seed[$k];
    $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
    if($request->isPost()){
      $username   = $request->getPost('username');
      ($username ? $username : $username = '');
      $email      = $request->getPost('email');
      $password   = $request->getPost('password');
      ($password ? $password : $password = $rand);
      $userrole   = $request->getPost('userrole');
      $firstname  = $request->getPost('fname');
      ($firstname ? $firstname : $firstname = '');
      $lastname   = $request->getPost('lname');
      ($lastname ? $lastname : $lastname = '');
      $bday       = $request->getPost('bday');
      ($bday ? $bday : $bday = date("Y-m-d H:i:s"));
      $gender     = $request->getPost('gender');
      ($gender ? $gender : $gender = '');
      $banner     = $request->getPost('banner');
      ($banner ? $banner : $banner = '');
      $status     = $request->getPost('status');
      ($status ? $status : $status = 0);

      if($userrole=="teacher" ||  $userrole=="leader" ||  $userrole=="mentor" ||  $userrole==null ||  $userrole=="" ||  $userrole==undefined){
       $status = 0;
     }
     $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
     $dates = explode(" ", $bday);
     $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
     /* Register User*/
     $guid = new \Utilities\Guid\Guid();
     $userid = $guid->GUID();
     $code = $guid->GUID();
     $usave = new Users();
     $usave->id = $userid;
     $usave->username = $username;
     $usave->email = $email;
     $usave->password = sha1($rand);
     $usave->task = $userrole;
     $usave->first_name = $firstname;
     $usave->last_name = $lastname;
     $usave->birthday = $d;
     $usave->gender = $gender;
     $usave->status = $status;
     $usave->review = 0;
     $usave->profile_pic_name = $banner;
     $usave->activation_code = $code;
     $usave->created_at = date("Y-m-d H:i:s");
     $usave->updated_at = date("Y-m-d H:i:s");
     if(!$usave->create()){
      $errors = array();
      foreach ($usave->getMessages() as $message) {
        $errors[] = $message->getMessage();
      }
      $data[]=array('error' => $errors);
    }else{
      $activationLink = "http://www.powerbraineducation.com/elearning/signin";
      //$activationLink = "http://pbe.gotitgenius.com/elearning/signin";

     $app = new CB();
     $content = '
     Welcome to the Power Brain Education Learning Community, 
     <br><br>
     Please click the confirmation link below to complete and activate your account. 
     <br><br>
     Your Temporary Password: '.$rand.'
     <br><br>
     <a href="'.$activationLink .'">'.$activationLink .'</a>
     <br><br>
     Congratulations on your choice to become a Power Brain instructor. We hope the videos and information on this website help you deliver Power Brain to your students with maximum effectiveness. Let\'s work together to share health, happiness, mindfulness and optimal achievement with all of our students.
     <br><br>
     Please visit our website to learn more about Power Brain Education and our programs.
     <br>
     <a href="http://www.powerbraineducation.com">http://www.powerbraineducation.com</a>
     <br><br>
     Don\'t hesitate to contact us with any questions or suggestions.
     Thank you.
     <br><br>
     Power Brain Education Staff
     ';
     $_rdata = $app->sendMail($email, 'Power Brain Education  Confirmation Email', $content);
     $data[]=array('success' => 'You are now successfuly registered.'.$_rdata);

           //START Log
     $audit = new CB();
     $audit->auditlog(array(
      "module" =>"User", /*//Examaple News, Create Center, Slider, Events etc...*/
      "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
      "title" => "Add User : ". $username ."" /*// Maybe some info here (confuse) XD*/
      ));
            //END Audit Log
   }
 }else{
  $data[]=array('error' => 'No post data.');
}
echo json_encode($data);
}





public function loginAction($username,$password){

  $request = new \Phalcon\Http\Request();
         $user = Users::findFirst('(username="'.$username.'" or email="'.$username.'") AND  password="'. sha1($password).'" AND status=1');////CURL*******
         $userid = $user->id;
         if($user->task == "CS"){
          $checkcenter = Center::findFirst('manager="'.$userid.'"');
          if(!$checkcenter){
           $data[]=array('error' => "NO CENTER FOUND!<br>Ask your System Administrator to set a Center for your account.");
           $data[]=array('success'=>$payload);
           $auditid = new \Utilities\Guid\Guid();
           $auditid->GUID();
           $db = \Phalcon\DI::getDefault()->get('db');
           $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$userid."','User','Login','Login Failed No Center Found: ".$username." ')");
           $sql->execute();
         }
         else{
           $jwt = new \Security\Jwt\JWT();
           $payload = array(
            "id" => $user->id,
            "username" => $user->username,
            "lastname" => $user->last_name,
            "firstname" => $user->first_name,
            "profile" => $user->profile_pic_name,
            "code" => $user->activation_code,
            "task" => $user->task,
            "exp" => time() + (60 * 60));
           $token = $jwt->encode($payload, $app->config->hashkey);

           $data[]=array('success'=>$payload);
           $auditid = new \Utilities\Guid\Guid();
           $auditid->GUID();
           $db = \Phalcon\DI::getDefault()->get('db');
           $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$user->id."','User','Login','Login Success: ".$username."')");
           $sql->execute();
         }
       } elseif($user->task == "Admin") {
        $jwt = new \Security\Jwt\JWT();
        $payload = array(
          "id" => $user->id,
          "username" => $user->username,
          "lastname" => $user->last_name,
          "firstname" => $user->first_name,
          "profile" => $user->profile_pic_name,
          "task" => $user->task,
           "exp" => time() + (60 * 60));
        $token = $jwt->encode($payload, $app->config->hashkey);

        $data[]=array('success'=>$payload, 'token'=>$token);
        $auditid = new \Utilities\Guid\Guid();
        $auditid->GUID();
        $db = \Phalcon\DI::getDefault()->get('db');
        $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$user->id."','User','Login','Login Success: ".$username."')");
        $sql->execute();
      }else{
        $data[]=array('error' => 'Username or Password is invalid.');

        $auditid = new \Utilities\Guid\Guid();
        $auditid->GUID();
        $db = \Phalcon\DI::getDefault()->get('db');
        $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','ID NOT AVAILABLE','User','Login','Login Failed : ".$username." ')");
        $sql->execute();
      }
      echo json_encode($data);

    }


    public function eloginAction($username,$password){

      $request = new \Phalcon\Http\Request();
         $user = Users::findFirst('(username="'.$username.'" or email="'.$username.'") AND  password="'. sha1($password).'" AND status=1');////CURL*******
         $userid = $user->id;
         if($user) {
       
          if($user->task != "Admin" && $user->task != "CS" ){
            $jwt = new \Security\Jwt\JWT();
            $payload = array(
              "id" => $user->id,
              "username" => $user->username,
              "lastname" => $user->last_name,
              "firstname" => $user->first_name,
              "profile" => $user->profile_pic_name,
              "task" => $user->task,
              "exp" => time() + (60 * 60));
            $token = $jwt->encode($payload, $app->config->hashkey);

            $data[]=array('success'=>$payload, 'token'=>$token);
            $auditid = new \Utilities\Guid\Guid();
            $auditid->GUID();
            $db = \Phalcon\DI::getDefault()->get('db');
            $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$user->id."','User','Login','Login Success: ".$username."')");
            $sql->execute();
          }else{
            $data[]=array('error' => 'Username or Password is invalid.');
          }
        }else{
          $data[]=array('error' => 'Username or Password is invalid.');

          $auditid = new \Utilities\Guid\Guid();
          $auditid->GUID();
          $db = \Phalcon\DI::getDefault()->get('db');
          $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','ID NOT AVAILABLE','User','Login','Login Failed : ".$username." ')");
          $sql->execute();
        }
        echo json_encode($data);
      }
      public function signinAction($username,$password){
        $request = new \Phalcon\Http\Request();
         $user = Users::findFirst('(username="'.$username.'" or email="'.$username.'") AND  password="'. sha1($password).'" AND status=0');////CURL*******
         $userid = $user->id;
         if($user){
          $jwt = new \Security\Jwt\JWT();
          $payload = array(
            "id" => $user->id,
            "username" => $user->username,
            "lastname" => $user->last_name,
            "firstname" => $user->first_name,
            "profile" => $user->profile_pic_name,
            "code" => $user->activation_code,
            "task" => $user->task,
            "exp" => time() + (60 * 60));
          $token = $jwt->encode($payload, $app->config->hashkey);
          $data[]=array('success'=>$payload, 'token'=>$token);
          $auditid = new \Utilities\Guid\Guid();
          $auditid->GUID();
          $db = \Phalcon\DI::getDefault()->get('db');
          $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$user->id."','User','Login','Login Success: ".$username."')");
          $sql->execute();
        }else{
          $data[]=array('error' => 'Username or Password is invalid.');
          $auditid = new \Utilities\Guid\Guid();
          $auditid->GUID();
          $db = \Phalcon\DI::getDefault()->get('db');
          $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','ID NOT AVAILABLE','User','Login','Login Failed : ".$username." ')");
          $sql->execute();
        }
        echo json_encode($data);
      }

      public function skipAction($name) { echo "auth skipped ($name)";}

      public function userListAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
          $userlist = Users::find(array(" task='CS' or task='admin'","order"=>"task"));
        } else {
          $conditions = "
          (task='CS' or task='admin')
          and 
          (username LIKE '%" . $keyword . "%' 
          or first_name LIKE '%" . $keyword . "%'
          or last_name LIKE '%" . $keyword . "%'
          or  email LIKE '%" . $keyword . "%')";
          $userlist= Users::find(array($conditions,"order"=>"task"));
        }
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
          array(
            "data" => $userlist,
            "limit" => 10,
            "page" => $currentPage
            )
          );
        // Get the paginated results
        $page = $paginator->getPaginate();
        $data = array();
        foreach ($page->items as $m){
          $data[] = array(
            'id' => $m->id,
            'fname' => $m->first_name,
            'lname' => $m->last_name,
            'email' => $m->email,
            'username' => $m->username,
            'status' => $m->status,
            'userrole' => $m->task
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
          $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
      }

      public function userListAdminAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
          $userlist = Users::find(array(" task='Admin'","order"=>"first_name"));
        } else {
          $conditions = "
          (task='Admin')
          and 
          (username LIKE '%" . $keyword . "%' 
          or first_name LIKE '%" . $keyword . "%'
          or last_name LIKE '%" . $keyword . "%'
          or  email LIKE '%" . $keyword . "%')";
          $userlist= Users::find(array($conditions,"order"=>"task"));
        }
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
          array(
            "data" => $userlist,
            "limit" => 10,
            "page" => $currentPage
            )
          );
        // Get the paginated results
        $page = $paginator->getPaginate();
        $data = array();
        foreach ($page->items as $m){
          $data[] = array(
            'id' => $m->id,
            'fname' => $m->first_name,
            'lname' => $m->last_name,
            'email' => $m->email,
            'username' => $m->username,
            'status' => $m->status,
            'userrole' => $m->task
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
          $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
      }

      public function userListManagerAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
          $userlist = Users::find(array(" task='CS'","order"=>"first_name"));
        } else {
          $conditions = "
          (task='CS')
          and 
          (username LIKE '%" . $keyword . "%' 
          or first_name LIKE '%" . $keyword . "%'
          or last_name LIKE '%" . $keyword . "%'
          or  email LIKE '%" . $keyword . "%')";
          $userlist= Users::find(array($conditions,"order"=>"task"));
        }
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
          array(
            "data" => $userlist,
            "limit" => 10,
            "page" => $currentPage
            )
          );
        // Get the paginated results
        $page = $paginator->getPaginate();
        $data = array();
        foreach ($page->items as $m){
          $data[] = array(
            'id' => $m->id,
            'fname' => $m->first_name,
            'lname' => $m->last_name,
            'email' => $m->email,
            'username' => $m->username,
            'status' => $m->status,
            'userrole' => $m->task
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
          $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
      }



      public function userteacherListAction($num, $page, $keyword, $usertype) {
        if ($keyword == 'null' || $keyword == 'undefined') {
          $userlist = Users::find(array(" task='".$usertype."'","order"=>"task"));
        } else {
          $conditions = "
          task='".$usertype."' 
          and 
          (username LIKE '%" . $keyword . "%' 
          or first_name LIKE '%" . $keyword . "%'
          or last_name LIKE '%" . $keyword . "%'
          or  email LIKE '%" . $keyword . "%')";
          $userlist= Users::find(array($conditions,"order"=>"last_name"));
        }

        $currentPage = (int) ($page);
        $paginator = new \Phalcon\Paginator\Adapter\Model(
          array(
            "data" => $userlist,
            "limit" => 10,
            "page" => $currentPage
            )
          );
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {

          if($m->status==0 && $m->review==0){
            $_pendinguser = true;
           }else{
            $_pendinguser = false;
           }
       


          $data[] = array(
            'id' => $m->id,
            'fname' => $m->first_name,
            'lname' => $m->last_name,
            'email' => $m->email,
            'username' => $m->username,
            'status' => $m->status,
            'userrole' => $m->task,
            'pending' => $_pendinguser
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
          $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
      }

      public function userPendingListAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
          $userlist = Users::find(array("status=0 and review='1'"));
        } else {
          $conditions = "
          (status=0 and review='1')
          and 
          (username LIKE '%" . $keyword . "%' 
          or first_name LIKE '%" . $keyword . "%'
          or last_name LIKE '%" . $keyword . "%'
          or  email LIKE '%" . $keyword . "%')";
          $userlist= Users::find(array($conditions,"order"=>"last_name"));
        }

        $currentPage = (int) ($page);
        $paginator = new \Phalcon\Paginator\Adapter\Model(
          array(
            "data" => $userlist,
            "limit" => 10,
            "page" => $currentPage
            )
          );
        $page = $paginator->getPaginate();
        $data = array();
        foreach ($page->items as $m) {
         $_skul = School::findFirst("id='".$m->school."'");
         $data[] = array(
          'id' => $m->id,
          'fname' => $m->first_name,
          'lname' => $m->last_name,
          'email' => $m->email,
          'username' => $m->username,
          'birthday' => date("F j, Y", $m->birthday),
          'gender' => $m->gender,
          'school' => $_skul->name,
          'grade' => $m->grade,
          'status' => $m->status,
          'userrole' => $m->task,
          'profile' => $m->profile_pic_name
          );
       }
       $p = array();
       for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
      }
      echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function userNewaddedListAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
          $userlist = Users::find(array("status=0 and review='0'"));
        } else {
          $conditions = "
          (status=0 and review='0')
          and 
          (username LIKE '%" . $keyword . "%' 
          or first_name LIKE '%" . $keyword . "%'
          or last_name LIKE '%" . $keyword . "%'
          or  email LIKE '%" . $keyword . "%')";
          $userlist= Users::find(array($conditions,"order"=>"last_name"));
        }

        $currentPage = (int) ($page);
        $paginator = new \Phalcon\Paginator\Adapter\Model(
          array(
            "data" => $userlist,
            "limit" => 10,
            "page" => $currentPage
            )
          );
        $page = $paginator->getPaginate();
        $data = array();
        foreach ($page->items as $m) {
         $_skul = School::findFirst("id='".$m->school."'");
         $data[] = array(
          'id' => $m->id,
          'fname' => $m->first_name,
          'lname' => $m->last_name,
          'email' => $m->email,
          'username' => $m->username,
          'birthday' => date("F j, Y", $m->birthday),
          'gender' => $m->gender,
          'school' => $_skul->name,
          'grade' => $m->grade,
          'status' => $m->status,
          'userrole' => $m->task,
          'profile' => $m->profile_pic_name
          );
       }
       $p = array();
       for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
      }
      echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function ListAllEUserAction($num, $page, $keyword) {
      if ($keyword == 'null' || $keyword == 'undefined') {
        $userlist = Users::find(array(" (task='leader' or task='mentor' or task='teacher') "));
      } else {
        $conditions = " 
        (task='leader' or task='mentor' or task='teacher')
        and 
        (username LIKE '%" . $keyword . "%' 
        or first_name LIKE '%" . $keyword . "%'
        or last_name LIKE '%" . $keyword . "%'
        or  email LIKE '%" . $keyword . "%')";
        $userlist= Users::find(array($conditions,"order"=>"last_name"));
      }

      $currentPage = (int) ($page);
      $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
          "data" => $userlist,
          "limit" => 10,
          "page" => $currentPage
          )
        );
      $page = $paginator->getPaginate();
      $data = array();
      foreach ($page->items as $m) {
       $_skul = School::findFirst("id='".$m->school."'");

       if($m->status==0 && $m->review==0){
        $_pendinguser = true;
       }else{
        $_pendinguser = false;
       }


       $data[] = array(
        'id' => $m->id,
        'fname' => $m->first_name,
        'lname' => $m->last_name,
        'email' => $m->email,
        'username' => $m->username,
        'birthday' => date("F j, Y", $m->birthday),
        'gender' => $m->gender,
        'school' => $_skul->name,
        'grade' => $m->grade,
        'status' => $m->status,
        'userrole' => $m->task,
        'profile' => $m->profile_pic_name,
        'pending' => $_pendinguser
        );
     }
     $p = array();
     for ($x = 1; $x <= $page->total_pages; $x++) {
      $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
  }

  public function deleteUserAction($id){
    $dlt = Users::findFirst('id="' . $id . '"');
    $username = $dlt->username;
    if ($dlt) {
      if($dlt->delete()){
        $data = array('success' => 'Photo has Been deleted');
        $audit = new CB();
        $audit->auditlog(array(
          "module" =>"User", 
          "event" => "Delete", 
          "title" => "Delete User : ". $username ."" 
          ));
      }else {
        $data = array('error' => 'Not Found');
      }
    }
  }

  public function userInfoction($id) {
    $getInfo = Users::findFirst('id="'. $id .'"');
    $data = array(
      'id' =>  $getInfo->id,
      'username' =>  $getInfo->username,
      'email' =>  $getInfo->email,
      'userrole' =>  trim($getInfo->task),
      'fname' =>  $getInfo->first_name,
      'lname' =>  $getInfo->last_name,
      'bday' =>  $getInfo->birthday,
      'gender' => $getInfo->gender,
      'status' => $getInfo->status,
      'banner' => $getInfo->profile_pic_name
      );
    echo json_encode($data);
  }
  public function userUpdateAction() {
    $request = new \Phalcon\Http\Request();
    if($request->isPost()){
      $id         = $request->getPost('id');
      $username   = $request->getPost('username');
      $email      = $request->getPost('email');
      $password   = $request->getPost('password');
      $userrole   = $request->getPost('userrole');
      $firstname  = $request->getPost('fname');
      $lastname   = $request->getPost('lname');
      $bday       = $request->getPost('bday');
      $hiddendate = $request->getPost('hiddendate');
      $gender     = $request->getPost('gender');
      $status     = $request->getPost('status');
      $banner     = $request->getPost('banner');
      $grade      = $request->getPost('grade');
      $oldpassword= $request->getPost('oldpassword');
      $newpassword= $request->getPost('newpassword');
      $password_c = $request->getPost('password_c');
      $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
      if(strlen($bday) <= 11){
        $d = $hiddendate;
      }else{
        $dates = explode(" ", $bday);
        $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
      }

      if($oldpassword != '' && sha1($newpassword) == sha1($password_c)){
        $getInfo = Users::findFirst('id="'. $id .'" and password="'. sha1($oldpassword).'"');
        if(strlen($newpassword)<5){
          $data=array('success' => 'TOOSHORT');
        }
        elseif($getInfo){
          $usave = Users::findFirst('id="' . $id . '"');
          $usave->username        = $username;
          $usave->email           = $email;
          $usave->task            = $userrole;
          $usave->first_name      = $firstname;
          $usave->last_name       = $lastname;
          $usave->birthday        = $d;
          $usave->gender          = $gender;
          $usave->status          = $status ;
          $usave->grade           = $grade ;
          $usave->profile_pic_name          = $banner ;
          $usave->password        = sha1($newpassword);

          if(!$usave->save()){
            $errors = array();
            foreach ($usave->getMessages() as $message) {
              $errors[] = $message->getMessage();
            }
            $data[]=array('error' => $errors);
          }else{
            $data=array('success' => 'UPDATED');
            $audit = new CB();
            $audit->auditlog(array("module" =>"User", "event" => "Update", "title" => "Update User : ". $username .""));
          }
        }
        else{
          $data=array('success' => 'OLDPASSWORDNOTMATCH');
        }
      }
      elseif($oldpassword != '' && sha1($newpassword) != sha1($password_c)){
        $data=array('success' => 'CONFIRMPASSWORDNOTMATCH');
      }
      else{
        $usave = Users::findFirst('id="' . $id . '"');
        $usave->username        = $username;
        $usave->email           = $email;
        $usave->task            = $userrole;
        $usave->first_name      = $firstname;
        $usave->last_name       = $lastname;
        $usave->birthday        = $d;
        $usave->gender          = $gender;
        $usave->status          = $status ;
        $usave->profile_pic_name= $banner ;
        $usave->grade           = $grade ;
        $usave->password        = sha1($newpassword);

        if(!$usave->save()){
          $errors = array();
          foreach ($usave->getMessages() as $message) {
            $errors[] = $message->getMessage();
          }
          $data[]=array('error' => $errors);
        }else{
          $data=array('success' => 'UPDATED');
          $audit = new CB();
          $audit->auditlog(array( "module" =>"User", "event" => "Update", "title" => "Update User : ". $username ."" ));                
        }
      }   
    }
    echo json_encode($data);
  }


  public function changestatusAction($id,$status){
    $getInfo = Users::findFirst('id="'. $id .'"');
    $username = $getInfo->username;
    if($status == 1){
     $getInfo->status = 0;
     $getInfo->save();
     $data=array('success' => 'Deactivated');
     $audit = new CB();
     $audit->auditlog(array("module" =>"User","event" => "Deactivated", "title" => "Deactivated User : ". $username ."" ));
   }
   else{
     $getInfo->status = 1;
     $getInfo->save();
     $data=array('success' => 'Activated');
     $audit = new CB();
     $audit->auditlog(array(
      "module" =>"User", 
      "event" => "Activated", 
      "title" => "Activated User : ". $username ."" 
      ));
   }
   echo json_encode($data);
 }
 public function centerinfoAction($userid){
   $center = Center::find('manager="'. $userid . '"');

   foreach ($center as $m) {
    $data[] = array(
      'centerid' => $m->centerid,
      'centertitle'=>$m->title,
      );


  }
  echo json_encode($data);
}

public function forgotpasswordAction($email) {
 $data = array();
 $NMSemail = $email;

 $SubsEmail = Users::findFirst("email='" . $NMSemail . "'");
 $username = $SubsEmail->username;
 if ($SubsEmail == true) 
 {
  $a = '';
  for ($i = 0; $i < 6; $i++) {
    $a .= mt_rand(0, 9);
  }
  $token = sha1($a);

  $forgotEmail = Forgotpasswords::findFirst("email='" . $NMSemail . "'");
  if ($forgotEmail == true) 
  {
    if ($forgotEmail->delete()) {

    }
  }
  $forgotpassword = new Forgotpasswords();
  $forgotpassword->assign(array(
    'email' => $NMSemail,
    'token' => $token,
    'date' => date('y-m-d')
    ));
  if (!$forgotpassword->save()) 
  {
    $data['msg'] = "Something went wrong saving the data, please try again.";

  } 
  else {
                        //mail
   $dc = new CB();
   $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">You&#39;re receiving this e-mail because you requested a password reset for your account at Power Brain Education.
   <br>
   <br>
   Please click the Reset Password link and choose a new password:
   <br>
   <a href="http://www.powerbraineducation.com/pbeadmin/forgotpassword/changepassword/'.$NMSemail.'/'.$token.'" target="_blank">Reset Password</a>
   <br>
   <br>
   <br></div>';

   $send = $dc->sendMail($NMSemail,'Power Brain Education : Password Reset',$body);
   $data['msg'] = 'Password Reset has been sent to your Email '. $NMSemail;

                        //START Log
   $audit = new CB();
   $audit->auditlog(array(
    "module" =>"User", /*//Examaple News, Create Center, Slider, Events etc...*/
    "event" => "Forgot Password", /*//Example ADD , EdIT , Delete ,View Details etc...*/
    "title" => "User : ". $username ."" /*// Maybe some info here (confuse) XD*/
    ));
                        //END Audit Log
 }
}
else
{
  $data = array('msg' => 'No account found with that email address.');

}
echo json_encode($data);
}

public function checktokenAction($email,$token) {
 $data = array();
 $forgottoken = Forgotpasswords::findFirst("token='" . $token . "' and email ='". $email ."'");
 if ($forgottoken == true) 
 {
  $data['msg'] = 'valid';
}
else
{
  $data['msg'] = 'invalid';
}
echo json_encode($data);
}

public function updatepasswordtokenAction() {

  $email = $_POST['email'];
  $token = $_POST['token'];
  $repassword = $_POST['repassword'];

  $data = array();
  $emailcheck = Users::findFirst("email='" . $email . "'");
  $username = $emailcheck->username;
  if ($emailcheck == true) 
  {
    $emailcheck->password = sha1($_POST['repassword']);
    if (!$emailcheck->save()) {
      $data['error'] = "Something went wrong saving the data, please try again.";
    } else 
    {

     $forgotEmail = Forgotpasswords::findFirst("token='" . $token . "'");
     if ($forgotEmail == true) 
     {
      if ($forgotEmail->delete()) {
                             //START Log
        $audit = new CB();
        $audit->auditlog(array(
          "module" =>"User", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Forgot Password", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Password Updated User : ". $username ."" /*// Maybe some info here (confuse) XD*/
          ));
                        //END Audit Log
      }
    }
    $data['msg'] = "Password Change Success.";
  }
}
else
{
}
echo json_encode($data); 
}



}
<?php

namespace Controllers;
use \Models\Mentorsvid as Mentorsvid;

class MentorsController extends \Phalcon\Mvc\Controller {
    public function SaveVideoAction(){
       $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $vidtitle = $request->getPost('title');
            $embed = $request->getPost('embed');
            $byspace = explode(" ", $embed);
            $byslash = explode("/", $byspace[3]);
            $utubevidid = trim(str_replace('"',' ', $byslash[4]));

           var_dump($request->getPost());

            $guid = new \Utilities\Guid\Guid();
            $guid->GUID();
            $saveembed = new Mentorsvid();
            $saveembed->assign(array(
                'idno'  => $guid->GUID(),
                'title' => $vidtitle,
                'vidid' => $utubevidid,
                'embed' => $embed
                ));
            if (!$saveembed->save()) {
                $errors = array();
                foreach ($saveembed->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            } else {
                $data[]=array('success' => "New Mentors Video Has Been Save");
            }
        }
        echo json_encode($data);
    }

    public function ListVideoAction() {
        $getimages = Mentorsvid::find();
        if(count($getimages) == 0){
                $data['error']=array('No data Available');
        }else{
            foreach ($getimages as $getimages) {
                $data[] = array(
                    'idno'=>$getimages->idno,
                    'title'=>$getimages->title,
                    'embedid'=>$getimages->vidid,
                    'embed'=>$getimages->embed
                );
            }
        }
        echo json_encode($data);
    }

    public function deletevideoAction($vidid) {
        $vid = Mentorsvid::findFirst('idno ="'. $vidid.'"');
        if ($vid) {
            if ($vid->delete()) {
                $data[]=array('success' => "");
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data );
    }

}
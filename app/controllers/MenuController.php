<?php
namespace Controllers;
use \Models\Menumain as Menumain;
use \Models\Menuconstant as ParentSub;
use \Models\Pages as Pages;
use \Controllers\ControllerBase as CB;
class MenuController extends \Phalcon\Mvc\Controller {

    public function menuMainAction(){
        $menuMain = Menumain::find();
        foreach ($menuMain as $m) {
            $data[] = array(
                'id' => $m->id,
                'parentmenu' => $m->title,
                'slug' => $m->slug,
                'subslug' => $m->mainsub,
                );
        }
        echo json_encode($data);
    }

     public function parentTitleAction($slug) {
        $menuMain = Menumain::findFirst(array("slug='".$slug."'"));
        $data = array('title' =>  $menuMain->title);
        echo json_encode($data);
    }


    public function showSubAction($id){
     $menuSub = Pages::find(array("mainmenu ='".$id."'" ));
     if(count($menuSub)!=0){
       foreach ($menuSub as $m) {
        if($m->submenu==null){
           $data[] = array(
            'title' => $m->title,
            'slug' => $m->pageslugs,
            'pageid' => $m->pageid,
            'parentmenu' => $m->parentmenu,
            'submenu' => $m->submenu,
            'title' => $m->title,
            );
       }
   }
}else{
    $data[] = array("error"=>"Saub Pages Note Found");
}
echo json_encode($data);
}
public function menuSubAction($slug) {
    $menuSub = ParentSub::find(array("parentmenu='".$slug."'"));
    foreach ($menuSub as $m) {
        $data[] = array(
            'parentmenu' => $m->parentmenu,
            'submenu' => $m->submenu,
            'title' => $m->title,
            );
    }
    echo json_encode($data);
}
public function viewPagesAction($slug,$page,$others) {
        //GET MAIN MENU ID
    $getMainid=Menumain::findFirst(array("slug='".$slug."'"));
    $mainId = $getMainid->id;
        //GET PAGES
    $getPages=Pages::findFirst(array("pageslugs='".$page."'"));
    $data = array(
        'shortdesc' => $getPages->shortdesc,
        'datapage' => $getPages->body
        );
    echo json_encode($data);

}

public function viewsubPagesAction($slug,$page,$others) {
        //GET PAGES
    $getPages=Pages::find(array("submenu='".$page."' ORDER BY suborder ASC"));
    foreach ($getPages as $getPages) {
        $data[] = array(
            'title' => $getPages->title,
            'slugs' => $getPages->pageslugs,
            'shortdesc' => $getPages->shortdesc,
            'datapage' => $getPages->body,
            'submenu' => $getPages->submenu,
            'banner' => $getPages->banner,
            );
    }
    echo json_encode($data);
}

public function tittlesubPagesAction($slug) {
        //GET PAGES
    $getPages=Pages::findFirst(array("pageslugs='".$slug."'"));
     $data = array(
            'title' => $getPages->title
            );
    echo json_encode($data);
}


public function checkpagesAction($main) {

    $Mainmenu=Menumain::find(array("slug='".$main."'"));
    if(count($Mainmenu)!=0){
    }else{
        $data[]=array('error' => "Redirect Page");
    }


    echo json_encode($data);

}
public function checksubAction($sub) {

 $submenu=Pages::find(array("pageslugs='".$sub."'"));
 if(count($submenu)!=0){
 }else{
    $data[]=array('error' => "Redirect Page");
}





echo json_encode($data);

}

}
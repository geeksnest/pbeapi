<?php

namespace Controllers;
use \Models\Centermap as Centermap;
use \Models\Center as Center;
use \Controllers\ControllerBase as CB;


class CentermapController extends \Phalcon\Mvc\Controller {

    public function centerlocAction($lat, $lon, $centerid){

        $ifexist = Centermap::find(array("centerid='".$centerid."'"));
        $center = Center::find(array("centerid='".$centerid."'"));
        $title = $center->title;

        if(count($ifexist)!=0){
            $save = Centermap::findFirst(array("centerid='".$centerid."'"));
            $save->lat = $lat;
            $save->lon = $lon;
            $save->updated_at = date("Y-m-d H:i:s");
            if (!$save->save()) {
                $errors = array();
               foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else {
                $data[] = array('success' => "Map Updates");
                 //START Log
                $audit = new CB();
                $audit->auditlog(array(
                  "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
                  "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                  "title" => "Update Center Map location - ".$title." ", /*// Maybe some info here (confuse) XD*/
                  ));
                //END Audit Log
            }
        }else{
            $guid = new \Utilities\Guid\Guid();
            $acti = new Centermap();
            $acti->assign(array(
                'mapid'   =>$guid->GUID(),
                'centerid' => $centerid,
                'lat' => $lat,
                'lon' => $lon,
                'created_at'  =>date("Y-m-d H:i:s"),
                'updated_at'  =>date("Y-m-d H:i:s")
                ));
            if (!$acti->save()) {
               $errors = array();
               foreach ($acti->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            } else {
                $data[] = array('success' => "Map Save");
                 //START Log
                $audit = new CB();
                $audit->auditlog(array(
                  "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
                  "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                  "title" => "Update Center Map location - ".$title." ", /*// Maybe some info here (confuse) XD*/
                  ));
                //END Audit Log
            }
        }
    echo json_encode($data = count($ifexist)."/".$centerid); 
}
public function getlocAction($centerid) {

     $ifexist = Centermap::findFirst(array("centerid='".$centerid."'"));
      
        $data=array(
        'lat' => $ifexist->lat,
        'lon' => $ifexist->lon
        );
    echo json_encode($data );
}

}

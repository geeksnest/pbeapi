<?php

namespace Controllers;
use \Models\Scheduleappointment as Scheduleappointment;
use \Models\Notifications as Notifications;
use \Models\Repliedmessage as Message;
use \Controllers\ControllerBase as CB;

class ScheduleappointmentController extends \Phalcon\Mvc\Controller {

    public function listappointmentAction($num, $page, $keyword ,$center) {

        if ($keyword == 'undefined' &&  $center == 'undefined') {
            $appointment = Scheduleappointment::find(array("order" => "status"));
        }
        elseif($center != 'undefined' && $keyword == 'undefined'){
            $appointment = Scheduleappointment::find(array("center = '" . $center . "'"));
        }
        elseif($center == 'undefined' && $keyword != 'undefined') {
            $conditions = "email LIKE '%" . $keyword . "%' OR 
                          center LIKE '%" . $keyword . "%'  OR 
                          datesched LIKE '%" . $keyword . "%'  OR
                          timesched LIKE '%" . $keyword . "%'  OR  
                          fname LIKE '%" . $keyword . "%'  OR 
                          lname LIKE '%" . $keyword . "%'  OR 
                          datesubmitted LIKE '%" . $keyword . "%'";
            $appointment = Scheduleappointment::find(array($conditions));
        }
        else{
             $conditions = "email LIKE '%" . $keyword . "%' AND center = '". $center ."' OR 
                          datesched LIKE '%" . $keyword . "%' AND center = '". $center ."' OR
                          timesched LIKE '%" . $keyword . "%' AND center = '". $center ."' OR  
                          fname LIKE '%" . $keyword . "%' AND center = '". $center ."' OR 
                          lname LIKE '%" . $keyword . "%' AND center = '". $center ."'  OR 
                          datesubmitted LIKE '%" . $keyword . "%' AND center = '". $center ."'";
            $appointment = Scheduleappointment::find(array($conditions));

        }

        $currentPage = (int) ($page);

            // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $appointment,
                "limit" => 10,
                "page" => $currentPage
                )
            );

            // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'fname' => $m->fname,
                'lname' => $m->lname,
                'email' => $m->email,
                'center' => $m->center,
                'datesched' => $m->datesched,
                'timesched' => $m->timesched,
                'content' => $m->content,
                'status' => $m->status,
                'datesubmitted' => $m->datesubmitted
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function appointmentdeleteAction($id) {
        $data = $id;
        $request = Scheduleappointment::findFirst('id="'. $id.'"');
        $Notifications = Notifications::findFirst('id="'. $id.'"');
        $fname = $request->fname;
        $lname = $request->lname;
        // $data = array('error' => 'Not Found');
        if ($request) {
            if ($request->delete()) {
                $Notifications->delete();
                $data = array('success' => 'appointment Deleted');
                $deletemessage = Message::find('rid="'.$id.'"');
                $deletemessage->delete();
                //START Log
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Appointment", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "".$fname." ".$lname." Appointment Deleted" /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            }
        }
        echo json_encode($data);
    }

    public function listreplyAction($id){
        $data = array();
        $getreply= Message::find('rid="' . $id . '" ');
        foreach ($getreply as $getreply) {
            $data[] = array(
                'id'=>$getreply->id,
                'rid'=>$getreply->rid,
                'message'=>$getreply->message,
                'date'=>$getreply->date
                );
        }
        echo json_encode($data);

    }

    public function viewappointmentAction($id) {

        $data = array();
        $viewappointment = Scheduleappointment::findFirst('id="' . $id .'"');
        if ($viewappointment) {
            $viewappointment = array(                
                'id' => $viewappointment->id,
                'fname' =>$viewappointment->fname,
                'lname' =>$viewappointment->lname,
                'email' =>$viewappointment->email,
                'center' =>$viewappointment->center,
                'datesched' =>$viewappointment->datesched,
                'timesched' =>$viewappointment->timesched,
                'content' =>$viewappointment->content,
                'status' => $viewappointment->status,
                'datesubmitted' =>$viewappointment->datesubmitted
                );
        }
        echo json_encode($viewappointment);

        $read = Scheduleappointment::findFirst('id="' . $id .'"');
        $Notifications = Notifications::findFirst('id="'. $id.'"');
        $fname = $read->fname;
        $lname = $read->lname;
        if($read->status == 0){
            $read->status = 1;
            $Notifications->status = 1;
            $Notifications->save();
            //START Log
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Appointment", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Review", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "".$fname." ".$lname." Appointment Reviewed", /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            if (!$read->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
        }
    }

            //reply PROPOSAL
    public function replyappointmentAction() {
        $data = array();

        $id = $_POST['rid'];
        $appointment = Scheduleappointment::findFirst('id="' . $id .'"');
        $Notifications = Notifications::findFirst('id="'. $id.'"');
        $appointment->status = 2;
        $Notifications->status = 2;
        $fname = $request->fname;
        $lname = $request->lname;
        $Notifications->save();
        if (!$appointment->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } 
        else {

                ///MAIL 
                $dc = new CB();
                $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">This is the reply to your message: "'.$_POST['content'].'"</div>' . '<br><br> Admin Reply: ' .$_POST['messages'];
                $send = $dc->sendMail($_POST['email'],'Power Brain Education : Reply',$body);

                $repliedmessage = new Message();
                $repliedmessage->assign(array(
                        'rid'  => $id,
                        'message' => $_POST['messages'],
                        'date' => date('Y-m-d')
                        ));
                if (!$repliedmessage->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                }else{
                        $data['success'] = "Success";
                        //START Log
                        $audit = new CB();
                        $audit->auditlog(array(
                            "module" =>"Appointment", /*//Examaple News, Create Center, Slider, Events etc...*/
                            "event" => "Reply", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                            "title" => "".$fname." ".$lname." Appointment Replied", /*// Maybe some info here (confuse) XD*/
                            ));
                        //END Audit Log
                }
        }
        echo json_encode($data);
    }


}
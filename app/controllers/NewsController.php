<?php

namespace Controllers;

use \Models\News as News;
use \Models\Centernews as Centernews;
use \Models\Newsimage as Newsimage;
use \Models\Newscategory as Newscategory;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class NewsController extends \Phalcon\Mvc\Controller {

    public function createNewsAction() {



        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){


            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $date = $request->getPost('date');
            $body = $request->getPost('body');
            $banner = $request->getPost('banner');
            $category = $request->getPost('category');
            

            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates = explode(" ", $date);
            $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];


            $page = new News();
            $page->assign(array(
                'title' => $title,
                'newsslugs' => $slugs,
                'author' => $author,
                'body' => $body,
                'banner' => $banner,
                'category' => $category,
                'date' => $d,
                'status' => 1,
                'views' => 1,
                'type' => 'News'
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";
                //START Log
                  $audit = new CB();
                  $audit->auditlog(array(
                      "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Add Center news- ".$title." ", /*// Maybe some info here (confuse) XD*/
                      ));
                 //END Audit Log
            
            }

        }

        echo json_encode($data);
           


    }



    public function createNewsCenterAction() {



        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){


            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $date = $request->getPost('date');
            $body = $request->getPost('body');
            $banner = $request->getPost('banner');
            $category = $request->getPost('category');
            

            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates = explode(" ", $date);
            $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];


            $page = new Centernews();
            $page->assign(array(
                'title' => $title,
                'newsslugs' => $slugs,
                'author' => $author,
                'body' => $body,
                'banner' => $banner,
                'date' => $d,
                'status' => 1,
                'views' => 1,
                'type' => 'News'
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";
                //START Log
                  $audit = new CB();
                  $audit->auditlog(array(
                      "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Add Center news- ".$title." ", /*// Maybe some info here (confuse) XD*/
                      ));
                //END Audit Log
            
            }

        }

        echo json_encode($data);
           


    }




    public function saveimageAction($filename) {

        $picture = new Newsimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
            //START Log
                  $audit = new CB();
                  $audit->auditlog(array(
                      "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Add Center news Image- ".$filename." ", /*// Maybe some info here (confuse) XD*/
                      ));
            //END Audit Log
        }
     
    }

    //IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = Newsimage::findFirst('id="'. $imgid.'"');
        $filename=$img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                 //START Log
                  $audit = new CB();
                  $audit->auditlog(array(
                      "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Delete Center news Image- ".$filename." ", /*// Maybe some info here (confuse) XD*/
                      ));
                //END Audit Log
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }

    public function listimageAction() {

        $getimages = Newsimage::find(array("order" => "id DESC"));
         if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) 
                {
                    $data[] = array(
                        'id'=>$getimages->id,
                        'filename'=>$getimages->filename
                        );
                }
         }
        echo json_encode($data);

    }

    public function listcategoryAction()
    {

        $getcategory= Newscategory::find(array("order" => "id ASC"));
        foreach ($getcategory as $getcategory) {
            $data[] = array(
                'id'=>$getcategory->id,
                'name'=>$getcategory->name
                );
        }
        echo json_encode($data);

    }


    public function managepagesAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Pages::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
            $Pages = Pages::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'pageid' => $m->pageid,
                'title' => $m->title,
                'pageslugs' => $m->pageslugs,
                'status' => $m->status
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function pageUpdatestatusAction($status,$pageid,$keyword) {

        $data = array();
        $page = Pages::findFirst('pageid=' . $pageid . ' ');
        $page->status = $status;
            if (!$page->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
                //START Log
                  $audit = new CB();
                  $audit->auditlog(array(
                      "module" =>"Update", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Update Page- ".$filename." ", /*// Maybe some info here (confuse) XD*/
                      ));
                //END Audit Log
            }

            echo json_encode($data);
    }

    public function pagedeleteAction($pageid) {
        $conditions = "pageid=" . $pageid;
        $pages = Pages::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    public function pageeditoAction($pageid) {
        $data = array();
        $leftbaritem = array();
        $rightbaritem = array();

        $pages = Pages::findFirst("pageid=" . $pageid);
        $leftbar = Pageleftbar::findFirst("pageid=" . $pageid);
         if ($leftbar) {
            $leftbaritem = array(
                'leftbar' => $leftbar->item
                );
        }

        $rightbar= Pagerightbar::find("pageid=" . $pageid);
         foreach ($rightbar as $rightbar) {;
            $rightbaritem[] = array(
                'rightbar'=>$rightbar->item
                );
        }
        
        if ($pages) {
            $data = array(
                'pageid' => $pages->pageid,
                'title' => $pages->title,
                'slugs' => htmlentities($pages->pageslugs),
                'body' => $pages->body,
                'layout' => $pages->pagelayout,
                'leftbaritem' => $leftbaritem,
                'rightbaritem' => $rightbaritem
                );
        }
        echo json_encode($data);
    }

    public function saveeditedPagesAction() {

        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){

            $pageid = $request->getPost('pageid');
            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $body = $request->getPost('body');
            $layout = $request->getPost('layout');
            $leftbar = $request->getPost('leftbar');
            $rightbar = $request->getPost('rightbar');

            $pages = Pages::findFirst('pageid=' . $pageid . ' ');
            $pages->title = $title;
            $pages->pageslugs = $slugs;
            $pages->body = $body;
            $pages->pagelayout = $layout;

            if (!$pages->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {

                


                $conditions = "pageid=" . $pageid;
                $rightbaritem = pagerightbar::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($rightbaritem) {
                    if ($rightbaritem->delete()) {
                        $data = array('success' => 'Page Deleted');
                    }
                }


                $tags = array(); 
                $tags = $rightbar;
                foreach($tags as $tag){
                    $pageright = new Pagerightbar();
                    $pageright->assign(array(
                        'pageid' => $pageid,
                        'item' => $tag
                        ));

                    if (!$pageright->save()) {
                        $data['error'] = "Something went wrong saving the Right Sidebar, please try again.";
                    } else {

                        $data['success'] = "Success";
                    }
                }


                 $conditions = "pageid=" . $pageid;
                $leftbaritem = Pageleftbar::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($leftbaritem) {
                    if ($leftbaritem->delete()) {
                        $data = array('success' => 'Page Deleted');
                    }
                }

                $pageleft = new Pageleftbar();
                $pageleft->assign(array(
                    'pageid' => $pageid,
                    'item' => $leftbar
                    ));

                if (!$pageleft->save()) {
                    $data['error'] = "Something went wrong saving the Left Sidebar, please try again.";
                } else {
                    $data['success'] = "Success";
                }
              


            }
            echo json_encode($data);



        }
    }


    
    
}
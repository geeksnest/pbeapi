<?php

namespace Controllers;

use \Models\Auditlog as Auditlog;
use \Models\Users as Users;
use \Controllers\ControllerBase as CB;

class AuditlogController extends \Phalcon\Mvc\Controller {

    public function listauditslogAction($num, $page, $keyword){
        if ($keyword == 'undefined') {
            $logs = Auditlog::find(array("order"=>"datetime desc"));
        } else {
            $conditions = "datetime LIKE '%" . $keyword . "%' 
            or module LIKE '%" . $keyword . "%'
            or title LIKE '%" . $keyword . "%'";
            $logs= Auditlog::find(array($conditions,"order"=>"datetime desc"));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $logs,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $logs = Users::findFirst("id ='".$m->userid."'");
            $idofuser = $logs->username;
            $data[] = array(
                'logid' => $m->logid,
                'datetime' => $m->datetime,
                'userid' => $m->userid,
                'username' => $idofuser,
                'module' => $m->module,
                'event' => $m->event,
                'title' => $m->title
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }
    public function deleteauditslogAction()
    {
        $logs = Auditlog::find();
        $logs->delete();
    }

}
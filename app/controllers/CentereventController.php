<?php

namespace Controllers;
use \Models\Centerevent as Centerevent;
use \Controllers\ControllerBase as CB;
use \Models\Center as Center;

class CentereventController extends \Phalcon\Mvc\Controller {

    public function addeventAction(){
       $data = array();
     /*$month = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => 10, 'Nov' => 11, 'Dec' => 12);
     $datefrom = explode(" ", $_POST['dfrom']);
     if($_POST['dto'] == ""){
        $datetto = explode(" ", $_POST['dfrom']);
    }else{
        $datetto = explode(" ", $_POST['dto']);
    }*/

    
    // $ft = array('00' => 12, '01' => 01, '02' => 02, '03' => 03, '04' => 04, '05' => 05, '06' => 06, '07' => 07, '08' => 08, '09' => 09, '10' => 10, '11' => '11', '12' => 12, '13' => 1, '14' => 2, '15' => 3, '16' => 4, '17' => 5, '18' => 6, '19' => 7, '20' => 8, '21' => 9, '22' => 10, '23' => 11);
    // if($_POST['mytime'] == ""){
    //     $mytime = "";
    // }else{
    //     $mt2 = explode(" ", $_POST['mytime']);
    //     $mt = explode(":", $mt2[4]);
    //     if($mt[0] > 12){
    //         $tx = 'PM';
    //     }else{
    //         $tx = 'AM';
    //     }
    //     $mytime = $ft[$mt[0]].':'.$mt[1].' '.$tx;
    // }
    
   // $datef1 = $datefrom[3].'-'.$month[$datefrom[1]].'-'.$datefrom[2];
   // $datet1 = $datetto[3].'-'. $month[$datetto[1]].'-'.$datetto[2];

    $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
    $dates = explode(" ", $_POST['dfrom']);
    $dates1 = explode(" ", $_POST['dto']);
    //$d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];


    /*$datef1 = str_replace('00:00:00', $_POST['mytime'], $_POST['dfrom']);
    $datet1 = str_replace('00:00:00', $_POST['mytime'], $_POST['dto']);*/
    $datef1 = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
    $datet1 = $dates1[3].'-'.$mont0[$dates1[1]].'-'.$dates1[2];

    $start = strtotime($datefrom[4].'-'.$month[$datefrom[1]].'-'.$datefrom[3]);
    $end = strtotime($datetto[3].'-'.$month[$datetto[1]].'-'.$datetto[2]);
    $days_between = ($end - $start) / 86400;

    if($_POST['centerid']=="" || $_POST['centerid']==null){
        $centerid = "global";
    }else{
        $centerid = $_POST['centerid'];
    }

    if ($days_between >= 0){
        $guid = new \Utilities\Guid\Guid();
        $acti = new Centerevent();
        $acti->assign(array(
            'eventid'   => $guid->GUID(),
            'centerid'  => $centerid,
            'title'     => $_POST['title'],
            'location'  => $_POST['loc'],
            'datefrom'  => $datef1,
            'dateto'    => $datet1,
            'time'      => $_POST['mytime'],
            'timeto'    => $_POST['mytimend'],
            'body'      => $_POST['body'],
            'desc'      => $_POST['shortdesc'],
            'type'      => $_POST['type'],
            'lat'       => $_POST['lat'],
            'lon'       => $_POST['lon'],
            'created_at'=> date("Y-m-d H:i:s"),
            'updated_at'=> date("Y-m-d H:i:s"),
            ));
        if (!$acti->save()) {
         $errors = array();
         foreach ($acti->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data['error']=array('error' => $errors);
    } else {
        $data['success'] = "Success";
        //START Log
        $audit = new CB();
        $audit->auditlog(array(
          "module" =>"Center event", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Add Center event - ".$title." ", /*// Maybe some info here (confuse) XD*/
          ));         
        //END Audit Log
    }
}else{
    $data = array('ErrDate' => 'Invalid Date.');
}

echo json_encode($data); 

}
public function listviewAction() {
    $viewcal = Centerevent::find(array("order" => "datefrom DESC"));
    $viewcalendar = json_encode($viewcal->toArray(), JSON_NUMERIC_CHECK);
    echo $viewcalendar;
}
public function viewcalendarAction() {
    $calac = Centerevent::find();
    if(count($calac)!=0){
        foreach ($calac as $c){

            $df = explode("-", $c->datefrom);
            $dt = explode("-", $c->dateto);
            $mt = explode(":", $c->time);
            $mt2 = explode(" ", $mt[1]);
            $fm = array(
                1 => 'January', 2 => 'February', 
                3 => 'March', 4 => 'April', 
                5 => 'May', 6 => 'June', 
                7 => 'July', 8 => 'August', 
                9 => 'September', 10 => 'October', 
                11 => 'November', 12 => 'December',
                );

            $center = Center::findFirst('centerid="'. $c->centerid.'"');
            if($center){
                $cname =  $center->title;
            }else{
                $cname = '';
            }



            $data[] = array(   
                'eventid' => $c->eventid,
                'centerid' => $c->centerid,
                'centername' => $cname,          
                'title' => $c->title,
                'loc' => $c->location,
                'type' => $c->type,
                'fyea' => $df[0], 'fmon' => $df[1], 'fday' => $df[2],
                'tyea' => $dt[0], 'tmon' => $dt[1], 'tday' => $dt[2],
                'fyea2' => $df[0], 'fmon2' => $fm[$df[1]], 'fday2' => $df[2],
                'tyea2' => $dt[0], 'tmon2' => $fm[$dt[1]], 'tday2' => $dt[2],
                'hhr' => $mt[0], 'hmin' => $mt2[0], 'hap' => $mt2[1],
                'body' => $c->body,
                'info' => $c->desc,
                'datef' => $c->datefrom,
                'datet' => $c->dateto,
                'mytime'=> $c->time,
                'endtime'=> $c->timeto,

                );
        }

    }else{
        $data = array("error" => "null");
    }
    echo json_encode($data);
}
public function centercalendarAction($centerid) {
    $calac = Centerevent::find(array("centerid='".$centerid."' OR centerid='global'"));
    if(count($calac)!=0){
        foreach ($calac as $c){

            $center = Center::findFirst('centerid="'. $c->centerid.'"');
            if($center){
                $cname =  $center->title;
            }else{
                $cname = '';
            }

            $data[] = array(   
                'eventid' => $c->eventid,
                'centerid' => $c->centerid,
                'centername' => $cname,           
                'title' => $c->title,
                'loc' => $c->location,
                'type' => $c->type,
                'body' => $c->body,
                'info' => $c->desc,
                'datef' => $c->datefrom,
                'datet' => $c->dateto,
                'mytime'=>$c->time,
                'mytimend'=>$c->timeto,
                ); 
        }
    }else{
        $data = array("error" => "null");
    }
    echo json_encode($data);
}
public function activityinfoAction($eventid) {
    $ca = Centerevent::findFirst("eventid='".$eventid."'");
    $data = array();
    if ($ca) {

        $mt = explode(" ", $ca->time);
        $mt2 = explode(":", $mt[0]);
        if ($mt[1] == 'PM') {
            $hrr = $mt2[0] + 12;
            $mytime2 = $hrr.':'.$mt2[1].':00';
        }else{
            $mytime2 = date('Y-m-d').' '.$mt[0].':00';
        }


        $df = explode(" ", $ca->datefrom);
        $dt = explode(" ", $ca->dateto);

        $fm = array(
            'Jan' => '01', 'Feb' => '02', 
            'Mar' => '03', 'Apr' => '04', 
            'May' => '05', 'Jun' => '06', 
            'Jul' => '07', 'Aug' => '08', 
            'Sep' => '09', 'Oct' => '10', 
            'Nov' => '11', 'Dec' => '12'
            );
        $data = array(                
            'eventid' => $ca->eventid,
            'title' => $ca->title,
            'loc' => $ca->location,
            'type' => $c->type,
            'dfrom' => $ca->datefrom,
            'dto' => $ca->dateto,
            'mytime2' => $mytime2,
            'body' => $ca->body,
            'shortdesc' => $ca->desc,
            'type' => $ca->type,
            'lat' => $ca->lat,
            'lon' => $ca->lon,
            'centerid' => $ca->centerid,
            'mytime' => $ca->time,
            'mytimend' => $ca->timeto,
            );
    }
    echo json_encode($data);
}

public function activityUpdateAction(){
    $data = array();

    $mont0 = array('Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12);
    $datef = explode(" ", $_POST['dfrom']);
    $datet = explode(" ", $_POST['dto']);

    $ft = array('00' => 12, '01' => 1, '02' => 2, '03' => 3, '04' => 4, '05' => 5, '06' => 6, '07' => 7, '08' => 8, '09' => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 1, 14 => 2, 15 => 3, 16 => 4, 17 => 5, 18 => 6, 19 => 7, 20 => 8, 21 => 9, 22 => 10, 23 => 11);
    if($_POST['mytime'] == ""){
        $mytime = "";
    }else{
        $mt2 = explode(" ", $_POST['mytime']);
        $mt = explode(":", $mt2[4]);

        if($mt[0] > 12){
            $tx = 'PM';
        }else{
            $tx = 'AM';
        }
        $mytime = $ft[$mt[0]].':'.$mt[1].' '.$tx;
    }


    $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
    $dates = explode(" ", $_POST['dfrom']);
    $dates1 = explode(" ", $_POST['dto']);


    $datet1 = $dates1[3].'-'.$mont0[$dates1[1]].'-'.$dates1[2];
    $datef1 = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

    $start = strtotime($datef[3].'-'.$mont0[$datef[1]].'-'.$datef[2]);
    $end = strtotime($datet[3].'-'.$mont0[$datet[1]].'-'.$datet[2]);
    $days_between = ($end - $start) / 86400;

    if ($days_between >= 0){
        $eventid = $_POST['eventid'];
        $ca = Centerevent::findFirst("eventid='".$eventid."'");

        $ca->title      = $_POST['title'];
        $ca->location   = $_POST['loc'];
        $ca->datefrom   = $datef1;
        $ca->dateto     = $datet1;
        $ca->time       = $_POST['mytime'];
        $ca->timeto     = $_POST['mytimend'];
        $ca->body       = $_POST['body'];
        $ca->desc       = $_POST['shortdesc'];
        if (!$ca->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Center event", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Update Center event - ".$_POST['title']." ", /*// Maybe some info here (confuse) XD*/
              ));         
        //END Audit Log
        }
    }else{
        $data = array('ErrDate' => 'Invalid Date.');
        $data = 1;
    }
    echo json_encode($data);
}


public function eventlocAction($lat, $lon, $eventid){


    $ca = Centerevent::findFirst("eventid='".$eventid."'");
    $eventname = $ca->title;
    $ca->lat = $lat;
    $ca->lon =$lon;

    if (!$ca->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } else {
        $data['success'] = "Success";
        //START Log
        $audit = new CB();
        $audit->auditlog(array(
          "module" =>"Center event", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Update Center event location - ".$eventname." ", /*// Maybe some info here (confuse) XD*/
          ));         
        //END Audit Log
    }
    echo json_encode($data);
    
}
public function activitydeleteAction($eventid) {
 $ca = Centerevent::findFirst('eventid="'. $eventid.'"');
 $eventname = $ca->title;
 if ($ca) {
    if ($ca->delete()) {
        $data[]=array('success' => "");
        //START Log
        $audit = new CB();
        $audit->auditlog(array(
          "module" =>"Center event", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Delete Center event - ".$eventname." ", /*// Maybe some info here (confuse) XD*/
          ));         
        //END Audit Log
    }else{
        $data[]=array('error' => '');
    }
}else{
    $data[]=array('error' => '');
}
echo json_encode($data);
}
public function eventlistCenterAction($num, $page, $keyword) {

 $Pages = Centerevent::find(array("centerid = 'global' and (datefrom >= '".date("Y-m-d")."') ORDER BY datefrom ASC"));
 $currentPage = (int) ($page);
 $paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
        "data" => $Pages,
        "limit" => 10,
        "page" => $currentPage
        )
    );
 $page = $paginator->getPaginate();

 $data = array();

 $month = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => 10, 'Nov' => 11, 'Dec' => 12);


 foreach ($page->items as $m) {
   $datefrom = explode(" ", $m->datefrom);
   $dateto = explode(" ", $m->dateto);
   $data[] = array(
    'eventid' => $m->eventid,
    'title' => $m->title,
    'datefrom' => $datefrom[0].' '.$datefrom[1].' '.$datefrom[2].' '.$datefrom[3]  ,
    'dateto' => $dateto[0].' '.$dateto[1].' '.$dateto[2].' '.$dateto[3]  ,
    'time' => $m->time,
    'mytimend' => $m->timeto,
    'location' => $m->location,
    'desc' => $m->desc,
    'body' => $m->body,
    'lat' => $m->lat,
    'lon' => $m->lon,
    'dfrom' => $month[$datefrom[1]].'-'.$datefrom[2],
    'dto' => $month[$dateto[1]].'-'.$dateto[2],

    );
}
$p = array();
for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}
}
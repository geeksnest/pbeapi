<?php

namespace Controllers;
use \Models\Requestinformation as Requestinformation;
use \Models\Scheduleappointment as Scheduleappointment;
use \Models\Notifications as Notifications;
use \Models\Users as Users;
use \Models\Center as Center;
use \Models\Centerevent as Centerevent;
use \Models\Centernews as Centernews;
use \Controllers\ControllerBase as CB;

class DashboardController extends \Phalcon\Mvc\Controller {
    public function dashboardAction(){
     $timeago = new CB();
     $listnewrequest = Requestinformation::find(array("status=0","order"=>"datesubmitted desc"));
     $count = count($listnewrequest);
     foreach ($listnewrequest as $list) 
     {
        $data[] = array(
                'id' => $list->id,
                'fname' => $list->fname,
                'lname' => $list->lname,
                'datesubmitted' => $timeago->time_elapsed_string($list->datecreated)
            );
     }

     $listnewappointment = Scheduleappointment::find(array("status=0","order"=>"datesubmitted desc"));
     $count1 = count($listnewappointment);
     foreach ($listnewappointment as $list) 
     {
        $data1[] = array(
                'id' => $list->id,
                'fname' => $list->fname,
                'lname' => $list->lname,
                'datesubmitted' => $timeago->time_elapsed_string($list->datecreated)
            );
     }

     $countevent = count(Centerevent::find());
     $countnews = count(Centernews::find());
     $countvideo = count(Centernews::find("featuredoption='video'"));


    echo json_encode(array('data'=>$data,'count'=>$count,'data1'=>$data1,'count1'=>$count1,'countevent'=>$countevent,'countnews'=>$countnews,'countvideo'=>$countvideo));
    }



    public function notificationAction($userid){
        $timeago = new CB();
        $user = Users::findFirst('id="'.$userid.'"');

        if($user->task == 'Admin'){
            $notif = Notifications::find(array("order"=>"datecreated DESC","limit"=>50));
            $new = count(Notifications::find("status=0")); 
            $reviewed = count(Notifications::find("status=1"));
            $replied = count(Notifications::find("status=2"));

        }else{
            $center = Center::findFirst('manager="'.$userid.'"');
            $notif = Notifications::find(array("center ='".$center->title."'","order"=>"datecreated DESC","limit"=>50));
            $new = count(Notifications::find('status=0 and center="'.$center->title.'"')); 
            $reviewed = count(Notifications::find('status=1 and center="'.$center->title.'"'));
            $replied = count(Notifications::find('status=2 and center="'.$center->title.'"'));
        }
                $count = count($notif);  
                foreach ($notif as $list) 
                {
                    $data[] = array(
                        'id' => $list->id,
                        'fname' => $list->fname,
                        'lname' => $list->lname,
                        'status' => $list->status,
                        'center' => $list->center,
                        'type' =>$list->type,
                        'timeago' => $timeago->time_elapsed_string($list->datecreated)
                        );
                }

        echo json_encode(array("data"=>$data,"count"=>$count,"countnew"=>$new,"countreviewed"=>$reviewed,"countreplied"=>$replied));

    }
    public function deletenoficationAction($id){
        $notif = Notifications::findFirst("id='".$id."'");

        if ($notif) {
            if ($notif->delete()) {
                $data = array('success' => 'Notification Deleted');
            }
        }
        echo json_encode($data);
    }
    public function changestatusAction($id,$status){
        $notif = Notifications::findFirst("id='".$id."'");
        $Requestinformation = Requestinformation::findFirst('id="' . $id .'"');
        $Scheduleappointment = Scheduleappointment::findFirst('id="' . $id .'"');
        
        if($Requestinformation){
            if($status == 0){
                $notif->status = 1;
                $Requestinformation->status = 1;
                $notif->save();
                $Requestinformation->save();
            }
            else{
                $notif->status = 0;
                $Requestinformation->status = 0;
                 $notif->save();
                $Requestinformation->save();
            } 
        }
        if($Scheduleappointment){
           if($status == 0){
                $notif->status = 1;
                $Scheduleappointment->status = 1;
                 $notif->save();
                $Scheduleappointment->save();
            }
            else{
                $notif->status = 0;
                $Scheduleappointment->status = 0;
                 $notif->save();
                $Scheduleappointment->save();
            }  
        }
    }

    public function listnotifyAction($num,$page,$keyword,$userid){
        $timeago = new CB();
        $user = Users::findFirst('id="'.$userid.'"');


        if($user->task == 'Admin'){
            if ($keyword == 'undefined') {
                $Notifications = Notifications::find(array("order"=>"datecreated DESC","limit"=>50));
            } 
            else {
                $Notifications = Notifications::find("status=$keyword");
            } 
            $new = count(Notifications::find("status=0")); 
            $reviewed = count(Notifications::find("status=1"));
            $replied = count(Notifications::find("status=2"));
        }
        else{
            $center = Center::findFirst('manager="'.$userid.'"');
            if ($keyword == 'undefined') {
                $Notifications = Notifications::find(array("center ='".$center->title."'","order"=>"datecreated DESC","limit"=>50));
            } 
            else {
                $Notifications = Notifications::find(array("status=$keyword and center ='".$center->title."'","order"=>"datecreated DESC","limit"=>50));
            }
            $new = count(Notifications::find('status=0 and center="'.$center->title.'"')); 
            $reviewed = count(Notifications::find('status=1 and center="'.$center->title.'"'));
            $replied = count(Notifications::find('status=2 and center="'.$center->title.'"'));
        }

        $count = count($Notifications);
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Notifications,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $list) {
            $data[] = array(
               'id' => $list->id,
               'fname' => $list->fname,
               'lname' => $list->lname,
               'status' => $list->status,
               'center' => $list->center,
               'type' =>$list->type,
               'timeago' => $timeago->time_elapsed_string($list->datecreated)
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items,"count"=>$count,"countnew"=>$new,"countreviewed"=>$reviewed,"countreplied"=>$replied));


    }

}
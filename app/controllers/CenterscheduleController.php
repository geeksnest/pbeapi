<?php

namespace Controllers;
use \Models\Centerschedule as Centerclass;
use \Models\Center as Center;

use \Controllers\ControllerBase as CB;


class CenterscheduleController extends \Phalcon\Mvc\Controller {

    public function classaddction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $centerid = $request->getPost('centerid');
            $type = $request->getPost('type');
            $day = $request->getPost('day');
            $from = $request->getPost('from');
            $to = $request->getPost('to');
            $userid = $request->getPost('userid');
            if($type=="kids"){
                $bg = "#ed6459";
            }else if($type=="teen"){
                $bg = "#87bf78";
            }else if($type=="adult"){
                $bg = "#79d0dc";
            }



            $guid = new \Utilities\Guid\Guid();
            $class = new Centerclass();
            $class->assign(array(
                'classid'=>$guid->GUID(),
                'centerid' => $centerid,
                'classtype' => $type,
                'classday' => $day,
                'classfrom' => $from,
                'classto' => $to,
                'colorlegend' => $bg,
                ));


            if (!$class->save()) {
                $errors = array();
                foreach ($class->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            } else {
             $data[]=array('success' => "Class has been Created");
             $center=Centerclass::findFirst(array("centerid='".$centerid."'"));
             $centername = $center->title;
            //START Log
             $audit = new CB();
             $audit->auditlog(array(
              "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Add Center Schedule ".$centername." on - ".$day." Class ".$type."", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
         }
     }
     echo json_encode($data );

 }
 public function classupdateaction(){
    $request = new \Phalcon\Http\Request();
    if($request->isPost()){
       $schedid = $request->getPost('schedid');
       $type = $request->getPost('type');
       $day = $request->getPost('day');
       $from = $request->getPost('from');
       $to = $request->getPost('to');
       if($type=="kids"){
        $bg = "#ed6459";
    }else if($type=="teen"){
        $bg = "#87bf78";
    }else if($type=="adult"){
        $bg = "#79d0dc";
    }
    $class = Centerclass::findFirst(array("classid='".$schedid."'"));
    $centerid = $class->centerid;
    $class->assign(array(
        'classtype' => $type,
        'classfrom' => $from,
        'classto' => $to,
        'colorlegend' => $bg,
        ));
    if (!$class->save()) {
        $errors = array();
        foreach ($class->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {
     $data[]=array('success' => "Class has been Updated");
     $center=Centerclass::findFirst(array("centerid='".$centerid."'"));
     $centername = $center->title;

     //START Log
     $audit = new CB();
     $audit->auditlog(array(
        "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
        "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
        "title" => "Update Center Schedule ".$centername." on ".$day." Class ".$type."", /*// Maybe some info here (confuse) XD*/
        ));
     //END Audit Log
 }



}
echo json_encode($data);

}
public function classlistaction($centerid){

    $class = Centerclass::find(array("centerid ='".$centerid."' ORDER BY classtype, STR_TO_DATE(classfrom, '%l:%i %p'), STR_TO_DATE(classto, '%l:%i %p') "));
    if( count($class)!=0){
        foreach ($class as $dbdata){
            $data[] = array(
                'classid' => $dbdata->classid,
                'centerid' => $dbdata->centerid,
                'type' => $dbdata->classtype,
                'day' => $dbdata->classday,
                'from' => $dbdata->classfrom,
                'to' => $dbdata->classto,
                'bg' => $dbdata->colorlegend,
                );
        }
    }else{
        $data= array();
    }
    
    echo json_encode($data );
}
public function classgetaction($schedid){

    $class = Centerclass::find(array("classid ='".$schedid."'  ORDER BY classfrom  ASC"));

    if( count($class)!=0){
        foreach ($class as $dbdata){
            $data = array(
                'schedid'  =>$schedid,
                'type' => $dbdata->classtype,
                'from' => $dbdata->classfrom,
                'to' => $dbdata->classto,
                );
        }
    }else{
        $data= array();
    }
    
    echo json_encode($data );
}
public function classdeleteaction($schedid, $userid) {

    $cls = Centerclass::findFirst('classid="'. $schedid.'"');
    $centerid = $cls->centerid;
    $day = $cls->classday;
    $type = $cls->classtype;
    $center=Centerclass::findFirst(array("centerid='".$centerid."'"));
    $centername = $center->title;
    if ($cls) {
        if ($cls->delete()) {
            $data[]=array('success' => "Schedule has been successfully deleted");
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
                "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                "title" => "Delete Center Schedule ".$centername." on - ".$day." Class ".$type."", /*// Maybe some info here (confuse) XD*/
                ));
            //END Audit Log
        }
    }else{
        $data[]=array('error' => "Seems the News Cant Be Deleted");

    }   
    echo json_encode($data);
}



}
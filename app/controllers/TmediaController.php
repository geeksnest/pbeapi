<?php

namespace Controllers;
use \Models\Tmedia as Tmedia;
use \Models\Epdf as Epdf;
use \Models\Epage as Epage;
use \Models\Submitclass as Submitclass;
use \Models\Pagewdsub as Pagewdsub;
use \Controllers\ControllerBase as CB;
class TmediaController extends \Phalcon\Mvc\Controller {
    public function ListmediaAction($idno) {
        $getimages = Tmedia::find(array("trainingid='".$idno."'", "order" => "created_at ASC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $get) 
            {
                $data[] = array(
                    'idno'=>$get->idno,
                    'trainingid'=>$get->trainingid,
                    'title'=>$get->title,
                    'body'=>$get->body,
                    'vidid'=>$get->vidid,
                    'embed'=>$get->embed,
                    'classfor'=>$get->classfor
                    );
            }
        }
        echo json_encode($data);
    }
    public function UsrListmediaAction($idno, $usrid, $task,$state){
      
        $Pagewdsub = Pagewdsub::findFirst(array("title='".str_replace("-", " ", $idno)."'"));
        $getimages = Tmedia::find(array("trainingid='".$Pagewdsub->idno."' ", "order" => "created_at ASC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $get) 
            {
                $ifsubmit = Submitclass::find(array("userid='".$usrid."' and classid='".$get->idno."'"));
                if(count($ifsubmit)!=0){
                    $chk = true;
                }else{
                    $chk = false;
                }
                $epdf= Epdf::find(array("parentid='".$get->idno."'"));
                if(count($epdf)!=0){
                    $pdficon = true;
                }else{
                    $pdficon = false;
                }

            if($state=="tr"){
                if($task == "teacher" && $get->classfor == "teacher"){
                     $data[] = array(
                    'idno'=>$get->idno,
                    'trainingid'=>$get->trainingid,
                    'title'=>$get->title,
                    'slug'=>$get->slug,
                    'body'=>$get->body,
                    'vidid'=>$get->vidid,
                    'embed'=>$get->embed,
                    'classfor'=>$get->classfor, 
                    'checked'=>$chk,
                    'pdficon'=>$pdficon);  
                }elseif($task == "mentor" ){
                    if($get->classfor == "teacher" || $get->classfor == "mentor"){
                         $data[] = array(
                        'idno'=>$get->idno,
                        'trainingid'=>$get->trainingid,
                        'title'=>$get->title,
                        'slug'=>$get->slug,
                        'body'=>$get->body,
                        'vidid'=>$get->vidid,
                        'embed'=>$get->embed,
                        'classfor'=>$get->classfor, 
                        'checked'=>$chk,
                        'pdficon'=>$pdficon);  
                    }
                }elseif($task == "leader"){
                     $data[] = array(
                    'idno'=>$get->idno,
                    'trainingid'=>$get->trainingid,
                    'title'=>$get->title,
                    'slug'=>$get->slug,
                    'body'=>$get->body,
                    'vidid'=>$get->vidid,
                    'embed'=>$get->embed,
                    'classfor'=>$get->classfor, 
                    'checked'=>$chk,
                    'pdficon'=>$pdficon);
                }

            }else{
                 $data[] = array(
                    'idno'=>$get->idno,
                    'trainingid'=>$get->trainingid,
                    'title'=>$get->title,
                    'slug'=>$get->slug,
                    'body'=>$get->body,
                    'vidid'=>$get->vidid,
                    'embed'=>$get->embed,
                    'classfor'=>$get->classfor, 
                    'checked'=>$chk,
                    'pdficon'=>$pdficon);    
            }
        }
    }
    echo json_encode($data);
}

public function SavemediaAction(){
    $trainingid = $_POST['trainingid'];
    $title = $_POST['title'];
    $slug = $_POST['blogslug'];
    $body = $_POST['body'];
    $embed = $_POST['embed'];
    $classfor = $_POST['classfor'];
    $byspace = explode(" ", $embed);
    $byslash = explode("/", $byspace[3]);
    $utubevidid = trim(str_replace('"',' ', $byslash[4]));
    $guid = new \Utilities\Guid\Guid();
    $idno = $guid->GUID();
    $_save = new Tmedia();
    $_save->assign(array(
        'idno' => $idno ,
        'trainingid' => $trainingid,
        'title' => $title,
        'slug' => $slug,
        'body' => $body,
        'vidid' => "n/a",
        'embed' => $embed ,
        'classfor'=> $classfor,
        'created_at'    =>date("Y-m-d H:i:s"),
        'updated_at'    =>date("Y-m-d H:i:s"),
        ));
    if (!$_save->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'successfully save');
  }
  echo json_encode(array("result"=>$data, "idno"=>$idno));
}

public function SavemediapdfAction(){
    $parentid = $_POST['parentid'];
    $filename = $_POST['filename'];
    $guid = new \Utilities\Guid\Guid();
    $idno = $guid->GUID();
    $_save = new Epdf();
    $_save->assign(array(
        'idno' => $idno ,
        'parentid' => $parentid,
        'filename' => $filename
        ));
    if (!$_save->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'successfully save');
  }
  echo json_encode($data);
}
public function DeletemediaAction($idno) {
    $img = Tmedia::findFirst('idno="'. $idno.'"');
    if ($img) {
        if ($img->delete()) {
            $data[]=array('success' => "");
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data);
}

public function editmediaAction($idno) {
    $getimages = Tmedia::find(array( "idno='".$idno."'"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $get) 
        {
            $data[] = array(
                'idno'=>$get->idno,
                'trainingid'=>$get->trainingid,
                'title'=>$get->title,
                'slugs'=>$get->slug,
                'body'=>$get->body,
                'vidid'=>$get->vidid,
                'embed'=>$get->embed,
                'classfor'=>$get->classfor
                );
        }
    }
    $getpdf = Epdf::find(array( "parentid='".$idno."'"));
    if(count($getpdf) == 0){
        $pdf=[];
    }else{
        foreach ($getpdf as $get) 
        {
            $pdf[] = array(
                'idno'=>$get->idno,
                'filename'=>$get->filename
                );
        }
    }
    echo json_encode(array("tmedia"=>$data, "pdf"=>$pdf));
}

public function readmediaAction($idno) {

    $Tmedia = Tmedia::find(array("slug='".$idno."'"));
    if(count($Tmedia) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($Tmedia as $get) 
        {
            $data[] = array(
                'idno'=>$get->idno,
                'trainingid'=>$get->trainingid,
                'title'=>$get->title,
                'body'=>$get->body,
                'vidid'=>$get->vidid,
                'embed'=>$get->embed,
                'classfor'=>$get->classfor
                );
        }
    }
    $getpdf = Epdf::find(array( "parentid='".$Tmedia[0]->idno."'"));
    if(count($getpdf) == 0){
        $pdf=[];
    }else{
        foreach ($getpdf as $get) 
        {
            $pdf[] = array(
                'idno'=>$get->idno,
                'filename'=>$get->filename
                );
        }
    }
    echo json_encode(array("tmedia"=>$data, "pdf"=>$pdf));
}

public function DeletepdfAction($idno) {
    $img = Epdf::findFirst('idno="'. $idno.'"');
    if ($img) {
        if ($img->delete()) {
            $data[]=array('success' => "");
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data);
}

public function UpdatemediaAction(){
    $idno = $_POST['idno'];
    $title = $_POST['title'];
     $slug = $_POST['blogslug'];
    $body = $_POST['body'];
    $embed = $_POST['embed'];
    $byspace = explode(" ", $embed);
    $byslash = explode("/", $byspace[3]);
    $utubevidid = trim(str_replace('"',' ', $byslash[4]));
    $_save = Tmedia::findFirst('idno="'.$idno.'"');
    $_save->assign(array(
        'title' => $title,
        'slug' => $slug,
        'embed' => $embed,
        'body' => $body, 
        ));
    if (!$_save->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'successfully save');
  }
  echo json_encode($data);
}



public function LoadFeaturedAction($idno) {
    $getimages = Epage::find(array( "pageid='".$idno."'"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $get) 
        {
            $data[] = array(
                'pageid'=>$get->pageid,
                'vidid'=>$get->vidid,
                'embed'=>$get->embed,
                );
        }
    }
    echo json_encode($data);
}


public function PageFeaturedAction(){
    $idno = $_POST['idno'];
    $embed = $_POST['embed'];
    $byspace = explode(" ", $embed);
    $byslash = explode("/", $byspace[3]);
    $utubevidid = trim(str_replace('"',' ', $byslash[4]));
        //check if exist
    $validate = Epage::find(array("pageid='".$idno."'"));
    if(count($validate)==0){
        $_save = new Epage();
        $_save->assign(array(
            'pageid' => $idno,
            'vidid' => $utubevidid,
            'embed' => $embed 
            ));
        if(!$_save->save()){
          $data[]=array('error' => 'Something went wrong saving the data, please try again.');
      }else{
        $data[]=array('success' => 'successfully save');
    }
}else{
  $_save = Epage::findFirst('idno="'.$validate[0]->idno.'"');
  $_save->assign(array(
    'vidid' => $utubevidid,
    'embed' => $embed 
    ));
  if (!$_save->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  }else{
      $data[]=array('success' => 'successfully Updated');
  }
}



echo json_encode($data);
}


}
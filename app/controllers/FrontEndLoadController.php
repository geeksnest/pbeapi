<?php
namespace Controllers;
use \Controllers\ControllerBase as CB;
use \Models\Center as Center;
use \Models\Centerdirector as Centerdirector;
use \Models\Centerslider as Centerslider;
use \Models\Googlescript as Script;
use \Models\Mainpageslider as Mainpageslider;
use \Models\Menuconstant as ParentSub;
use \Models\Menumain as Menumain;
use \Models\Pages as Pages;
use \Models\Testimonials as Testimonials;
use \Models\Settings as Settings;
use \Models\Users as Users;


class FrontEndLoadController extends \Phalcon\Mvc\Controller {
    public function Page404Action($main, $sub){
        $Mainmenu=Menumain::find(array("slug='".$main."'"));
        if(count($Mainmenu)==0){$_main[]=array('error' => "Redirect Page");}
        ///_^ End Main Menu
        $submenu=Pages::find(array("pageslugs='".$sub."'"));
        if(count($submenu)==0){$_sub[]=array('error' => "Redirect Page");}
        ///_^ End Main Menu
        // Array Result
        echo json_encode(array("main"=>$_main, "sub"=>$_sub));
    }



    public function feIndexAction($slug = '', $page = '', $others = ''){
        $menuMain = Menumain::find();
        foreach ($menuMain as $m){$menu_data[] = array('id' => $m->id,'parentmenu' => $m->title,'slug' => $m->slug, 'subslug' => $m->mainsub);}
        ///_^ End Main Menu //SOURCE_Controller=> MenuController->menuMainAction

        $testimonial = Testimonials::findFirst(array("testimonialcategory = 'featured' and status = 1 order by datepublished desc"));
        if ($testimonial){$testimonial_data = array('content' => $testimonial->content,'image' => $testimonial->image,'sender' => $testimonial->sender);}
        ///_^ End Testimonial //SOURCE_Controller=> TestimonialsController->featuredtestimonialsAction

        $slider = Mainpageslider::find();
        if(count($slider) == 0){$data['error']=array('NOIMAGE');}else{foreach ($slider as $slider) {$slider_data[] = array('id'=>$slider->id,'filename'=>$slider->filename);}}
        ///_^ End Slider //SOURCE_Controller=> MainpagesliderController->mainpagesliderAction

        $m_setting = Settings::findFirst("id=" . 1);
        if($m_setting) {$manage_data = array('id' => $m_setting->id,'value1' =>$m_setting->value1,'value2' =>$m_setting->value2,'value3' =>$m_setting->value3,'logo'=>$m_setting->logo, );}
        ///_^ End Manage Setting //SOURCE_Controller=> SettingsController->managesettingsAction

        $d_setting = Script::findFirst();
        if($d_setting){$display_data = array('script' => $d_setting->script,);}
        ///_^ End Display Setting //SOURCE_Controller=> SettingsController->displaytAction

        $slugtitle = Menumain::findFirst(array("slug='".$slug."'"));
        $_slugtitle = array('title' =>  $slugtitle->title);
        ///_^ End  Menu //SOURCE_Controller=> MenuController->parentTitleAction

        $pagetitle=Pages::findFirst(array("pageslugs='".$others."'"));
        $_pagetitle = array('title' => $pagetitle->title);
         ///_^ End SLUG TITLE //SOURCE_Controller=> MenuController->tittlesubPagesAction

        $menuSub = ParentSub::find(array("parentmenu='".$slug."'"));
        foreach ($menuSub as $m) {$_menusub[] = array('parentmenu' => $m->parentmenu,'submenu' => $m->submenu,'title' => $m->title,);}
        ///_^ End Sub Menus //SOURCE_Controller=> MenuController->menuSubAction

        $getpages=Pages::findFirst(array("pageslugs='".$page."'"));
        $_getpages = array('shortdesc' => $getpages->shortdesc,'datapage' => $getpages->body);
        ///_^ End Get PAGES //SOURCE_Controller=> MenuController->viewPagesAction

        $subPages=Pages::find(array("submenu='".$page."' ORDER BY suborder ASC"));
        foreach ($subPages as $subPages){$_subpages[] = array('title' => $subPages->title,'slugs' => $subPages->pageslugs,'shortdesc' => $subPages->shortdesc,'datapage' => $subPages->body,'submenu' => $subPages->submenu,'banner' => $subPages->banner);}
        ///_^ End Sub PAGES //SOURCE_Controller=> MenuController->viewsubPagesAction

        // Array Result
        echo json_encode(array(
            "parent_menu"=>$menu_data,                  //> MENU'S
            "testimonial"=>$testimonial_data,           //> LATEST TESTIMONIAL
            "slider"=>$slider_data,                     //> INDEX SLIDER
            "settings_managesettings"=>$manage_data,    //> MANAGE SETTING
            "settings_script"=>$display_data,           //> SCRIPT
            "slugtitle"=>$_slugtitle,                   //> SLUG TITLE
            "pagetitle"=>$_pagetitle,                   //> PAGE TITLE
            "submenu_menu"=>$_menusub,                  //> SUB MENU's
            "getpages"=>$_getpages,                     //> GET PAGE CONTENT
            "subpages"=>$_subpages,                     //> GET PAGE CONTENT
        )); 
    }
    public function feCentersAction($slug){
        $center = Center::findFirst('slugs="'. $slug.'"');
        $center_info=array('centerid'=> $center->centerid,'title'=> $center->title,'slugs'=> $center->slugs,'manager'=> $center->manager,'region'=> $center->region,'district'=> $center->district ,'state'=> $center->state,'city'=> $center->city,'zipcode'=> $center->zipcode,'address'=> $center->address,'metatitle'=> $center->metatitle,'desc'=> $center->desc,'banner'=> $center->banner,'mffrom'=> $center->mffrom,'mfto'=> $center->mfto,'satfrom'=> $center->satfrom,'satto'=> $center->satto,'contactnumber'=> $center->contactnumber,'status'=> $center->status,);
        ///_^ End Center Info //SOURCE_Controller=> CenterController->feInfocenterAction

        $getimages = Centerslider::find(array("centerid='".$center->centerid."'", "order" => "id DESC"));
        if(count($getimages) == 0){$_slider['error']=array('NOIMAGE');}else{foreach ($getimages as $getimages){$_slider[] = array('id'=>$getimages->id,'filename'=>$getimages->imagepath);}}
        ///_^ End Slider //SOURCE_Controller=> CenterController->centersliderAction

        $centermngr = Users::findFirst('id="'. $center->manager.'" and task="CS"');
        $_centermngr=array('username'=> $centermngr->username,'email'=> $centermngr->email,'fname'=> $centermngr->first_name,'lname'=> $centermngr->last_name,'profile'=> $centermngr->profile_pic_name);
         ///_^ End Slider //SOURCE_Controller=> CenterController->centerManagerAction

        $msg = Centerdirector::find(array("centerid='".$center->centerid."'"));
        if(count($msg)!=0){$_msg=array('info'=>$msg[0]->info,'message'=>$msg[0]->message);}else{$_msg=array('info' =>"",'message'=>"");
        }
        ///_^ End DIRECTOR message //SOURCE_Controller=> DirectorController->centerManagerActiongetMsgAction
        echo json_encode(array(
            "centerinfo"         =>$center_info,
            "slider"             =>$_slider,
            "centermanager"      =>$_centermngr,
            "directormessage"    =>$_msg,
        ));
    }

    public function feTestimonialAction(){
        function _testimonial($_query, $opt_query){
            if($opt_query=='all'){
                $listtestimonials= Testimonials::find(array("testimonialcategory = '".$_query."' and status = 1 order by datepublished desc"));
                foreach($listtestimonials as $listtestimonials){$data[] = array('id'=> $listtestimonials->id,'title'=> $listtestimonials->title,'status'=> $listtestimonials->status,'sender'=> $listtestimonials->sender,'age'=> $listtestimonials->age,'content'=> $listtestimonials->content,'image'=> $listtestimonials->image,'datepublished'=> $listtestimonials->datepublished,'testimonialcategory'=> $listtestimonials->testimonialcategory);}
            }else{
                $listtestimonials= Testimonials::findFirst(array("testimonialcategory = '".$_query."' and status = 1 order by datepublished desc"));
                if ($listtestimonials){$data = array('id'=> $listtestimonials->id,'title'=> $listtestimonials->title,'status'=> $listtestimonials->status,'sender'=> $listtestimonials->sender,'age'=> $listtestimonials->age,'content'=> $listtestimonials->content,'image'=> $listtestimonials->image,'datepublished'=> $listtestimonials->datepublished,'testimonialcategory'=> $listtestimonials->testimonialcategory);} 
            }
            return $data;
        }

        echo json_encode(array(
            "pbsTestimonial"                 => _testimonial("powerbrain","all"),  //SOURCE_Controller=> TestimonialsController->pbstestimonialsAction
            "featuredforAdultTestimonial"    => _testimonial("adults","first"),    //SOURCE_Controller=> TestimonialsController->foradultsfeaturedtestimonialsAction
            "featuredforFamiliesTestimonial" => _testimonial("families","first"),  //SOURCE_Controller=> TestimonialsController->forfamiliesfeaturedtestimonialsAction
            "forAdultsTestimonial"           => _testimonial("adults","all"),      //SOURCE_Controller=> TestimonialsController->foradultstestimonialsAction
            "forFamiliesTestimonial"         => _testimonial("families","all")     //SOURCE_Controller=> TestimonialsController->forfamiliestestimonialsAction
        ));
    }

}
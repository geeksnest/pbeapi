<?php

namespace Controllers;
use \Models\Users as Users;
use \Controllers\ControllerBase as CB;
use \Models\Center as Center;
use \Models\School as School;
use \Models\Forgotpassword as Forgotpasswords;
use \Models\Usersession as Usersession;



class EuserController extends \Phalcon\Mvc\Controller {
  public function verifyAction($userid,$code) {
    function _audit($_msg){
     $auditid = new \Utilities\Guid\Guid();
     $auditid->GUID();
     $db = \Phalcon\DI::getDefault()->get('db');
     $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$userid."','User','Login','".$_msg.": ".$username." ')");
     $sql->execute();
   }

   $request = new \Phalcon\Http\Request();
   $user = Users::findFirst('id="'.$userid.'" AND  activation_code="'.$code.'" AND status=0');
   $userid = $user->id;
   if($user){
     _audit("Login Success");
     $jwt = new \Security\Jwt\JWT();
     $payload = array(
      "id" => $user->id,
      "username" => $user->username,
      "lastname" => $user->last_name,
      "firstname" => $user->first_name,
      "task" => $user->task,
      "email" => $user->email,
      "birthday" => $user->birthday,
      "gender" => $user->gender,
      "profile" => $user->profile_pic_name,
      "school" => $user->school,
      "grade" => $user->grade,
      "task" => $user->task, 
      "exp" => time() + (60 * 60)); 
     $token = $jwt->encode($payload, $app->config->hashkey);
     $data[]=array('success'=>$payload, 'token'=>$token);
     echo json_encode($data);

   }else{
    echo json_encode(array('error' => 'Username or Password is invalid.'));
    _audit("Login Failed");
  }
}
public function userUpdateAction() {
  $request = new \Phalcon\Http\Request();
  if($request->isPost()){
    $id         = $request->getPost('userid');
    $username   = $request->getPost('username');
    $password   = $request->getPost('password');
    $fname      = $request->getPost('fname');
    // $mname      = $request->getPost('mname');
    $lname      = $request->getPost('lname');
    $bday       = $request->getPost('year').'-'.$request->getPost('month').'-'.$request->getPost('day');
    $gender     = $request->getPost('gender');
    $school     = $request->getPost('skul');
    $grade      = $request->getPost('grade');
    $profile    = $request->getPost('profile');

    $getInfo = Users::findFirst('id="'. $id .'"');
    if($getInfo){
      $udata = Users::findFirst('id="'.$id .'"');
      $udata->username        = $username;
      $udata->password        = sha1($password);
      $udata->first_name      = $fname;
      $udata->last_name       = $lname;
      $udata->birthday        = $bday;
      $udata->gender          = $gender;
      $udata->school          = $school;
      $udata->profile_pic_name= $profile;
      $udata->grade           = $grade;
      $udata->status          = 1;
      $udata->review          = 1;
      if(!$udata->save()){
        $errors = array();
        foreach ($udata->getMessages() as $message){
          $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
        echo json_encode($data);
      }else{

        // $audit = new CB();
        // $audit->auditlog(array("module" =>"User","event" => "Update","title" => "Update User : ". $username .""));
        // $app_USER = new CB();
        // $content = "Hi ".$fname ." ".$lname .",<br><br>We are greetful to inform you that your record will be sended to Power Brain Education admin and will be subject for review.We will send you a confirmation email once we approved your application. <br><br><br>Thank you adn God Bless!";
        // $_rdata_USER  = $app_USER ->sendMail($getInfo->email, 'Power Brain Education  Confirmation Email', $content);

        // $app_ADMIN = new CB();
        // $content = "Power Brain Education<br><br>".$fname ." ".$lname .", has been completed his/her record for review. Please be informed that you need to Review the application and set his/her user roles for approval.<br><br><br>Thank you and God Bless!";
        // $_rdata_ADMIN = $app_ADMIN->sendMail("powerb@mailinator.com", 'Power Brain Education  Confirmation Email', $content);

        $user = Users::findFirst('id="'.$id.'"');
        $jwt = new \Security\Jwt\JWT();
        $payload = array(
          "id" => $user->id,
          "username" => $user->username,
          "lastname" => $user->last_name,
          "firstname" => $user->first_name,
          "task" => $user->task,
          "email" => $user->email,
          "birthday" => $user->birthday,
          "gender" => $user->gender,
          "profile" => $user->profile_pic_name,
          "school" => $user->school,
          "grade" => $user->grade,
          "task" => $user->task, 
          "exp" => time() + (60 * 60)); 
        $token = $jwt->encode($payload, $app->config->hashkey);

        $data=array('success' => 'UPDATED', 'token'=> $token);  




      }
      echo json_encode(array("data"=>$data));
    }else{
      echo json_encode(array('error' => 'Username or Password is invalid.'));
    }
  }

}


public function confirmAction() {
  $request = new \Phalcon\Http\Request();
  if($request->isPost()){
    $id         = $request->getPost('userid');
    $userrole   = $request->getPost('userrole');
    $email      = $request->getPost('email');
    $fname      = $request->getPost('fname');
    $lname      = $request->getPost('lname');

    $getInfo = Users::findFirst('id="'. $id .'"');
    if($getInfo){
      $udata = Users::findFirst('id="'.$id .'"');
      $udata->task        = $userrole;
      $udata->review      = 0;
      $udata->status      = 1;
      if(!$udata->save()){
        $errors = array();
        foreach ($udata->getMessages() as $message){
          $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
        echo json_encode($data);
      }else{
        $data=array('success' => 'UPDATED');              
        $app = new CB();
        $app->auditlog(array("module" =>"User","event" => "Update","title" => "Update User : ". $username .""));



        $content = "Hi ".$fname ." ".$lname .",,<br><br>We are greetful to inform you that your record has been approved by the Power Brain Education admin. You can now used the site and you can now log in at Learning Community page.<br><br><br>Thnak you and God Bless!";
        $_rdata = $app->sendMail($email, 'Power Brain Education  Confirmation Email', $content);
      }
      echo json_encode($data);
    }else{
      echo json_encode(array('error' => 'Username or Password is invalid.'));
    }
  }

}

public function EuserUpdateAction() {
 function _audit($_msg){
   $auditid = new \Utilities\Guid\Guid();
   $auditid->GUID();
   $db = \Phalcon\DI::getDefault()->get('db');
   $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$userid."','User','Login','".$_msg.": ".$username." ')");
   $sql->execute();
 }
 $request = new \Phalcon\Http\Request();
 if($request->isPost()){
  $id         = $request->getPost('userid');
  $username   = $request->getPost('username');
  $email      = $request->getPost('email');
  $fname      = $request->getPost('fname');
  $lname      = $request->getPost('lname');
  $bday       = $request->getPost('year').'-'.$request->getPost('month').'-'.$request->getPost('day');
  $gender     = $request->getPost('gender');
  $task       = $request->getPost('task');
  $banner     = $request->getPost('banner');

  $getInfo = Users::findFirst('id="'. $id .'"');
  if($getInfo){

    $udata = Users::findFirst('id="'.$id .'"');
    $udata->username        = $username;
    $udata->email           = $email;
    $udata->first_name      = $fname;
    $udata->last_name       = $lname;
    $udata->birthday        = $bday;
    $udata->gender          = $gender;
    if(!$udata->save()){
      $errors = array();
      foreach ($udata->getMessages() as $message){
        $errors[] = $message->getMessage();
      }
      $data[]=array('error' => $errors);
      echo json_encode($data);
    }else{          
      $audit = new CB();
      $audit->auditlog(array("module" =>"User","event" => "Update","title" => "Update User : ". $username .""));
      $user = Users::findFirst('id="'.$request->getPost('userid').'"');
      $jwt = new \Security\Jwt\JWT();
      $payload = array(
        "id" => $user->id,
        "username" => $user->username,
        "lastname" => $user->last_name,
        "firstname" => $user->first_name,
        "task" => $user->task,
        "email" => $user->email,
        "birthday" => $user->birthday,
        "gender" => $user->gender,
        "profile" => $user->profile_pic_name,
        "school" => $user->school,
        "grade" => $user->grade,
        "task" => $user->task, 
        "exp" => time() + (60 * 60)); 
      $token = $jwt->encode($payload, $app->config->hashkey);
      $data[]=array('success'=>$payload, 'token'=>$token);
      echo json_encode($data);
    }
  }else{
    echo json_encode(array('error' => 'Username or Password is invalid.'));
  }
}

}
public function chngePassAction() {
  $request = new \Phalcon\Http\Request();
  if($request->isPost()){
    $id         = $request->getPost('userid');
    $oldpass    = $request->getPost('oldpass');
    $password   = $request->getPost('password');
    $getInfo = Users::find('id="'. $id .'" and password="'. sha1($oldpass).'" ');
    if(count($getInfo) !=0){

      $udata = Users::findFirst('id="'.$id .'"');
      $udata->password        = sha1($password);
      if(!$udata->save()){
        $errors = array();
        foreach ($udata->getMessages() as $message){
          $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
        echo json_encode($data);
      }else{
       $data=array('success' => 'UPDATED');              
       $audit = new CB();
       $audit->auditlog(array("module" =>"User","event" => "Update","title" => "Update User : ". $username .""));
       echo json_encode(array('success' => 'Photo has Been deleted'));
     }

   }else{
    echo json_encode(array('error' => 'error'));
  }
}
}

public function listSchoolAction() {
  $_gdata = School::find();
  $data = array();
  foreach ($_gdata as $m) {
    $data[] = array(
      'id' => $m->id,
      'name' => $m->name,
      );
  }
  echo json_encode($data );
}

public function logoutUserAction($userid) {
  $_exist = Usersession::findFirst('userid="'. $userid .'"');
  if($_exist){
    $_udata = Usersession::findFirst('id="'. $_exist->id .'"');
    $_udata->logstat = 0;
    if(!$_udata->save()){
      $errors = array();
      foreach ($_udata->getMessages() as $message) {
        $errors[] = $message->getMessage();
      }
      $data[]=array('error' => $errors);
    }else{
      $data[]=array('sucess' => "User  Login updated");
    }
  }else{
    $data[]=array('error' => $errors);
  }
  echo json_encode($data);
}




public function loginUserAction($userid){
  $_exist = Usersession::findFirst('userid="'. $userid .'"');
  if($_exist){
    $_udata = Usersession::findFirst('id="'. $_exist->id .'"');
    $_udata->logstat = 1;
    if(!$_udata->save()){
      $errors = array();
      foreach ($_udata->getMessages() as $message) {
        $errors[] = $message->getMessage();
      }
      $data[]=array('error' => $errors);
    }else{
      $data[]=array('sucess' => "User  Login updated");
    }
  }else{
    $_sdata = new Usersession();
    $_sdata->userid = $userid;
    $_sdata->logstat = 1;
    if(!$_sdata->create()){
      $errors = array();
      foreach ($_sdata->getMessages() as $message) {
        $errors[] = $message->getMessage();
      }
      $data[]=array('error' => $errors);
    }else{
      $data[]=array('sucess' => "User  Login");
    }
  }
  echo json_encode($data);
}







public function loguserAction($userid) {
  $_exist = Usersession::findFirst('userid="'. $userid .'"');
  if($_exist){
    if($_exist->logstat == 1){
      $data[]=array('sucess' => "User Still Login");
    }else{
      $data[]=array('error' => "User Already Logout");
    }
  }else{
    $data[]=array('error' => "User Already Logout");
    // $_sdata = new Usersession();
    // $_sdata->userid = $userid;
    // $_sdata->logstat = 1;
    // if(!$_sdata->create()){
    //   $errors = array();
    //   foreach ($_sdata->getMessages() as $message) {
    //     $errors[] = $message->getMessage();
    //   }
    //   $data[]=array('error' => $errors);
    // }else{
    //   $data[]=array('sucess' => "User  Login");
    // }
  }
  echo json_encode($data);
}

}
<?php
namespace Controllers;

use \Models\Centerdirector as Centerdirector;
use \Models\Center as Center;
use \Controllers\ControllerBase as CB;
class DirectorController extends \Phalcon\Mvc\Controller {

    public function dirMsgAction() {
     $request = new \Phalcon\Http\Request();
     if($request->isPost()){
        $centerid = $request->getPost('centerid');
        $info = $request->getPost('info');
        $message = $request->getPost('message');
        $cntmsg = Centerdirector::find(array("centerid='".$centerid."'"));
        $center = Center::findFirst(array("centerid='".$centerid."'"));
        $centername = $center->title;
        if(count($cntmsg)!=0){
            $usave = Centerdirector::findFirst(array("centerid='".$centerid."'"));
            $usave->info        = $info;
            $usave->message    = $message;
            $usave->updated_at   = date("Y-m-d H:i:s");
            if(!$usave->save()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'User info has been successfuly updates.');
                //START Log
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Message", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "Directors Message Updated in Center : ".$centername."", /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            }
        }else{
         $guid = new \Utilities\Guid\Guid();
         $page = new Centerdirector();
         $page->assign(array(
            'msgid'=>$guid->GUID(),
            'centerid' => $centerid,
            'info' => $info,
            'message' => $message,
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s"),
            ));
         if (!$page->save()) {
            $errors = array();
            foreach ($page->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data[]=array('error' => $errors);
        } else {
           $data[]=array('success' => "New Page has been successfully Created!");
           //START Log
           $audit = new CB();
           $audit->auditlog(array(
            "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
            "event" => "Message", /*//Example ADD , EdIT , Delete ,View Details etc...*/
            "title" => "Directors Message Updated in Center : ".$centername."", /*// Maybe some info here (confuse) XD*/
            ));
            //END Audit Log
       }

   }
}
echo json_encode($data);

}
public function getMsgAction($centerid) {
    $msg = Centerdirector::find(array("centerid='".$centerid."'"));

    if(count($msg)!=0){

        $data=array(
            'info' => $msg[0]->info,
            'message'  => $msg[0]->message
            );
    }else{
         $data=array(
            'info' => "",
            'message'  => ""
            );
    }
    echo json_encode($data);
}

}
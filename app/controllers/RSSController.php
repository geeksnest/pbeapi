<?php

namespace Controllers;
use \Models\Centernews as Centernews;
use \Models\Centerevent as Centerevent;
use \Models\Center as Center;
use \Controllers\ControllerBase as CB;
class RSSController extends \Phalcon\Mvc\Controller {

    public function getrssAction($centerid) {
        $dbnews = Centernews::find();
        foreach ($dbnews as $value) {
            $data[]= array(
                'type'=>'news',
                'id'=>  $value->newsid,
                'centerid'=>  $value->centerid,
                'title'=>$value->title,
                'slugs'=>$value->slugs,
                'date'=>$value->updated_at,
                'description'=>$value->description,
                );
        }
        $dbevent = Centerevent::find();
        foreach ($dbevent as $value) {
            $data[]= array(
                'type'=>'event',
                'id'=>  $value->eventid,
                'centerid'=>  $value->centerid,
                'title'=>$value->title,
                'slugs'=>" ",
                'date'=>$value->updated_at,
                'description'=>$value->desc,
                );
        }
         $dbcenter = Center::find();
        foreach ($dbcenter as $value) {
            $data[]= array(
                'type'=>'center',
                'id'=>  $value->centerid,
                'centerid'=> "",
                'title'=>$value->title,
                'slugs'=>$value->slugs,
                'date'=>$value->created_at,
                'description'=>$value->desc,
                );
        }

        echo json_encode($data);
    }

}
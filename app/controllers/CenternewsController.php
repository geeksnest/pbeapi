<?php

namespace Controllers;
use \Models\Centernews as Centernews;
use \Models\Blog as Blog;
use \Models\Author as Author;
use \Models\Center as Center;
use \Models\Centernewstags as Tags;
use \Models\Centernewscategory as Category;
use \Models\Centernewsslctedcateg as Selctcateg;
use \Models\Centerselectednewstags as SelctTags;
use \Models\Centerfeauredvideo as Featuredvideo;
use \Controllers\ControllerBase as CB;

class CenternewsController extends \Phalcon\Mvc\Controller {


    public function Validatenewstitlesction($centername){
        $validate = Centernews::find(array("title='".$centername."'"));

        echo json_encode(count($validate));
    }

    public function centerNewsction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
           $centerid       = $request->getPost('centerid');
           $title          = addslashes($request->getPost('title'));
           $slugs          = $request->getPost('slugs');
           $author         = $request->getPost('author');
           $date           = $request->getPost('date');
           $description    = $request->getPost('summary');
           $body           = $request->getPost('body');
           $status         = $request->getPost('status');
           $featuredtype   = $request->getPost('featuredthumbtype');
           $featuredthumb  = $request->getPost('featuredthumb');
           $metadesc       = $request->getPost('metadesc');
           $metakeyword    = $request->getPost('metakeyword');
           $metatitle      = $request->getPost('metatitle');
           $tags           = $request->getPost('tag');
           $categ          = $request->getPost('category');

           $guid = new \Utilities\Guid\Guid();
           $_guid = new \Utilities\Guid\Guid();
           $_checkifexist = Centernews::find('newsid="'. $guid->GUID().'"');
           $newsid = (count($_checkifexist)!=0 ? $guid->GUID() : $_guid->GUID());



           $page = new Centernews();
           $page->assign(array(
            'newsid'        => $newsid,
            'centerid'      => $centerid,
            'authorid'      => $author,
            'title'         => $title,
            'slugs'          => $slugs,
            'description'   => $description,
            'body'          => $body,
            'banner'        => "temp banner",
            'featuredoption' => $featuredtype,
            'featured'      => $featuredthumb,
            'publish'       => $date,
            'status'        => $status,
            'metadesc'      => $metadesc,
            'metakeyword'   => $metakeyword,
            'metatitle'     => $metatitle,
            'created_at'    =>date("Y-m-d H:i:s"),
            'updated_at'    =>date("Y-m-d H:i:s")
            ));
           if (!$page->save()) {
            $errors = array();
            foreach ($page->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data[]=array('error' => $errors);
        }else{
            $audit = new CB(); $audit->auditlog(array("module" =>"Blog", "event" => "Add","title" => "Add Blog- ".$title." ",));
            
            foreach($categ as $cat){
                $catnews = new Selctcateg();
                $catnews->assign(array(
                    'newsid' => $newsid,
                    'categ' => $cat
                    ));
                if (!$catnews->save()){
                    $errors = array();
                    foreach ($catnews->getMessages() as $message){
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{
                // $data['success'] = "Success cat";
                }
            }

            foreach($tags as $tag){
                $gettags = Tags::findFirst("tags='".$tag."'");
                if(!$gettags){
                    $newstags = new Tags();
                    $newstags->assign(array(
                        'tags' => $tag,
                        'slugs' => str_replace("-", " ", $tag),
                        'created_at'    =>date("Y-m-d H:i:s"),
                        'updated_at'    =>date("Y-m-d H:i:s")
                        ));
                    if (!$newstags->save()){
                        $data['error'] = "Something went wrong saving the newstags, please try again.";
                    }
                    else{
                        $chkif = SelctTags::findFirst("tags='".$tag."'");
                        if(!$chkif){
                            $tagnews = new SelctTags();
                            $tagnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $tag
                                ));
                            if (!$tagnews->save()){
                                $errors = array();
                                foreach ($tagnews->getMessages() as $message){
                                    $errors[] = $message->getMessage();
                                }
                                //echo json_encode(array('error' => $errors));
                            }else{
                                //$data['success'] = "Success cat";
                            }
                        }
                    }
                }else{
                   $chkif = SelctTags::findFirst("tags='".$tag."'");
                   if(!$chkif){
                    $tagnews = new SelctTags();
                    $tagnews->assign(array(
                        'newsid' => $newsid,
                        'tags' => $tag
                        ));
                    if (!$tagnews->save()){
                        $errors = array();
                        foreach ($tagnews->getMessages() as $message){
                            $errors[] = $message->getMessage();
                        }
                        //echo json_encode(array('error' => $errors));
                    }else{
                        //$data['success'] = "Success cat";
                    }
                }
            }
        }
    }
}

echo json_encode($data );
}

public function manageNewsAction($num, $page, $keyword, $centerid) {

    if ($keyword == 'undefined' && $centerid == 'undefined'){
       $Pages = Centernews::find(array("order" => "publish DESC"));
   } 
   elseif($keyword == 'undefined' && $centerid != 'undefined')
   {
    $Pages = Centernews::find(array("centerid = '".$centerid."'"));
}
else {
    if($keyword !='undefined' && $centerid !='undefined'){
        $conditions = "centerid = '".$centerid."' and title LIKE '%" . $keyword . "%' OR slugs LIKE '%" . $keyword . "%'";
        $Pages = Centernews::find(array($conditions));
    }else{
        $conditions = "title LIKE '%" . $keyword . "%' OR slugs LIKE '%" . $keyword . "%'";
        $Pages = Centernews::find(array($conditions));
    }

}

$currentPage = (int) ($page);


$paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
        "data" => $Pages,
        "limit" => 10,
        "page" => $currentPage
        )
    );
$page = $paginator->getPaginate();

$data = array();
foreach ($page->items as $m) {
 $getAuthor = Author::findFirst('id="'.$m->authorid.'"');
 $getcenter = Center::findFirst('centerid="'.$m->centerid.'"');
 $data[] = array(
    'newsid' => $m->newsid,
    'authorname'  => $getAuthor->name,
    'title' => stripslashes($m->title),
    'slugs' => $m->slugs,
    'publish' => $m->publish,
    'status' => $m->status,
    'center' => $getcenter->title
    );
}
$p = array();
for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}


public function changestatusAction($id,$status){

    $getInfo = Centernews::findFirst('newsid="'. $id .'"');
    $title = $getInfo->title;
    if($status == 1){
     $getInfo->status = 0;
     $getInfo->save();
     $data=array('success' => 'Deactivated');
     $audit = new CB();
     $audit->auditlog(array("module" =>"Blog","event" => "Deactivated", "title" => "Deactivated News : ". $title ."" ));
   }
   else{
     $getInfo->status = 1;
     $getInfo->save();
     $data=array('success' => 'Activated');
     $audit = new CB();
     $audit->auditlog(array("module" =>"Blog", "event" => "Activated", "title" => "Activated News : ". $title ."" ));
   }
   echo json_encode($data);
 }



public function newsDeleteAction($newsid) {

    $news = Centernews::findFirst('newsid="'. $newsid.'"');
    $title = $news->title;
    if ($news) {
        if ($news->delete()) {
            $data[]=array('success' => "News has been successfully deleted");
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Center news- ".$title." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
    }else{
        $data[]=array('error' => "Seems the News Cant Be Deleted");
    }   
    echo json_encode($data);
}

public function newseditAction($newsid) {
    $data = array();
    $news =  Centernews::findFirst('newsid="'. $newsid.'"');
    if ($news) {

        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt1 = $db1->prepare("SELECT * FROM centerselectednewstags WHERE centerselectednewstags.newsid = '" . $newsid . "'");
        $stmt1->execute();
        $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
        $db2 = \Phalcon\DI::getDefault()->get('db');
        $stmt2 = $db2->prepare("SELECT * FROM centernewsslctedcateg INNER JOIN centernewscategory ON centernewscategory.categoryid = centernewsslctedcateg.categ WHERE centernewsslctedcateg.newsid  = '" . $newsid . "'");
        $stmt2->execute();
        $searchresult2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
        if($news->featuredtype!="video"){
            $img = $news->featured;
            $vid = "";
        }else{
            $img = "";
            $vid = $news->featured;
        }
        $data = array(
            'newsid' => $news->newsid,
            'centerid'=>$news->centerid,
            'title' => stripslashes($news->title),
            'slugs' => $news->slugs,
            'author' => $news->authorid,
            'summary' => $news->description,
            'body' => $news->body,
            'imagethumb' => $img,
            'videothumb' => $vid,
            'newslocation' => "$news->newslocation",
            'category' => $searchresult2,
            'status' => $news->status,
            'featurednews' => $news->featurednews,
            'date' => $news->publish,
            'datecreated' => $news->created_at,
            'tag' => $searchresult1,
            'metatitle' => $news->metatitle,
            'metakeyword' => $news->metakeyword,
            'metadesc' => $news->metadesc
            );
    }

    echo json_encode($data);
}
public function centerupdatection() {


   $request = new \Phalcon\Http\Request();

   if($request->isPost()){
       $newsid         = $request->getPost('newsid');
       $centerid       = $request->getPost('centerid');
       $title          = addslashes($request->getPost('title'));
       $slugs          = $request->getPost('slugs');
       $author         = $request->getPost('author');
       $date           = $request->getPost('date');
       $description    = $request->getPost('summary');
       $body           = $request->getPost('body');
       $status         = $request->getPost('status');
       $featuredtype   = $request->getPost('featuredthumbtype');
       $featuredthumb  = $request->getPost('featuredthumb');
       $metadesc       = $request->getPost('metadesc');
       $metakeyword    = $request->getPost('metakeyword');
       $metatitle      = $request->getPost('metatitle');
       $tags           = $request->getPost('tag');
       $categ          = $request->getPost('category');

       /*$banner = $request->getPost('banner');*/

       $page = Centernews::findFirst('newsid="'. $newsid.'"');
       $page->assign(array(
        'centerid'      => $centerid,
        'authorid'      => $author,
        'title'         => $title,
        'slugs'          => $slugs,
        'description'   => $description,
        'body'          => $body,
        'banner'        => "temp banner",
        'featuredoption' => $featuredtype,
        'featured'      => $featuredthumb,
        'publish'       => $date,
        'status'        => $status,
        'metadesc'      => $metadesc,
        'metakeyword'   => $metakeyword,
        'metatitle'     => $metatitle,
        'created_at'    =>date("Y-m-d H:i:s"),
        'updated_at'    =>date("Y-m-d H:i:s")
        ));
       if (!$page->save()) {
        $errors = array();
        foreach ($page->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    }else{
       $data[]=array('success' => "New Center has been successfully Created!");
            // START Log
       $audit = new CB(); $audit->auditlog(array("module" =>"Blog", "event" => "Add","title" => "Add Blog- ".$title." "));

       $_checkifexist = Selctcateg::find("newsid='".$newsid ."'");
        foreach($_checkifexist as $cat){
            if(count($_checkifexist)!=0){
                $dlt = Selctcateg::findFirst('num="'.$cat->num.'"');
                if ($dlt){
                    if ($dlt->delete()) {
                        $data[]=array('success' => "Category Deleted");
                        $dlt_count++;
                    }else{
                        $data[]=array('error' => '');
                    }
                }else{$data[]=array('error' => '');}
            }
        }
        //RE-ADD ALL CATEG IN SELECTED TAGS
        foreach($categ as $cat){
            $checkifexist = Selctcateg::find("newsid='".$newsid ."' and categ='".$cat."'");
            if(count($checkifexist)==0){
                $catnews = new Selctcateg();
                $catnews->assign(array(
                    'newsid' => $newsid,
                    'categ' => $cat
                    ));
                if(!$catnews->save()){
                    $errors = array();
                    foreach ($page->getMessages() as $message){
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{$data['success'] = "Success cat";}
            }
        } 

        //TAGS
        $delall = SelctTags::find("newsid='".$newsid."'");
        if($delall->delete()){
            foreach($tags as $tag){
                $gettags = Tags::findFirst("tags='".$tag."'");
                if(!$gettags){
                    $newstags = new Tags();
                    $newstags->assign(array(
                        'tags' => $tag,
                        'slugs' => str_replace("-", " ", $tag),
                        'created_at'    =>date("Y-m-d H:i:s"),
                        'updated_at'    =>date("Y-m-d H:i:s")
                        ));
                    if (!$newstags->save()){
                        //$data['error'] = "Something went wrong saving the newstags, please try again.";
                    }
                    else{
                        $chkif = SelctTags::findFirst("newsid='".$newsid."' and tags='".$tag."'");
                        if(!$chkif){
                            $tagnews = new SelctTags();
                            $tagnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $tag
                                ));
                            if (!$tagnews->save()){
                                $errors = array();
                                foreach ($tagnews->getMessages() as $message){
                                    $errors[] = $message->getMessage();
                                }
                                //echo json_encode(array('error' => $errors));
                            }else{
                                //$data['success'] = "Success cat";
                            }
                        }
                    }
                }else{
                    $chkif = SelctTags::findFirst("newsid='".$newsid."' and tags='".$tag."'");
                    if(!$chkif){
                        $tagnews = new SelctTags();
                        $tagnews->assign(array(
                            'newsid' => $newsid,
                            'tags' => $tag
                            ));
                        if (!$tagnews->save()){
                            $errors = array();
                            foreach ($tagnews->getMessages() as $message){
                                $errors[] = $message->getMessage();
                            }
                            //echo json_encode(array('error' => $errors));
                        }else{
                            //$data['success'] = "Success cat";
                        }
                    }
                }
            }
        }
   }
}

echo json_encode($data );
}




public function feNewsAction($num, $page, $keyword, $centerid) {

  if ($keyword == 'null' || $keyword == 'undefined') {

       $Pages = Centernews::find(array("centerid = '".$centerid."' and status= '1'"));
   } else {
    $conditions = "centerid = '".$centerid."' and title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
    $Pages = Centernews::find(array($conditions));
  }

$currentPage = (int) ($page);


$paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
        "data" => $Pages,
        "limit" => 10,
        "page" => $currentPage
        )
    );
$page = $paginator->getPaginate();

$data = array();
foreach ($page->items as $m) {
    $getAuthor = Author::findFirst('id="'.$m->authorid.'"');
    $data[] = array(
        'newsid' => $m->newsid,
        'slug' => $m->slugs,
        'authorname'  => $getAuthor->name,
        'title' => stripslashes($m->title),
        'description' => $m->description,
        'body' => $m->body,
        'publish' => $m->publish,
        'featuredoption' => $m->featuredoption,
        'featured' => $m->featured
        );
}
$p = array();
for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}
public function listauthorAction(){
 $listauthor = Author::find();
 foreach ($listauthor as $listauthor) 
 {
    $data[] = array(
        'id'=>$listauthor->id,
        'name'=>$listauthor->name
        );
}
echo json_encode($data);
}
public function listcenterAction(){
   $listcenter = Center::find("status='true'");
   foreach ($listcenter as $listcenter) 
   {

    $data[] = array(
        'centerid'=>$listcenter->centerid,
        'title'=>$listcenter->title
        );
}
echo json_encode($data);
}
public function readnewsAction($slugnews) {
    $center = Centernews::findFirst('slugs="'. $slugnews.'"');
    $seltags = SelctTags::find('newsid="'.$center->newsid.'"');
    foreach ($seltags as $m) {
        $tags = Tags::find('id="'. $m->tags.'"');
        if(count($tags)!=0){
            $data2[] = array(
                'tags' => $tags[0]->tags,
                'tagslug' => $tags[0]->slugs,
                );
        }
    }
    foreach ($seltags as $m) {
        $tags = Tags::find('id="'. $m->tags.'"');
        if(count($tags)!=0){
            $data3[] = array(
                'tags' => $tags[0]->tags,
                );
        }
    }

    $category = Category::find();
    foreach ($category as $data) {
        $dbcategory[] = array(
            "category"=> $data->categoryname,
            "slugs"=> $data->categoryslugs,
            );
    }


    $getAuthor = Author::findFirst('id="'. $center->authorid.'"');
    $categ = Category::findFirst('categoryid="'. $center->category.'"');
    $data=array(
        'author'      => $center->authorid,
        'authorname'  => $getAuthor->name,
        'authorbio'   => $getAuthor->about,
        'authorpic'   => $getAuthor->image,
        'title'       => $center->title,
        'slugs'       => $center->slugs,
        'description' => $center->description,
        'body'        => $center->body ,
        /*'banner'      => $center->banner,*/
        'city'          => $center->city,
        'publish'       => $center->publish,
        'authorid'       => $center->authorid,
        'centerid'     =>$center->centerid,
        'featuredoption'  => $center->featuredoption,
        'feat'     =>$center->featured,
        'category'     => $categ->categoryname,
        'categoryslugs'     => $categ->categoryslugs,
        );

    echo json_encode(array("data"=> $data, "tags"=> $data2 , "category"=> $dbcategory,"metatags"=>$data3));

}

public function authorNewsAction($offset, $page,  $authorid) {
    $data = array();
    $centernews = Centernews::find(array("authorid = '".$authorid."'"));
    foreach($centernews as $m) {
        $data[] = array(
            'newsid' => $m->newsid,
            'slug' => $m->slugs,
            'title' => stripslashes($m->title),
            'description' => $m->description,
            'body' => $m->body,
            'publish' => $m->publish,
            'feattype' => 'banner',
            'featured' => $m->banner,
            'type' => 'news',
            'link'=>'/../centernews/'.$m->slugs
            );
    }
    $blog = Blog::find(array("authorid = '".$authorid."'"));
    foreach($blog as $m) {
        $data[] = array(
            'newsid' => $m->blogid,
            'slug' => $m->slug,
            'title' => stripslashes($m->title),
            'description' => $m->description,
            'body' => $m->body,
            'publish' => $m->publish,
            'feattype' => $m->featuredtype,
            'featured' => $m->featured,
            'type' => 'blog',
            'link'=>'/../blog/'.$m->slug
            );
    }
    echo json_encode(array_slice($data, $offset, $page));
}

public function addembedAction() {
 $request = new \Phalcon\Http\Request();
 if($request->isPost()){
    $centerid = $request->getPost('centerid');
    $embed = $request->getPost('video');

    //EXPLOSION PAPUTOK
    $byspace = explode(" ", $embed);
    $byslash = explode("/", $byspace[3]);
    $utubevidid = trim(str_replace('"',' ', $byslash[4]));
    //EXPLOSION PAPUTOK

    $guid = new \Utilities\Guid\Guid();
    $guid->GUID();
    $saveembed = new Featuredvideo();
    $saveembed->assign(array(
        'vidid' => $guid->GUID(),
        'centerid' => $centerid,
        'embedid' => $utubevidid,
        'embed' => $embed
        ));
    if (!$saveembed->save()) {
        $errors = array();
        foreach ($saveembed->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {
        $data[]=array('success' => "New Page has been successfully Created!");
    }


}
echo json_encode($data);
}
public function featvideoAction($centerid) {
    $getimages = Featuredvideo::find(array("centerid ='".$centerid."'"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'vidid'=>$getimages->vidid,
                'embedid'=>$getimages->embedid,
                'embed'=>$getimages->embed
                );
        }
    }
    echo json_encode($data);
}
public function deletevideoAction($vidid) {
    $vid = Featuredvideo::findFirst('vidid="'. $vidid.'"');
    if ($vid) {
        if ($vid->delete()) {
            $data[]=array('success' => "");
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data );
}



}
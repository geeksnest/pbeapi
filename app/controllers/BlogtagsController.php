<?php

namespace Controllers;

use \Models\Blogtag as Tags;
use \Controllers\ControllerBase as CB;

class BlogtagsController extends \Phalcon\Mvc\Controller {
   public function managetagsAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $tag = Tags::find();
        } else {
            $conditions = "tags LIKE '%" . $keyword . "%'";
            $tag = Tags::find(array($conditions));
        }
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $tag,
                "limit" => 10,
                "page" => $currentPage
                )
            );
        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'tags' => stripslashes($m->tags),
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }


    public function validatetagsAction($tags){
        $validatetags = Tags::find(array("tags='".mysql_real_escape_string($tags)."'"));
        echo json_encode(count($validatetags));
    }
    public function createtagsAction(){

       $data = array(); 
       $createTags = addslashes($_POST['tags']);
       $tagsnames = new Tags();
       $tagsnames->assign(array(
        'tags' => $createTags,
        'slugs' => $_POST['slugs'],
        'created_at'=>date("Y-m-d H:i:s"),
        'updated_at'=>date("Y-m-d H:i:s"),
        ));
        if (!$tagsnames->save()){
        $errors = array();
                foreach ($tagsnames->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
        } 
        else{
         $data['success'] = "Success";
          $audit = new CB();
          $audit->auditlog(array( "module" =>"Center News","event" => "Add","title" => "Add Tag - ".$createTags." "));
        }
        echo json_encode($data);
    }
     public function tagsdeleteAction($id) {


        $conditions = "id=" . $id;
        $news = Tags::findFirst(array($conditions));
        $tag = $news->tags;
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'Category Deleted');
                 //START Log
                $audit = new CB();
                $audit->auditlog(array(
                  "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
                  "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                  "title" => "Delete Tag - ".$tag." ", /*// Maybe some info here (confuse) XD*/
                  ));
                //END Audit Log
            }
        }




        
        echo json_encode($data);
    }
    public function updatetagsAction() {


        function clean($string) {
           $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
           $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
           return strtolower(preg_replace('/-+/', '-', $string)); // Replaces multiple hyphens with single one.
        }

        $name = addslashes($_POST['name']); 
        $Id = $_POST['id']; 

        $ifexist = Tags::find("id='". $Id ."' and tags ='".mysql_real_escape_string($_POST['name'])."'");
        if(count($ifexist)==0){
        $data = array();
        $news = Tags::findFirst('id='. $Id . '');
        $news->tags = $name;
        $news->slugs = clean($name);
        $news->updated_at = date("Y-m-d H:i:s");
        if (!$news->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } 
        else{
            $data['success'] = "Success";
              $audit = new CB();
              $audit->auditlog(array("module" =>"Center News", "event" => "Update", "title" => "Update Tag - ".$tagname." "));
        }

        }else{
            $data['error'] = "error";
        }
       
        echo json_encode($data);
    }



}
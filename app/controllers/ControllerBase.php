<?php

namespace Controllers;
use \Models\Auditlog as Auditlog; //Audit Log Table

class ControllerBase extends \Phalcon\Mvc\Controller {

    public function modelsManager($phql) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $result = $app->modelsManager->executeQuery($phql);
    }

    public function getConfig(){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $app->config;
    }

    public function sendMail($email, $subject,$content){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From' => $app->config->postmark->signature,
            'To' => $email,
            'Subject' => $subject,
            'HtmlBody' => $content
            ));
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
            ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
        return $response;
    }

    public function userid(){
      $request = new \Phalcon\Http\Request();
      return $request->getheader("AUTHORIZATION");
    }
    public function auditlog($data){
        $num = 0; 
        foreach ($data as $key => $value) {
            $num++;
            if(count($data)>$num){
               $c = ',';
           }else{
               $c = '';
           }
           $datacont.= trim("auditlog.".$key."".$c);
           $dataval.="'".$value."'".$c;
       }
       $auditid = new \Utilities\Guid\Guid();
       $auditid->GUID();
       $db = \Phalcon\DI::getDefault()->get('db');
       $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,".$datacont.") VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$this->userid()."',".$dataval.")");
       $sql->execute();

   }
   public function time_elapsed_string($datetime, $full = false) {
        $now =  new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

}

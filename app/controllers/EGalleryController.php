<?php

namespace Controllers;
use \Models\Egallery as Egallery;
use \Models\Evideo as Evideo;
use \Controllers\ControllerBase as CB;
class EGalleryController extends \Phalcon\Mvc\Controller {
  public function ListBannerAction() {
    $getimages = Egallery::find(array("order" => "id DESC"));
    if(count($getimages) == 0){
      $data['error']=array('NOIMAGE');
    }else{
      foreach ($getimages as $getimages) 
      {
        $data[] = array(
          'id'=>$getimages->id,
          'filename'=>$getimages->filename,
          'title'=>$getimages->title,
          'description'=>$getimages->description,
          );
      }
    }
    echo json_encode($data);
  }

  public function ListBannerFeaAction($id) {
    $getimages = Egallery::find(array("usrid='".$id."'", "order" => "id DESC"));
    if(count($getimages) == 0){
      $data['error']=array('NOIMAGE');
    }else{
      foreach ($getimages as $getimages) 
      {
        $data[] = array(
          'id'=>$getimages->id,
          'filename'=>$getimages->filename,
          'title'=>$getimages->title,
          'description'=>$getimages->description,
          );
      }
    }
    echo json_encode($data);
  }

  public function ListEvideoAction() {
    $getimages = Evideo::find(array("order" => "id DESC"));
    if(count($getimages) == 0){
      $data['error']=array('NOIMAGE');
    }else{
      foreach ($getimages as $getimages){
        $data[] = array(
          'id'=>$getimages->id,
          'embed'=>$getimages->embed,
          'filename'=>$getimages->thumbnail,
          'title'=>$getimages->title,
          'description'=>$getimages->description,
          );
      }
    }
    echo json_encode($data);
  }

   public function ListEvideoFeAction($id) {
    $getimages = Evideo::find(array("userid='".$id."'","order" => "id DESC"));
    if(count($getimages) == 0){
      $data['error']=array('NOIMAGE');
    }else{
      foreach ($getimages as $getimages){
        $data[] = array(
          'id'=>$getimages->id,
          'embed'=>$getimages->embed,
          'filename'=>$getimages->thumbnail,
          'title'=>$getimages->title,
          'description'=>$getimages->description,
          );
      }
    }
    echo json_encode($data);
  }

  public function BlogBannerAction(){
    $filename     = $_POST['img'];
    $title        = $_POST['imgtitle'];
    $description  = $_POST['imgdesc'];

    $picture = new Egallery();
    $picture->assign(array(
      'filename' => "$filename",
      'title' => "$title",
      'description' => "$description"
      ));
    if (!$picture->save()) {
      $errors = array();
      foreach ($picture->getMessages() as $message) {$errors[] = $message->getMessage();}
      $data[]=array('error' => $errors);
    } else {
      $data[]=array('success' => 'Images has been uploaded');
    }
    echo json_encode($data);
  }

 public function BlogBannerFEAction(){
    $filename     = $_POST['img'];
    $title        = $_POST['imgtitle'];
    $description  = $_POST['imgdesc'];
     $userid  = $_POST['userid'];

    $picture = new Egallery();
    $picture->assign(array(
      'filename' => "$filename",
      'title' => "$title",
      'description' => "$description",
      'usrid' => "$userid"
      ));
    if (!$picture->save()) {
      $errors = array();
      foreach ($picture->getMessages() as $message) {$errors[] = $message->getMessage();}
      $data[]=array('error' => $errors);
    } else {
      $data[]=array('success' => 'Images has been uploaded');
    }
    echo json_encode($data);
  }

  public function EvideoAction(){
    $image     = $_POST['image'];
    $embed     = $_POST['embed'];
    $title     = $_POST['title'];
    $desc      = $_POST['desc'];

    $picture = new Evideo();
    $picture->assign(array(
      'embed'       => "$embed",
      'thumbnail'   => "$image",
      'title'       => "$title",
      'description' => "$desc"
      ));
    if (!$picture->save()) {
      $errors = array();
      foreach ($picture->getMessages() as $message) {$errors[] = $message->getMessage();}
      $data[]=array('error' => $errors);
    } else {
      $data[]=array('success' => 'Images has been uploaded');
    }
    echo json_encode($data);
  }

  public function EvideoFEAction(){
    $image     = $_POST['image'];
    $embed     = $_POST['embed'];
    $title     = $_POST['title'];
    $desc      = $_POST['desc'];
    $userid    = $_POST['userid'];

    $picture = new Evideo();
    $picture->assign(array(
      'embed'       => "$embed",
      'thumbnail'   => "$image",
      'title'       => "$title",
      'description' => "$desc",
      'userid'      => "$userid"
      ));
    if (!$picture->save()) {
      $errors = array();
      foreach ($picture->getMessages() as $message) {$errors[] = $message->getMessage();}
      $data[]=array('error' => $errors);
    } else {
      $data[]=array('success' => 'Images has been uploaded');
    }
    echo json_encode($data);
  }


  public function UBlogBannerAction(){
    $id           = $_POST['id'];
    $title        = $_POST['imgtitle'];
    $description  = $_POST['imgdesc'];

    $picture = Egallery::findFirst('id="'. $id.'"');
    $picture->assign(array(
      'title' => "$title",
      'description' => "$description"
      ));
    if (!$picture->save()) {
      $errors = array();
      foreach ($picture->getMessages() as $message) {$errors[] = $message->getMessage();}
      $data[]=array('error' => $errors);
    } else {
      $data[]=array('success' => 'Images has been uploaded');
    }
    echo json_encode($data);
  }


  public function UeVideoAction(){
    $id        = $_POST['id'];
    $image     = $_POST['image'];
    $embed     = $_POST['embed'];
    $title     = $_POST['title'];
    $desc      = $_POST['desc'];

    $picture = Evideo::findFirst('id="'. $id.'"');
    $picture->assign(array(
      'embed'       => $embed,
      'thumbnail'   => $image,
      'title'       => $title,
      'description' => $desc
      ));
    if (!$picture->save()) {
      $errors = array();
      foreach ($picture->getMessages() as $message) {$errors[] = $message->getMessage();}
      $data[]=array('error' => $errors);
    } else {
      $data[]=array('success' => 'Images has been uploaded');
    }
    echo json_encode($data);
  }


  public function DeleteBannerAction($imgid) {
    $img = Egallery::findFirst('id="'. $imgid.'"');
    if ($img) {
      if ($img->delete()) {
        $data[]=array('success' => "");
      }else{
        $data[]=array('error' => '');
      }
    }else{
      $data[]=array('error' => '');
    }
    echo json_encode($data);
  }


  public function DeleteVideoAction($imgid) {
    $img = Evideo::findFirst('id="'. $imgid.'"');
    if ($img) {
      if ($img->delete()) {
        $data[]=array('success' => "");
      }else{
        $data[]=array('error' => '');
      }
    }else{
      $data[]=array('error' => '');
    }
    echo json_encode($data);
  }
}
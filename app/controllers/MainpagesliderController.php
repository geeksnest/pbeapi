<?php

namespace Controllers;

use \Models\Mainpageslider as Mainpageslider;
use \Controllers\ControllerBase as CB;

class MainpagesliderController extends \Phalcon\Mvc\Controller {
    public function mainpagesliderAction() {
        $getimages = Mainpageslider::find();
        if(count($getimages) == 0){
                $data['error']=array('NOIMAGE');
            }else{
                foreach ($getimages as $getimages) 
                    {
                        $data[] = array(
                            'id'=>$getimages->id,
                            'filename'=>$getimages->filename
                            );
                    }
             }
            echo json_encode($data);
    }
    public function deleteimageAction($imgid) {
        $img = Mainpageslider::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                 //START Log
                  $audit = new CB();
                  $audit->auditlog(array(
                      "module" =>"Main Slider", /*//Examaple News, Create Center, Slider, Events etc...*/
                      "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                      "title" => "Delete Image - ".$filename." ", /*// Maybe some info here (confuse) XD*/
                      ));
                 //END Audit Log
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Main Slider", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Image - ".$filename." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
        echo json_encode($data);
    }
    public function uploadimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Mainpageslider();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
          $data['error']=array('Something went wrong saving the data, please try again.');
        } else {
          $data['success']=array('Images has been uploaded');
          //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Main Slider", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Add Image - ".$filename." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
      echo json_encode($data);
    }
   
}
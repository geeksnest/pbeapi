<?php

namespace Controllers;
use \Models\School as School;
use \Controllers\ControllerBase as CB;

class SchoolController extends \Phalcon\Mvc\Controller {
 public function managetagsAction($num, $page, $keyword) {
  if ($keyword == 'null' || $keyword == 'undefined') {
    $tag = School::find();
  } else {
    $conditions = "name LIKE '%" . $keyword . "%'";
    $tag = School::find(array($conditions));
  }
  $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
  $paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
      "data" => $tag,
      "limit" => 10,
      "page" => $currentPage
      )
    );
        // Get the paginated results
  $page = $paginator->getPaginate();

  $data = array();
  foreach ($page->items as $m) {
    $data[] = array(
      'id' => $m->id,
      'name' => $m->name,
      );
  }
  $p = array();
  for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
  }
  echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}


public function validatetagsAction($tags){
  $validatetags = School::find(array("name='".$tags."'"));

  echo json_encode(count($validatetags));
}
public function createtagsAction(){

 $data = array(); 

 $tagsnames = new School();
 $tagsnames->assign(array(
  'name' => $_POST['tags'],
  'slugs' => $_POST['slugs'],
  'created_at'=>date("Y-m-d H:i:s"),
  'updated_at'=>date("Y-m-d H:i:s"),
  ));
 if (!$tagsnames->save()){
  $errors = array();
  foreach ($tagsnames->getMessages() as $message) {
    $errors[] = $message->getMessage();
  }
  echo json_encode(array('error' => $errors));
} 
else{
 $data['success'] = "Success";
          //START Log
 $audit = new CB();
 $audit->auditlog(array(
  "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
  "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
  "title" => "Add Tag - ".$_POST['tags']." ", /*// Maybe some info here (confuse) XD*/
  ));
          //END Audit Log
}
echo json_encode($data);
}
public function tagsdeleteAction($id) {


  $conditions = "id=" . $id;
  $news = School::findFirst(array($conditions));
  $tag = $news->name;
  $data = array('error' => 'Not Found');
  if ($news) {
    if ($news->delete()) {
      $data = array('success' => 'Category Deleted');
                 //START Log
      $audit = new CB();
      $audit->auditlog(array(
        "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
        "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
        "title" => "Delete Tag - ".$tag." ", /*// Maybe some info here (confuse) XD*/
        ));
                //END Audit Log
    }
  }




  
  echo json_encode($data);
}
public function updatetagsAction($tagname,$id) {

  function clean($string) {
         $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
         return strtolower(preg_replace('/-+/', '-', $string)); // Replaces multiple hyphens with single one.
       }

       $ifexist = School::find("id='". $id ."'");


       if(count($ifexist)!=0){
        $data = array();
        $news = School::findFirst('id='. $id . '');
        $news->name = $tagname;
        $news->slugs = clean($tagname);
        $news->updated_at = date("Y-m-d H:i:s");
        if (!$news->save()) {
          $data['error'] = "Something went wrong saving the data, please try again.";
        } 
        else{
          $data['success'] = "Success";
             //START Log
          $audit = new CB();
          $audit->auditlog(array(
            "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
            "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
            "title" => "Update Tag - ".$tagname." ", /*// Maybe some info here (confuse) XD*/
            ));
             //END Audit Log
        }

      }else{
        $data['error'] = "error";
      }
      
      echo json_encode($data= count($ifexist));
    }



  }
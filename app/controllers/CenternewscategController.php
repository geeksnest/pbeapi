<?php

namespace Controllers;
use \Models\Centernews as Centernews;
use \Models\Author as Author;
use \Models\Center as Center;
use \Models\Centernewstags as Tags;
use \Models\Centernewscategory as Category;
use \Controllers\ControllerBase as CB;


class CenternewscategController extends \Phalcon\Mvc\Controller {


    public function validatecategAction($categ){
        $validate = Category::find(array("categoryname='".mysql_real_escape_string($categ)."'"));
        echo json_encode(count($validate));
    }

     public function validatetagsAction($tags){
        $validatetags = Tags::find(array("tags='".mysql_real_escape_string($tags)."'"));
        echo json_encode(count($validatetags));
    }



    public function managecategoryAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Category::find();
        } else {
            $conditions = "categoryname LIKE '%" . $keyword . "%'";
            $Pages = Category::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'categoryid' => $m->categoryid,
                'categoryname' => stripslashes($m->categoryname),
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }
    public function createcategoryAction(){
       $data = array(); 
       $catnames = new Category();
       $createCateg = addslashes($_POST['catnames']); 
       $catnames->assign(array(
        'categoryname' => $createCateg,
        'categoryslugs' => $_POST['slugs'],
        'created_at'=>date("Y-m-d H:i:s"),
        'updated_at'=>date("Y-m-d H:i:s"),
        ));
       if (!$catnames->save()){
        $errors = array();
        foreach ($catnames->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        echo json_encode(array('error' => $errors));
    } 
    else{
     $data['success'] = "Success";
     $audit = new CB();
      $audit->auditlog(array("module" =>"Center News", "event" => "Add", "title" => "Add Category - ".$createCateg." "));
 }
 echo json_encode($data);
}
public function categorydeleteAction($id) {
    $conditions = "categoryid=" . $id;
    $news = Category::findFirst(array($conditions));
    $catname = $news->categoryname;
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'Category Deleted');
             //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Center news Category", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Category - ".$catname." ", /*// Maybe some info here (confuse) XD*/
              ));
             //END Audit Log
        }
    }
    echo json_encode($data);
} 
public function updatecategoryAction() {
function clean($string) {
      $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
      $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
      return strtolower(preg_replace('/-+/', '-', $string)); // Replaces multiple hyphens with single one.
  }

  $Categ = addslashes($_POST['name']); 
  $Id = $_POST['id']; 
  $ifexist = Category::find("categoryid='". $Id ."' and categoryname='".mysql_real_escape_string($_POST['name'])."'");
  if(count($ifexist)==0){
    $news = Category::findFirst('categoryid=' . $Id . ' ');
    $news->categoryname = $Categ;
    $news->categoryslugs =clean($Categ);
    $news->updated_at = date("Y-m-d H:i:s");
    if (!$news->save()){
      $data['error'] = "Something went wrong saving the data, please try again.";
    } 
    else {
      $data['success'] = "Success";
      $audit = new CB();
      $audit->auditlog(array("module" =>"Center news Category", "event" => "Update", "title" => "Update Category - ".$Categ." "));
    }      
  }else{
    $data['error'] = "error";
  }
  echo json_encode($data);
}

public function listcategoryAction()
{

    $getcategory= Category::find(array("order" => "categoryid ASC"));
    foreach ($getcategory as $getcategory) {
        $data[] = array(
            'categoryid'=>$getcategory->categoryid,
            'categoryname'=>stripslashes($getcategory->categoryname)
            );
    }
    echo json_encode($data);

}    




public function felistcategoryAction($num, $page, $keyword, $categslug) {
    $getid = Category::findFirst(array("categoryslugs='".$categslug."'"));
    $Pages = Centernews::find(array("category = '".$getid->categoryid."'"));
    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );
        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'newsid' => $m->newsid,
            'slug' => $m->slugs,
            'authorname'  => $getAuthor->name,
            'title' => $m->title,
            'description' => $m->description,
            'body' => $m->body,
            'publish' => $m->publish,
            'banner' => $m->banner,
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}
}
<?php

namespace Controllers;

use \Models\Author as Author;
use \Models\Authorimage as Authorimage;
use \Models\Authormedia as Authormedia;

use \Controllers\ControllerBase as CB;

class AuthorController extends \Phalcon\Mvc\Controller {

    public function saveauthorAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();

        $id = $guid->GUID();
        $name= $request->getPost('name');
        $location= $request->getPost('location');
        $occupation= $request->getPost('occupation');
        $since= $request->getPost('since');
        $about= $request->getPost('about');
        $image= $request->getPost('image');
        $userid= $request->getPost('userid');


                //Social Media
        $facebook   = $request->getPost('facebook');
        $twitter    = $request->getPost('twitter');
        $google     = $request->getPost('google');
        $yahoo      = $request->getPost('yahoo');

        //date converter
        $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
        $dates = explode(" ", $since);
        $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

        $add = new Author();
        $add->assign(array(
            'id' => $id,
            'name' => $name,
            'location' => $location,
            'occupation' => $occupation,
            'authorsince' => $d,
            'about' => $about,
            'image' => $image,
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s"),
            ));
                    // $add->save();
        if (!$add->save()) {
            $errors = array();
            foreach ($add->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
            $data['error'] ="!SAVE";
        } 

        else{
            $data['success'] ="SAVE";
            //START Log
            $audit = new CB();
            $audit->auditlog(array( "module" =>"Author","event" => "Add", "title" => "Author : ".$name.""));
            //SOcial Media
            $add = new Authormedia();
            $add->assign(array(
                'authorid' => $id,
                'facebook' => $facebook,
                'twitter'  => $twitter,
                'google'   => $google,
                'yahoo'    => $yahoo
                ));
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 




        }
        echo json_encode(array($data));
    }


    public function listauthorAction($num, $page, $keyword){
        if ($keyword == 'undefined') {
            $listauthor = Author::find();
        } else {
            $conditions = "name LIKE '%" . $keyword . "%' 
            or location LIKE '%" . $keyword . "%'
            or authorsince LIKE '%" . $keyword . "%'";
            $listauthor= Author::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $listauthor,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'name' => $m->name,
                'location' => $m->location,
                'occupation' => $m->occupation,
                'authorsince' => $m->authorsince
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }
    public function authordeleteAction($id,$userid){
        $data = array();
        $dlt = Author::findFirst('id="' . $id . '"');
        $name = $dlt->name;
        if ($dlt) {
            if($dlt->delete()){
                $data['success'] = "deleted";
                //START Log
                $audit = new CB();
                $auditid = new \Utilities\Guid\Guid();
                $auditid->GUID();
                $audit->auditlog(array(
                    "module" =>"Author", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "Delete Author - ".$name." ", /*// Maybe some info here (confuse) XD*/
                    ));                 
                //END Audit Log
            }else {
                $data['error'] = "notdeleted";
            }
            echo json_encode($data);
        }
    }
    //IMAGE UPLOAD
    public function saveimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Authorimage();
        $picture->assign(array(
            'filename' => $filename
            ));
        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] ="SAVE";
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Author", /*//Examaple News, Create Center, Slider, Events etc...*/
                "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                "title" => "Add Image - ".$filename." ", /*// Maybe some info here (confuse) XD*/
                ));
            //END Audit Log
        }
    }
    //IMAGE DELETE
    public function deleteimageAction($imgid,$userid) {
        $img = Authorimage::findFirst('id="'. $imgid.'"');
        $name = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                //START Log
                $audit = new CB();
                $audit->auditlog(array(
                   "module" =>"Author", /*//Examaple News, Create Center, Slider, Events etc...*/
                   "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                   "title" => "Delete Image - ".$name." ", /*// Maybe some info here (confuse) XD*/
                   ));
                //END Audit Log
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }

    public function listimageAction() {

        $getimages = Authorimage::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) 
            {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->filename
                    );
            }
        }
        echo json_encode($data);

    }

    ///EDIT 
    public function authoreditAction($id) {
        $data = array();
        $author = Author::findFirst('id="' . $id . '"');
        $_media = Authormedia::find('authorid="' . $id . '"');

        if(count($_media)!=0){
            $_author= Authormedia::findFirst('id="' . $_media[0]->id. '"');
            $facebook = $_author->facebook;$twitter =  $_author->twitter;$google =  $_author->google;$yahoo =  $_author->yahoo;
        }else{$facebook = "";$twitter = "";$google = ""; $yahoo = "";}


        if ($author) {
            $data = array(
                'id' => $author->id,
                'name' => $author->name,
                'location' => $author->location,
                'occupation' => $author->occupation,
                'authorsince' => $author->authorsince,
                'about' => $author->about,
                'image' => $author->image,
                'facebook' => $facebook,
                'twitter' => $twitter,
                'google' => $google,
                'yahoo' => $yahoo
                );
        }
        echo json_encode($data);
    }
    //Update
    public function updateauthorAction(){
        $request = new \Phalcon\Http\Request();

        $id = $request->getPost('id');
        $name= $request->getPost('name');
        $location= $request->getPost('location');
        $occupation= $request->getPost('occupation');
        $since= $request->getPost('authorsince');
        $hiddendate= $request->getPost('hiddendate');
        $about= $request->getPost('about');
        $image= $request->getPost('image');
        $userid= $request->getPost('userid');

        //Social Media
        $facebook   = $request->getPost('facebook');
        $twitter    = $request->getPost('twitter');
        $google     = $request->getPost('google');
        $yahoo      = $request->getPost('yahoo');


        //date converter
        $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');

        if(strlen($since) <= 11){
            $d = $hiddendate;
        }else{
            $dates = explode(" ", $since);
            $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
        }

        $add = Author::findFirst('id="' . $id . '"');
        $add->assign(array(
            'id' => $id,
            'name' => $name,
            'location' => $location,
            'occupation' => $occupation,
            'authorsince' => $d,
            'about' => $about,
            'image' => $image,
            )); 
            // $add->save();
        if (!$add->save()) {
            $errors = array();
            foreach ($add->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
            $data['error'] ="!SAVE";
        } 
        else{
            $data['success'] ="SAVE";
                $audit = new CB();
                $audit->auditlog(array("module" =>"Author","event" => "Update","title" => "Update - ".$name." "));
                $_media = Authormedia::find('authorid="' . $id . '"');
            if(count($_media)==0){

               $add = new Authormedia();  
               $data = "add";  
            }else{
                $add = Authormedia::findFirst('id="'.$_media[0]->id.'"');
                $data = "update";  
            }
            $add->assign(array(
                'authorid' => $id,
                'facebook' => $facebook,
                'twitter'  => $twitter,
                'google'   => $google,
                'yahoo'    => $yahoo
                ));
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            }
        }
        echo json_encode(array($data));    
    }  
}
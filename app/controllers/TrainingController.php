<?php
namespace Controllers;
use \Models\Pagewdsub as Pagewdsub;
use \Models\Submitclass as Submitclass;
use \Models\Tmedia as Tmedia;
use \Models\Lastview as Lastview;
use \Controllers\ControllerBase as CB;
class TrainingController extends \Phalcon\Mvc\Controller {
    public function ListtrainingAction() {
        $getimages = Pagewdsub::find(array("order" => "idno DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $get) 
            {
                $data[] = array(
                    'idno'=>$get->idno,
                    'page'=>$get->parentpage,
                    'title'=>$get->title,
                    'description'=>$get->description,
                    'metatitle'=>$get->metatitle,
                    'logo'=>$get->logo,
                    'theme'=>$get->theme,
                    );
            }
        }
        echo json_encode($data);
    }
    public function UserListtrainingAction($userid, $classfor, $state) {
        switch ($classfor) {
            case 'teacher':
                $addquery = "and classfor='teacher'";
                break;
            case 'mentor':
                $addquery = "and (classfor='mentor' or classfor='teacher')";
                break;
            default:
                $addquery = "";
                break;
        }

        $Pagewdsub = Pagewdsub::find(array("order" => "sort ASC"));
        if(count($Pagewdsub) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($Pagewdsub as $get){
                if($state == "tr"){
                    $geclass = Tmedia::find(array("trainingid='".$get->idno."' ".$addquery.""));
                }else{
                    $geclass = Tmedia::find(array("trainingid='".$get->idno."'"));    
                }
             
                if(count($geclass) == 0){
                    $percent = 0;
                    $_show = true; //TEMPORARY in true value
                }else{
                    $_show = true;
                    $count = 0;
                    foreach ($geclass as $_get){
                        $ifsubmit = Submitclass::find(array("userid='$userid' and classid='$_get->idno'"));
                        if(count($ifsubmit)!=0){
                            $count++;
                        }
                    }
                    $percent = ($count / count($geclass)) * 100;
                }
                $data[] = array(
                    'idno'=>$get->idno,
                    'page'=>$get->parentpage,
                    'title'=>$get->title,
                    'slug'=>str_replace(" ", "-", $get->title),
                    'description'=>$get->description,
                    'metatitle'=>$get->metatitle,
                    'logo'=>$get->logo,
                    'theme'=>$get->theme,
                    'percent'=>trim(round($percent).'%'),
                    'showsubpage'=>$_show,
                    );
            }
        }
       echo json_encode($data);
    }

    public function edittrainingAction($idno) {
        $Pagewdsub = Pagewdsub::find(array("idno='".$idno."'"));
        if(count($Pagewdsub) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($Pagewdsub as $get) 
            {
                $data[] = array(
                    'idno'=>$get->idno,
                    'title'=>$get->title,
                    'description'=>$get->description,
                    'metatitle'=>$get->metatitle,
                    'logo'=>$get->logo,
                    'tlogo'=>$get->logo,
                    'theme'=>$get->theme,
                    );
            }
        }
        echo json_encode($data);
    }
    
    public function SaveTrainingAction(){
        $page = $_POST['page'];
        $title = $_POST['title'];
        $metatitle = $_POST['metatitle'];
        $description = $_POST['description'];
        $logo = $_POST['logo'];
        $theme = $_POST['theme'];
        $guid = new \Utilities\Guid\Guid();
        $idno = $guid->GUID();
        $_save = new Pagewdsub();
        $_save->assign(array(
            'idno' => $idno ,
            'parentpage' => $page ,
            'title' => $title ,
            'metatitle' => $metatitle,
            'description' => $description,
            'logo' => $logo ,
            'theme' => $theme,
            'created_at'    =>date("Y-m-d H:i:s"),
            'updated_at'    =>date("Y-m-d H:i:s"),
            ));
        if (!$_save->save()) {
          $data[]=array('error' => 'Something went wrong saving the data, please try again.');
      } else {
          $data[]=array('success' => 'successfully save');
      }
      echo json_encode(array("result"=>$data, "idno"=>$idno));
  }


  public function UpdateTrainingAction(){
    $idno = $_POST['idno'];
    $title = $_POST['title'];
    $metatitle = $_POST['metatitle'];
    $description = $_POST['description'];
    $logo = $_POST['logo'];
    $theme = $_POST['theme'];


    $_save = Pagewdsub::findFirst('idno="'. $idno.'"');
    $_save->assign(array(
        'title' => $title ,
        'metatitle' => $metatitle,
        'description' => $description,
        'logo' => $logo,
        'theme' => $theme, 
        ));
    if (!$_save->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'successfully save');
  }
  echo json_encode($data);
}
public function DeleteTrainingAction($idno) {
    $img = Pagewdsub::findFirst(array("idno='".$idno."'"));
    if ($img) {
        if ($img->delete()) {
            $data[]=array('success' => "");
        }else{
            $errors = array();
            foreach ($img->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        }
    }else{
        $data[]=array('error' => 'Data not available');
    }
    echo json_encode($data);
}
public function SubmitTrainingAction(){
    $usrid = $_POST['usrid'];
    $classid = $_POST['classid'];
    $msg = $_POST['msg'];
    $_checkifexist = Submitclass::find(array("userid='$usrid' and classid='$classid'"));
    if(count($_checkifexist) == 0){
        $_save = new Submitclass();
        $_save->assign(array(
            'userid' => $usrid ,
            'classid' => $classid ,
            'message' => $msg,
            'created_at'    =>date("Y-m-d H:i:s"),
            'updated_at'    =>date("Y-m-d H:i:s"),
            ));
        if (!$_save->save()) {
          $data[]=array('error' => 'Something went wrong saving the data, please try again.');
        } else {
          $data[]=array('success' => 'successfully save');
        }
    }else{
        $_save = Submitclass::findFirst(array('id="'.$_checkifexist[0]->id.'"'));
   
        $_save->assign(array(
            'message' => $msg,
            ));
        if (!$_save->save()) {
          $data[]=array('error' => 'Cant Update Something Went Wrong.');
        } else {
          $data[]=array('success' => 'successfully UPdated');
        }
    }
  echo json_encode($data);
}
public function getsentTrainingAction($userid, $classid) {
    $getimages = Submitclass::find(array("userid='$userid' and classid='$classid'"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $get) 
        {
            $data[] = array(
                'message'=>$get->message,
                );
        }
    }
    echo json_encode($data);
}
public function lastTrainingAction(){
    $userid = $_POST['userid'];
    $title = $_POST['title'];
    $link = $_POST['link'];
    $_save = new Lastview();
    $_save->assign(array(
        'userid' => $userid ,
        'classtitle' => $title ,
        'link' => $link,
        'created_at'    =>date("Y-m-d H:i:s"),
        'updated_at'    =>date("Y-m-d H:i:s"),
        ));
    if (!$_save->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'successfully save');
  }
  echo json_encode($data);
}
public function ListvisitAction($usrid) {
    $db = Lastview::find(array("userid='$usrid'","order" => "id DESC LIMIT 1"));


    if(count($db) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($db as $get){
            $_tmendia = Tmedia::findFirst(array("title='".$get->classtitle."'"));
            $_pagewdsub = Pagewdsub::findFirst(array("idno='".$_tmendia->trainingid."'"));
            $data[] = array(
                'userid'=>$get->userid,
                'classtitle'=>$get->classtitle,
                'link'=>$get->link,
                'vimeo'=>$_tmendia->embed,
                'pagewdsub'=>$_pagewdsub->logo,

                );
        }
    }
    echo json_encode($data);
}
}
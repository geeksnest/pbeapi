<?php
namespace Controllers;
use \Models\Blogvid as Video;
use \Models\Blogbanner as Banner;
use \Models\Blog as Blog;
use \Models\Author as Author;
use \Models\Blogslcttags as Blogtags;
use \Models\Blogslctcateg as BlogCateg;
use \Models\Blogcateg as Centernewscategory;
use \Models\Blogtag as Centernewstags;
use \Models\Blogtag as Tags;
use \Controllers\ControllerBase as CB;
class BlogController extends \Phalcon\Mvc\Controller {
    public function BlogVideoAction() {
       $request = new \Phalcon\Http\Request();
       if($request->isPost()){
        $embed = $request->getPost('video');
        $byspace = explode(" ", $embed);
        $byslash = explode("/", $byspace[3]);
        $utubevidid = trim(str_replace('"',' ', $byslash[4]));
        $guid = new \Utilities\Guid\Guid();
        $guid->GUID();
        $saveembed = new Video();
        $saveembed->assign(array(
            'id' => $guid->GUID(),
            'vidid' => 'Video',
            'embed' => $embed
            ));
        if (!$saveembed->save()) {
            $errors = array();
            foreach ($saveembed->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data[]=array('error' => $errors);
        } else {
            $data[]=array('success' => "New Video Has Been Uploaded");
        }
    }
    echo json_encode($data);
}

public function ListVideoAction() {
    $getimages = Video::find();
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $getimages) {
            $data[] = array(
                'vidid'=>$getimages->id,
                'embedid'=>$getimages->vidid,
                'embed'=>$getimages->embed
                );
        }
    }
    echo json_encode($data);
}

public function deletevideoAction($vidid) {
    $vid = Video::findFirst('id ="'. $vidid.'"');
    if ($vid) {
        if ($vid->delete()) {
            $data[]=array('success' => "");
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data );
}

public function ListBannerAction() {
    $getimages = Banner::find(array("order" => "id DESC"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
    }
    echo json_encode($data);
}

public function BlogBannerAction(){
    $filename = $_POST['filename'];
    $picture = new Banner();
    $picture->assign(array(
        'filename' => $filename
        ));

    if (!$picture->save()) {
        $errors = array();
        foreach ($picture->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        echo json_encode(array('error' => $errors));
      // $data[]=array('error' => 'Something went wrong saving the data, please try again.');
    } else {
      $data=array('success' => 'Images has been uploaded');
  }
  echo json_encode($data);
}

public function DeleteBannerAction($imgid) {
    $img = Banner::findFirst('id="'. $imgid.'"');
    if ($img) {
        if ($img->delete()) {
            $data[]=array('success' => "");
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data);
}

public function SaveBlogAction() {
    $request = new \Phalcon\Http\Request();
    if($request->isPost()){
        $title          = addslashes($request->getPost('title'));
        $slugs          = $request->getPost('slugs');
        $author         = $request->getPost('author');
        $date           = $request->getPost('date');
        $description    = $request->getPost('summary');
        $body           = $request->getPost('body');
        $status         = $request->getPost('status');
        $featuredtype   = $request->getPost('featuredthumbtype');
        $featuredthumb  = $request->getPost('featuredthumb');
        $metadesc       = $request->getPost('metadesc');
        $metakeyword    = $request->getPost('metakeyword');
        $metatitle      = $request->getPost('metatitle');
        $tags           = $request->getPost('tag');
        $categ          = $request->getPost('category');
        $guid = new \Utilities\Guid\Guid();
        $_guid = new \Utilities\Guid\Guid();
        $_checkifexist = Blog::find('blogid="'. $guid->GUID().'"');
        $blogid = (count($_checkifexist)!=0 ? $guid->GUID() : $_guid->GUID());
        $page = new Blog();
        $page->assign(array(
            'blogid'         => $blogid,
            'title'         => $title,
            'slug'          => $slugs,
            'authorid'      => $author,
            'publish'       => $date,
            'featuredtype'  => $featuredtype,
            'featured'      => $featuredthumb,
            'description'   => $description,
            'body'          => $body,
            'status'        => $status,
            'metadesc'      => $metadesc,
            'metakeyword'   => $metakeyword,
            'metatitle'     => $metatitle,
            'created_at'    =>date("Y-m-d H:i:s"),
            'updated_at'    =>date("Y-m-d H:i:s")
            ));
        if (!$page->save()) {
            $errors = array();
            foreach ($page->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data[]=array('error' => $errors);
        } else {

         $data['success'] = "Success cat";

         $audit = new CB();
         $audit->auditlog(array("module" =>"Blog","event" => "Add","title" => "Add Blog- ".$title." "));
         foreach($categ as $cat){
            $catnews = new BlogCateg();
            $catnews->assign(array(
                'blogid' => $blogid,
                'categ' => $cat
                ));
            if (!$catnews->save()){
                $errors = array();
                foreach ($page->getMessages() as $message){
                    $errors[] = $message->getMessage();
                }
                //echo json_encode(array('error' => $errors));
            }else{
                // $data['success'] = "Success cat";
            }
        }
        foreach($tags as $tag){
            $gettags = Tags::findFirst("tags='".mysql_real_escape_string($tag)."'");
            if(!$gettags){
                $_newtags = addslashes($tag);
                $newstags = new Tags();
                $newstags->assign(array(
                    'tags' => $_newtags,
                    'slugs' => str_replace("-", " ", $tag),
                    'created_at'    =>date("Y-m-d H:i:s"),
                    'updated_at'    =>date("Y-m-d H:i:s")
                    ));
                if (!$newstags->save()){
                   // $data['error'] = "Something went wrong saving the newstags, please try again.";
                }
                else{
                    $chkif = Blogtags::findFirst("tags='".$tag."'");
                    if(!$chkif){
                        $tagnews = new Blogtags();
                        $tagnews->assign(array(
                            'blogid' => $blogid,
                            'tags' => $tag
                            ));
                        if (!$tagnews->save()){
                            $errors = array();
                            foreach ($tagnews->getMessages() as $message){
                                $errors[] = $message->getMessage();
                            }
                            //echo json_encode(array('error' => $errors));
                        }else{
                            //$data['success'] = "Success cat";
                        }
                    }
                }
            }else{
             $chkif = Blogtags::findFirst("tags='".$tag."'");
             if(!$chkif){
                $tagnews = new Blogtags();
                $tagnews->assign(array(
                    'blogid' => $blogid,
                    'tags' => $tag
                    ));
                if (!$tagnews->save()){
                    $errors = array();
                    foreach ($tagnews->getMessages() as $message){
                        $errors[] = $message->getMessage();
                    }
                    //echo json_encode(array('error' => $errors));
                }else{
                    //$data['success'] = "Success cat";
                }
            }
        }
    }
}
}
echo json_encode($data);
}


public function manageBlogAction($num, $page, $keyword) {

    if($keyword == 'undefined' ){
       $Pages = Blog::find(array("order" => "publish DESC"));
   } 
   else {
    if($keyword !='undefined'){
        $conditions = " title LIKE '%" . $keyword . "%' OR slug LIKE '%" . $keyword . "%'";
        $Pages = Blog::find(array($conditions, "order" => "publish DESC"));
    }else{
        $conditions = "title LIKE '%" . $keyword . "%' OR slug LIKE '%" . $keyword . "%'";
        $Pages = Blog::find(array($conditions, "order" => "publish DESC"));
    }

}

$currentPage = (int) ($page);
$paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
        "data" => $Pages,
        "limit" => 10,
        "page" => $currentPage
        )
    );
$page = $paginator->getPaginate();

$data = array();
foreach ($page->items as $m) {
 $getAuthor = Author::findFirst('id="'.$m->authorid.'"');
 $data[] = array(
    'blogid' => $m->blogid,
    'authorname'  => $getAuthor->name,
    'title' => stripslashes($m->title),
    'slugs' => $m->slugs,
    'publish' => $m->publish,
    'status' => $m->status
    );
}
$p = array();
for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}


public function changestatusAction($id,$status){
    $getInfo = Blog::findFirst('blogid="'. $id .'"');
    $title = $getInfo->title;
    if($status == 1){
       $getInfo->status = 0;
       $getInfo->save();
       $data=array('success' => 'Deactivated');
       $audit = new CB();
       $audit->auditlog(array("module" =>"Blog","event" => "Deactivated", "title" => "Deactivated Blog : ". $title ."" ));
   }
   else{
       $getInfo->status = 1;
       $getInfo->save();
       $data=array('success' => 'Activated');
       $audit = new CB();
       $audit->auditlog(array("module" =>"Blog", "event" => "Activated", "title" => "Activated Blog : ". $title ."" ));
   }
   echo json_encode($data);
}



public function ListBlogAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM blog LEFT JOIN author on  author.id = blog.authorid  ORDER BY blog.blogid DESC ");
    $stmt->execute();
    $blog = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $x = 0;
    foreach($blog as $b=> $v){
        $stmt = $db->prepare("SELECT blogtag.tags FROM blogslcttags INNER JOIN blogtag ON blogslcttags.tags = blogtag.slugs WHERE blogslcttags.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $tags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $newarray = array();
        foreach($tags as $t => $l){
            $newarray[] = $l['tags'];
        }
        $blog[$x]['tagstext'] = join(',', $newarray);
        $stmt = $db->prepare("SELECT blogcateg.categoryname FROM blogslctcateg INNER JOIN blogcateg ON blogslctcateg.categ = blogcateg.categoryid WHERE blogslctcateg.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $cat = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $catarray = array();
        foreach($cat as $t => $l){
            $catarray[] = $l['categoryname'];
        }
        $blog[$x]['categoriestext'] = join(',', $catarray);
        $x++;
    }
    echo json_encode($blog);
}

public function FEListBlogAction($offset, $page) {
    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM blog LEFT JOIN author on  author.id = blog.authorid WHERE blog.status = '1'    ORDER BY blog.publish DESC  LIMIT $offset, $page");
    $stmt->execute();
    $blog = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    $x = 0;
    foreach($blog as $b=> $v){
        $stmt = $db->prepare("SELECT blogtag.tags FROM blogslcttags INNER JOIN blogtag ON blogslcttags.tags = blogtag.slugs WHERE blogslcttags.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $tags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $newarray = array();
        foreach($tags as $t => $l){
            $newarray[] = $l['tags'];
        }
        $blog[$x]['tagstext'] = join(',', $newarray);
        $stmt = $db->prepare("SELECT blogcateg.categoryname FROM blogslctcateg INNER JOIN blogcateg ON blogslctcateg.categ = blogcateg.categoryid WHERE blogslctcateg.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $cat = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $catarray = array();
        foreach($cat as $t => $l){
            $catarray[] = $l['categoryname'];
        }
        $blog[$x]['categoriestext'] = join(',', $catarray);
        $x++;
    }
    echo json_encode($blog);
}

public function readBlogAction($_id) {
    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM blog LEFT JOIN author on  author.id = blog.authorid WHERE blog.slug='$_id'  ORDER BY blog.blogid DESC LIMIT 1");
    $stmt->execute();
    $blog = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $x = 0;
    foreach($blog as $b=> $v){
        $stmt = $db->prepare("SELECT blogtag.tags, blogtag.slugs FROM blogslcttags INNER JOIN blogtag ON blogslcttags.tags = blogtag.tags WHERE blogslcttags.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $tags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        // $newarray = array();
        // foreach($tags as $t => $l){
        //     $newarray[] = $l['tags'];
        // }
        // $blog[$x]['tagstext'] = join($newarray);
        // $blog[$x]['tagstext'] = join($tags);

        $stmt = $db->prepare("SELECT blogcateg.categoryname, blogcateg.categoryslugs FROM blogslctcateg INNER JOIN blogcateg ON blogslctcateg.categ = blogcateg.categoryid WHERE blogslctcateg.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $cat = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        // $catarray = array();
        // foreach($cat as $t => $l){
        //     $catarray[] = $l['categoryname'];
        // }
        // $blog[$x]['categoriestext'] = join($catarray);
        // $blog[$x]['categoriestext'] = join($cat);
        // $x++;
    }
    echo json_encode(array("blog"=>$blog,"tag"=>$tags, "categ"=>$cat));
}

public function eListBlogAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM blog LEFT JOIN author on  author.id = blog.authorid WHERE blog.featuredtype ='banner'  ORDER BY blog.blogid DESC ");
    $stmt->execute();
    $blog = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    $x = 0;
    foreach($blog as $b=> $v){
        $stmt = $db->prepare("SELECT blogtag.tags FROM blogslcttags INNER JOIN blogtag ON blogslcttags.tags = blogtag.slugs WHERE blogslcttags.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $tags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $newarray = array();
        foreach($tags as $t => $l){
            $newarray[] = $l['tags'];
        }
        $blog[$x]['tagstext'] = join(',', $newarray);
        $stmt = $db->prepare("SELECT blogcateg.categoryname FROM blogslctcateg INNER JOIN blogcateg ON blogslctcateg.categ = blogcateg.categoryid WHERE blogslctcateg.blogid = '".$v['blogid']."'");
        $stmt->execute();
        $cat = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $catarray = array();
        foreach($cat as $t => $l){
            $catarray[] = $l['categoryname'];
        }
        $blog[$x]['categoriestext'] = join(',', $catarray);
        $x++;
    }
    echo json_encode($blog);
}


public function updateBlogAction() {
   $request = new \Phalcon\Http\Request();
   if($request->isPost()){
    $blogid         = $request->getPost('newsid');
    $title          = addslashes($request->getPost('title'));
    $slugs          = $request->getPost('slugs');
    $author         = $request->getPost('author');
    $date           = $request->getPost('date');
    $description    = $request->getPost('summary');
    $body           = $request->getPost('body');
    $status         = $request->getPost('status');
    $featuredtype   = $request->getPost('featuredthumbtype');
    $featuredthumb  = $request->getPost('featuredthumb');
    //META
    $metadesc         = $request->getPost('metadesc');
    $metakeyword      = $request->getPost('metakeyword');
    $metatitle        = $request->getPost('metatitle');
    //ARRAY
    $tags           = $request->getPost('tag');
    $categ          = $request->getPost('category');
    $page = Blog::findFirst('blogid="'. $blogid.'"');
    $page->assign(array(
        'title'         => $title,
        'slug'          => $slugs,
        'authorid'      => $author,
        'publish'       => $date,
        'featuredtype'  => $featuredtype,
        'featured'      => $featuredthumb,
        'description'   => $description,
        'body'          => $body,
        'status'        => $status,
        'metadesc'      => $metadesc,
        'metakeyword'   => $metakeyword,
        'metatitle'     => $metatitle
        ));
    if (!$page->save()) {
        $errors = array();
        foreach ($page->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {
        $data[]=array('success' => "New Center has been successfully Created!");

        $audit = new CB();
        $audit->auditlog(array("module" =>"Blog", "event" => "Add","title" => "Add Blog- ".$title." ")); 

        $_checkifexist = BlogCateg::find("blogid='".$blogid ."'");
        foreach($_checkifexist as $cat){

            if(count($_checkifexist)!=0){
                $dlt = BlogCateg::findFirst('num="'.$cat->num.'"');
                if ($dlt){
                    if ($dlt->delete()) {
                        $data[]=array('success' => "Category Deleted");
                        $dlt_count++;
                    }else{
                        $data[]=array('error' => '');
                    }
                }else{$data[]=array('error' => '');}
            }
        }

        foreach($categ as $cat){
            $checkifexist = BlogCateg::find("blogid='".$blogid ."' and categ='".$cat."'");
            if(count($checkifexist)==0){
                $catnews = new BlogCateg();
                $catnews->assign(array(
                    'blogid' => $blogid,
                    'categ' => $cat
                    ));
                if(!$catnews->save()){
                    $errors = array();
                    foreach ($page->getMessages() as $message){
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{$data['success'] = "Success cat";}
            }
        } 
        $delall = Blogtags::find("blogid='".$blogid."'");
        if($delall->delete()){
            foreach($tags as $tag){
                $gettags = Tags::findFirst("tags='".mysql_real_escape_string($tag)."'");
                if(!$gettags){
                     $_newtags = addslashes($tag);
                    $newstags = new Tags();
                    $newstags->assign(array(
                        'tags' => $_newtags,
                        'slugs' => str_replace("-", " ", $tag),
                        'created_at'    =>date("Y-m-d H:i:s"),
                        'updated_at'    =>date("Y-m-d H:i:s")
                        ));
                    if (!$newstags->save())
                    {
                        $data['error'] = "Something went wrong saving the newstags, please try again.";
                    }
                    else{
                        $chkif = Blogtags::findFirst("blogid='".$blogid."' and tags='".$tag."'");
                        if(!$chkif){
                            $tagnews = new Blogtags();
                            $tagnews->assign(array(
                                'blogid' => $blogid,
                                'tags' => $tag
                                ));
                            if (!$tagnews->save()){
                                $errors = array();
                                foreach ($tagnews->getMessages() as $message){
                                    $errors[] = $message->getMessage();
                                }
                                echo json_encode(array('error' => $errors));
                            }else{
                                $data['success'] = "Success cat";
                            }
                        }
                    }
                }else{
                    $chkif = Blogtags::findFirst("blogid='".$blogid."' and tags='".$tag."'");
                    if(!$chkif){
                        $tagnews = new Blogtags();
                        $tagnews->assign(array(
                            'blogid' => $blogid,
                            'tags' => $tag
                            ));
                        if (!$tagnews->save()){
                            $errors = array();
                            foreach ($tagnews->getMessages() as $message){
                                $errors[] = $message->getMessage();
                            }
                            echo json_encode(array('error' => $errors));
                        }else{
                            $data['success'] = "Success cat";
                        }
                    }
                }
            }
        }
    }
}
//echo json_encode($data);
}
public function DeletBlogAction($idno) {

    $dlt = Blog::findFirst('blogid="'. $idno.'"');
    if ($dlt) {
        if ($dlt->delete()) {
            $deletecat = BlogCateg::find(array("blogid='".$idno."'"));
            foreach ($deletecat as $_catdlt) {
                $dltcat = BlogCateg::findFirst(array("num='".$_catdlt->num."'"));
                if ($dltcat) {
                    if ($dltcat->delete()) {
                    }else{
                        //$data[]=array('error' => '');
                    }
                }else{
                    ///$data[]=array('error' => '');
                }
            }

            $deletetag = Blogtags::find(array("blogid='".$idno."'"));
            foreach ($deletecat as $_cattag) {
                $dlttag = Blogtags::findFirst(array("num='".$_cattag->num."'"));
                if ($dlttag) {
                    if ($dlttag->delete()) {
                        echo json_encode("Deleted");
                    }else{
                        //$data[]=array('error' => '');
                    }
                }else{
                    //$data[]=array('error' => '');
                }
            }

            $data[]=array('success' => "Blog has been successfully deleted");

        }else{
           $data[]=array('error' => "Seems the News Cant Be Deleted");
       }
   }else{
    $data[]=array('error' => "Seems the News Cant Be Deleted");
}





echo json_encode($data);


}
public function EditBlogAction($id){

 $data = array();
 $news = Blog::findFirst(array('blogid="'. $id.'"'));
 if ($news) {
    $db1 = \Phalcon\DI::getDefault()->get('db');
    $stmt1 = $db1->prepare("SELECT * FROM blogslcttags WHERE blogslcttags.blogid = '" . $id . "'");
    $stmt1->execute();
    $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
    $db2 = \Phalcon\DI::getDefault()->get('db');
    $stmt2 = $db2->prepare("SELECT * FROM blogslctcateg INNER JOIN blogcateg ON blogcateg.categoryid = blogslctcateg.categ WHERE blogslctcateg.blogid  = '" . $id . "'");
    $stmt2->execute();
    $searchresult2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
    if($news->featuredtype!="video"){
        $img = $news->featured;
        $vid = "";
    }else{
        $img = "";
        $vid = $news->featured;
    }
    $data = array(
        'newsid' => $news->blogid,
        'title' => stripslashes($news->title),
        'slugs' => $news->slug,
        'author' => $news->authorid,
        'summary' => $news->description,
        'body' => $news->body,
        'imagethumb' => $img,
        'videothumb' => $vid,
        'newslocation' => "$news->newslocation",
        'category' => $searchresult2,
        'status' => $news->status,
        'featurednews' => $news->featurednews,
        'date' => $news->publish,
        'datecreated' => $news->created_at,
        'tag' => $searchresult1,
        'metatitle' => $news->metatitle,
        'metakeyword' => $news->metakeyword,
        'metadesc' => $news->metadesc
        );
}
echo json_encode($data);
}

public function CategoryListAction(){    
    $newscategory = Centernewscategory::find(array());
    $newscategory = json_encode($newscategory->toArray(), JSON_NUMERIC_CHECK);
    echo $newscategory;
}
public function TagsListAction(){    
    $newscategory = Centernewstags::find(array());
    $newscategory = json_encode($newscategory->toArray(), JSON_NUMERIC_CHECK);
    echo $newscategory;
}
public function ArchiveListAction() {
    $news = Blog::find(array('status' => '1', 'order' => 'publish DESC'));
    $dates = [];
    foreach ($news as $news)
    {
        if(!in_array(date('F Y', strtotime($news->publish)), $dates)){
            $data[] = array(
                'month' => date('F', strtotime($news->publish)),
                'year' => date('Y', strtotime($news->publish)),
                'datepublished' => $news->publish
                );
            array_push($dates, date('F Y', strtotime($news->publish)));
        }
    }
    echo json_encode($data);
}
public function fecategoryAction($cat, $offset, $page){
    $data = array();
    $newscategory = Centernewscategory::findFirst('categoryslugs="'.$cat.'"');


    $blogcat = BlogCateg::find(array('categ="'.$newscategory->categoryid.'"', 'limit'=> "$offset, $page"));

    if(count($blogcat)==0){
        $data = array("error"=>"Something Went Wrong");
    }else{
        foreach ($blogcat as $value) {
           $getdata = Blog::find(array('blogid="'. $value->blogid.'"'));
           $getauthorname = Author::findFirst(array('id="'.$getdata[0]->authorid.'"'));
           $data[] = array(
             'blogid' => $getdata[0]->blogid,
             'title'  => stripslashes($getdata[0]->title),
             'slug'  => $getdata[0]->slug,
             'publish'=> $getdata[0]->publish,
             'name'   => $getauthorname->name,
             'description' => $getdata[0]->description,
             'featuredtype' => $getdata[0]->featuredtype,
             'featured' => $getdata[0]->featured
             );
       }
   }



   echo json_encode($data);
}

public function felistarchieveAction($date){
    $monthdate = date('Y', strtotime($date)) . "-" . date('m', strtotime($date));
    $blog = Blog::find(array("status='1' AND publish LIKE '%" . $monthdate . "%'","ORDER"=>"publish DESC"));
    foreach ($blog as $value) {
     $getauthorname = Author::findFirst('id="'.$value->authorid.'"');
     $data[] = array(
       'blogid' => $value->blogid,
       'title'  =>stripslashes( $value->title),
       'slug'  => $value->slug,
       'publish'=> $value->publish,
       'name'   => $getauthorname->name,
       'description' => $value->description,
       'featuredtype' => $value->featuredtype,
       'featured' => $value->featured
       );
 }

 echo json_encode($data);
}
public  function listBlogbyTagAction($tagsslugs, $offset, $page ){
    $newstag = Centernewstags::findFirst('slugs="'.$tagsslugs.'"');

    $blogtag = Blogtags::find(array('tags="'.$newstag->tags.'"', 'limit'=> "$offset, $page" ));
    if(count($blogtag)!=0){
        foreach ($blogtag as $value) {
            $getdata = Blog::findFirst(array('blogid="'. $value->blogid.'"'));
            if($getdata){
                $getauthorname = Author::findFirst('id="'.$getdata->authorid.'"');
                $data[] = array(
                 'blogid' => "$getdata->blogid",
                 'title'  => stripslashes($getdata->title) ,
                 'slug'  => $getdata->slug,
                 'publish'=> $getdata->publish,
                 'name'   => $getauthorname->name,
                 'description' => $getdata->description,
                 'featuredtype' => $getdata->featuredtype,
                 'featured' => $getdata->featured);
            }else{
                $data = array("error"=>"Something Went Wrong");
            }
        }
    }else{
        $data = array("error"=>"Something Went Wrong");
    }
    echo json_encode($data);
}


public function validatecategAction($categ){
    $validate = Centernewscategory::find(array("categoryname='".mysql_real_escape_string($categ)."'"));

    echo json_encode(count($validate));
    echo json_encode($categ);
}
public function createcategoryAction(){

   $data = array(); 

   $catnames = new Centernewscategory();
   $catnames->assign(array(
    'categoryname' => $_POST['catnames'],
    'categoryslugs' => $_POST['slugs'],
    'created_at'=>date("Y-m-d H:i:s"),
    'updated_at'=>date("Y-m-d H:i:s"),
    ));
   if (!$catnames->save()){
    $errors = array();
    foreach ($catnames->getMessages() as $message) {
        $errors[] = $message->getMessage();
    }
    echo json_encode(array('error' => $errors));
} 
}
public function listcategoryAction()
{

    $getcategory= Centernewscategory::find(array("order" => "categoryid ASC"));
    foreach ($getcategory as $getcategory) {
        $data[] = array(
            'categoryid'=>$getcategory->categoryid,
            'categoryname'=> stripslashes($getcategory->categoryname)
            );
    }
    echo json_encode($data);

} 

}
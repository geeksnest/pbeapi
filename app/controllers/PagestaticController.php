<?php

namespace Controllers;
use \Controllers\ControllerBase as CB;

use \Models\Pagehomeimg as Pagehomeimg;
use \Models\Pagebanner as Pagebanner;
use \Models\Pagestatic as Pagestatic;

class PagestaticController extends \Phalcon\Mvc\Controller{
	/** 
    * Ohhhaaaa
    */  
    public function pageStaticGetAction($_page){
        $data = array();
        $pageStatic = Pagestatic::find("page='".$_page."'");
        count($pageStatic);
        foreach ($pageStatic as $m) {

        	$data[]  = array(
                "id"    =>$m->id,
                "page"  =>$m->page,
                "img"   =>$m->img,
                "body"  =>($m->page=="banner" || $m->page=="home" || $m->page=="social" ?  json_decode($m->body) : $m->body)
            );
        }
        echo json_encode($data);
    }

    public function bannerSaveAction() {
        if($_POST){
            $page = Pagestatic::findFirst("id=".$_POST['id']);
            $page->assign(array(
                'body' => $_POST['body']
                ));
            (!$page->save() ? $data['error'] = "Something went wrong saving the data, please try again." : $data['success'] = "Success" );
        }
        echo json_encode(($data));
    }

    public function otherSaveAction() {
        if($_POST){
            $page = Pagestatic::findFirst("id=".$_POST['id']);
            $page->assign(array(
                'body' => $_POST['body'],
                'img' => $_POST['img'],
                ));
            (!$page->save() ? $data['error'] = "Something went wrong saving the data, please try again." : $data['success'] = "Success" );
        }
        echo json_encode(($data));
    }

    public function listimageAction() {
        $getimages = Pagebanner::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) 
            {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->filename
                    );
            }
        }
        echo json_encode($data);
    }
    public function saveimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Pagebanner();
        $picture->assign(array(
            'filename' => $filename
            ));
        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
            $audit = new CB();
            $audit->auditlog(array("module" =>"Page Banner","event" => "Upload","title" => "Upload Image : ". $filename .""));
        }
    }
    public function deleteimageAction($imgid) {
        $img = Pagebanner::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                $audit = new CB();
                $audit->auditlog(array("module" =>"Testimonials","event" => "Delete", "title" => "Delete Image : ". $filename .""));
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }


    // HOME PAGE
    public function listimgAction() {
        $getimages = Pagehomeimg::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) 
            {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->filename
                    );
            }
        }
        echo json_encode($data);
    }
    public function saveimgAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Pagehomeimg();
        $picture->assign(array(
            'filename' => $filename
            ));
        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
            $audit = new CB();
            $audit->auditlog(array("module" =>"Page Banner","event" => "Upload","title" => "Upload Image : ". $filename .""));
        }
    }
    public function dltimgAction($imgid) {
        $img = Pagehomeimg::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                $audit = new CB();
                $audit->auditlog(array("module" =>"Testimonials","event" => "Delete", "title" => "Delete Image : ". $filename .""));
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }

}
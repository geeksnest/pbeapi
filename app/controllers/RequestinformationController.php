<?php

namespace Controllers;
use \Models\Requestinformation as Requestinformation;
use \Models\Notifications as Notifications;
use \Models\Repliedmessage as Message;
use \Controllers\ControllerBase as CB;

class RequestinformationController extends \Phalcon\Mvc\Controller {

    public function listrequestAction($num, $page, $keyword ,$center) {

        if ($keyword == 'undefined' &&  $center == 'undefined') {
            $request = Requestinformation::find(array("order" => "status"));
        }
        elseif($center != 'undefined' && $keyword == 'undefined'){
            $request = Requestinformation::find(array("center = '" . $center . "'"));
        } 
        elseif($center == 'undefined' && $keyword != 'undefined') {
            $conditions = "email LIKE '%" . $keyword . "%' OR 
                          center LIKE '%" . $keyword . "%'  OR 
                          category LIKE '%" . $keyword . "%'  OR 
                          fname LIKE '%" . $keyword . "%'  OR 
                          lname LIKE '%" . $keyword . "%'  OR 
                          datesubmitted LIKE '%" . $keyword . "%'";
            $request = Requestinformation::find(array($conditions));
        }
        else {
            $conditions = "email LIKE '%" . $keyword . "%' AND center = '". $center ."' OR 
                          center LIKE '%" . $keyword . "%' AND center = '". $center ."' OR 
                          category LIKE '%" . $keyword . "%' AND center = '". $center ."' OR 
                          fname LIKE '%" . $keyword . "%' AND center = '". $center ."' OR 
                          lname LIKE '%" . $keyword . "%' AND center = '". $center ."' OR 
                          datesubmitted LIKE '%" . $keyword . "%' AND center = '". $center ."'";
            $request = Requestinformation::find(array($conditions));
        }

        $currentPage = (int) ($page);

            // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $request,
                "limit" => 10,
                "page" => $currentPage
                )
            );

            // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'fname' => $m->fname,
                'lname' => $m->lname,
                'email' => $m->email,
                'center' => $m->center,
                'category' => $m->category,
                'content' => $m->content,
                'status' => $m->status,
                'datesubmitted' => $m->datesubmitted
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function requestdeleteAction($id) {
        $data = $id;
        $request = Requestinformation::findFirst('id="'. $id.'"');
        $Notifications = Notifications::findFirst('id="'. $id.'"');
        $fname = $request->fname;
        $lname = $request->lname;
        // $data = array('error' => 'Not Found');
        if ($request) {
            if ($request->delete()) {
                $Notifications->delete();
                $data = array('success' => 'REQUEST Deleted');
                $deletemessage = Message::find('rid="'.$id.'"');
                $deletemessage->delete();
                //START Log
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Request Information", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "".$fname." ".$lname." Request Deleted", /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            }
        }
        echo json_encode($data);
    }

    public function listreplyAction($id){
        $data = array();
        $getreply= Message::find('rid="' . $id . '" ');
        foreach ($getreply as $getreply) {
            $data[] = array(
                'id'=>$getreply->id,
                'rid'=>$getreply->rid,
                'message'=>$getreply->message,
                'date'=>$getreply->date
                );
        }
        echo json_encode($data);

    }
    public function viewreplyAction($id) {

        $data = array();
        $viewrequest = Requestinformation::findFirst('id="' . $id .'"');
        $Notifications = Notifications::findFirst('id="'. $id.'"');
        if ($viewrequest) {
            $viewrequest = array(                
                'id' => $viewrequest->id,
                'fname' =>$viewrequest->fname,
                'lname' =>$viewrequest->lname,
                'email' =>$viewrequest->email,
                'center' =>$viewrequest->center,
                'category' =>$viewrequest->category,
                'content' =>$viewrequest->content,
                'status' => $viewrequest->status,
                'datesubmitted' =>$viewrequest->datesubmitted
                );
        }
        echo json_encode($viewrequest);

        $read = Requestinformation::findFirst('id="' . $id .'"');
        $fname = $read->fname;
        $lname = $read->lname;
        if($read->status == 0){
            $read->status = 1;
            $Notifications->status = 1;
            $Notifications->save();

            //START Log
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Request Information", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Review", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "".$fname." ".$lname." Request Reviewed", /*// Maybe some info here (confuse) XD*/
                    ));
            //END Audit Log


            if (!$read->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
        }
    }

            //reply PROPOSAL
    public function replyrequestAction() {
        $data = array();

        $id = $_POST['rid'];
        $request = Requestinformation::findFirst('id="' . $id .'"');
        $Notifications = Notifications::findFirst('id="'. $id.'"');
        $fname = $request->fname;
        $lname = $request->lname;
        $request->status = 2;
        $Notifications->status = 2;
        $Notifications->save();
        if (!$request->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } 
        else {

                ///MAIL 
                $dc = new CB();
                $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">This is the reply to your message: "'.$_POST['content'].'"</div>' . '<br><br> Admin Reply: ' .$_POST['messages'];
                $send = $dc->sendMail($_POST['email'],'Power Brain Education : Reply',$body);

                $repliedmessage = new Message();
                $repliedmessage->assign(array(
                        'rid'  => $id,
                        'message' => $_POST['messages'],
                        'date' => date('Y-m-d')
                        ));
                if (!$repliedmessage->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                }else{
                        $data['success'] = "Success";
                        //START Log
                        $audit = new CB();
                        $audit->auditlog(array(
                            "module" =>"Request Information", /*//Examaple News, Create Center, Slider, Events etc...*/
                            "event" => "Reply", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                            "title" => "".$fname." ".$lname." Request Replied", /*// Maybe some info here (confuse) XD*/
                            ));
                        //END Audit Log
                }
        }
        echo json_encode($data);
    }
}
<?php

namespace Controllers;

use \Models\Testimonials as savetest;
use \Models\Testimonialsimage as imagetest;
use \Controllers\ControllerBase as CB;

class TestimonialsController extends \Phalcon\Mvc\Controller {

    public function savetestimonialAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();

        $id = $guid->GUID();
        $title= addslashes($request->getPost('title'));
        $status= $request->getPost('status');
        $sender= addslashes($request->getPost('sender'));
        $age= $request->getPost('age');
        $content= $request->getPost('content');
        $category= $request->getPost('category');
        $datepublished= $request->getPost('datepublished');
        $image= $request->getPost('image');

        $timeconcat = date("H:i:s");


        $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
        $dates = explode(" ", $datepublished);
        $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2].' '.$timeconcat;




        $add = new savetest();
        $add->assign(array(
            'id' => $id,
            'title' => $title,
            'status' => $status,
            'sender' => $sender,
            'age' => $age,
            'content' => $content,
            'testimonialcategory' => $category,
            'image' => $image,
            'datepublished' => $d
            ));
                    // $add->save();
        if (!$add->save()) {
            $errors = array();
            foreach ($add->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
            $data['error'] ="!SAVE";
        } 

        else{
            $data['success'] ="SAVE";
                            //START Log
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Testimonials", /*//Examaple News, Create Center, Slider, Events etc...*/
                "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                "title" => "Add Testimonial : ". $title ."" /*// Maybe some info here (confuse) XD*/
                ));
                            //END Audit Log
        }
        echo json_encode(array($data));
    }
    public function listtestimonialsAction($num, $page, $keyword){
        if ($keyword == 'undefined') {
            $listtestimonials = savetest::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%' 
            or sender LIKE '%" . $keyword . "%'
            or testimonialcategory LIKE '%" . $keyword . "%'";
            $listtestimonials= savetest::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $listtestimonials,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => stripslashes($m->title),
                'sender' => stripslashes($m->sender),
                'status' => $m->status,
                'testimonialcategory' => $m->testimonialcategory
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }
    public function testimonialdeleteAction($id){
        $data = array();
        $dlt = savetest::findFirst('id="' . $id . '"');
        $title = $dlt->title;
        if ($dlt) {
            if($dlt->delete()){
                $data['success'] = "deleted";
                 //START Log
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Testimonials", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "Delete Testimonial : ". $title ."" /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            }else {
                $data['error'] = "notdeleted";
            }
            echo json_encode($data);
        }
    }
    //IMAGE UPLOAD
    public function saveimageAction() {

        $filename = $_POST['imgfilename'];

        $picture = new imagetest();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
             //START Log
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Testimonials", /*//Examaple News, Create Center, Slider, Events etc...*/
                "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                "title" => "Add Image : ". $filename ."" /*// Maybe some info here (confuse) XD*/
                ));
                //END Audit Log
        }

    }
    //IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = imagetest::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                //START Log
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Testimonials", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "Delete Image : ". $filename ."" /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }


    public function listimageAction() {

        $getimages = imagetest::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) 
            {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->filename
                    );
            }
        }
        echo json_encode($data);

    }

    ///EDIT 
    public function testimonialeditAction($id) {
        $data = array();
        $leftbaritem = array();
        $rightbaritem = array();

        $testimonial = savetest::findFirst('id="' . $id . '"');
        if ($testimonial) {
            $data = array(
                'id' => $testimonial->id,
                'title' => stripslashes($testimonial->title),
                'status' => $testimonial->status,
                'sender' => stripslashes($testimonial->sender),
                'age' => $testimonial->age,
                'content' => $testimonial->content,
                'image' => $testimonial->image,
                'datepublished' => substr($testimonial->datepublished, 0,10),
                'testimonialcategory' => $testimonial->testimonialcategory
                );
        }
        echo json_encode($data);
    }
    //Update
    public function updatetestimonialAction(){
        $request = new \Phalcon\Http\Request();

        $id = $request->getPost('id');
        $title= addslashes($request->getPost('title'));
        $status= $request->getPost('status');
        $sender= addslashes($request->getPost('sender'));
        $age= $request->getPost('age');
        $content= $request->getPost('content');
        $category= $request->getPost('testimonialcategory');
        $image= $request->getPost('image');
        $datepublished = $request->getPost('hiddendate');


        $timeconcat = date("H:i:s");
                    //date converter
        $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');

        if(strlen($datepublished)<=11){
            $d = $datepublished .' '. $timeconcat;
        }else{
            $dates = explode(" ", $datepublished);
            $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2].' '.$timeconcat;
        }



        $add = savetest::findFirst('id="' . $id . '"');
        $add->assign(array(
            'title' => $title,
            'status' => $status,
            'sender' => $sender,
            'age' => $age,
            'content' => $content,
            'testimonialcategory' => $category,
            'image' => $image,
            'datepublished' => $d
            )); 

                // $add->save();
        if (!$add->save()) {
            $errors = array();
            foreach ($add->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
            $data['error'] ="!SAVE";
        } 

        else{
            $data['success'] ="SAVE";
                            //START Log
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Testimonials", /*//Examaple News, Create Center, Slider, Events etc...*/
                "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                "title" => "Update Testimonial : ". $title ."" /*// Maybe some info here (confuse) XD*/
                ));
                            //END Audit Log
        }


        echo json_encode(array($data));    
    }
    public function changestatusAction($id,$status){
        $getInfo = savetest::findFirst('id="'. $id .'"');
        $title = $getInfo->title;
        if($status == 1){
           $getInfo->status = 0;
           $getInfo->save();
           $data=array('success' => 'Unpublished');
             //START Log
           $audit = new CB();
           $audit->auditlog(array(
            "module" =>"Testimonials", /*//Examaple News, Create Center, Slider, Events etc...*/
            "event" => "Unpublished", /*//Example ADD , EdIT , Delete ,View Details etc...*/
            "title" => "Unpublished Testimonial : ". $title ."" /*// Maybe some info here (confuse) XD*/
            ));
              //END Audit Log
       }
       else{
           $getInfo->status = 1;
           $getInfo->save();
           $data=array('success' => 'Published');
             //START Log
           $audit = new CB();
           $audit->auditlog(array(
            "module" =>"Testimonials", /*//Examaple News, Create Center, Slider, Events etc...*/
            "event" => "Published", /*//Example ADD , EdIT , Delete ,View Details etc...*/
            "title" => "Published Testimonial : ". $title ."" /*// Maybe some info here (confuse) XD*/
            ));
              //END Audit Log
       }
       echo json_encode($data);
   }

   public function pbstestimonialsAction(){
    $listtestimonials= savetest::find(array("testimonialcategory = 'powerbrain'  and status = 1 order by datepublished desc"));

    foreach($listtestimonials as $listtestimonials) 
    {
        $data[] = array(
            'id' => $listtestimonials->id,
            'title' => stripslashes($listtestimonials->title),
            'status' => $listtestimonials->status,
            'sender' => stripslashes($listtestimonials->sender),
            'age' => $listtestimonials->age,
            'content' => $listtestimonials->content,
            'image' => $listtestimonials->image,
            'datepublished' => $listtestimonials->datepublished,
            'testimonialcategory' => $listtestimonials->testimonialcategory
            );
    }
    echo json_encode($data);
}
public function foradultsfeaturedtestimonialsAction(){
    $testimonial = savetest::findFirst(array("testimonialcategory = 'adults' and status = 1 order by datepublished desc"));
    if ($testimonial) {
        $data = array(
            'content' => $testimonial->content,
            'image' => $testimonial->image,
            );
    }
    echo json_encode($data);
}
public function forfamiliesfeaturedtestimonialsAction(){
    $testimonial = savetest::findFirst(array("testimonialcategory = 'families' and status = 1 order by datepublished desc"));
    if ($testimonial) {
        $data = array(
            'content' => $testimonial->content,
            'image' => $testimonial->image,
            );
    }
    echo json_encode($data);
}
public function featuredtestimonialsAction(){
    $testimonial = savetest::findFirst(array("testimonialcategory = 'featured' and status = 1 order by datepublished desc"));
    if ($testimonial) {
        $data = array(
            'content' => $testimonial->content,
            'image' => $testimonial->image,
            'sender' => $testimonial->sender,
            );
    }
    echo json_encode($data);


}
public function foradultstestimonialsAction(){
    $listtestimonials= savetest::find(array("testimonialcategory = 'adults' and status = 1 order by datepublished desc"));

    foreach($listtestimonials as $listtestimonials) 
    {
        $data[] = array(
            'id' => $listtestimonials->id,
            'title' => stripslashes($listtestimonials->title),
            'status' => $listtestimonials->status,
            'sender' => stripslashes($listtestimonials->sender),
            'age' => $listtestimonials->age,
            'content' => $listtestimonials->content,
            'image' => $listtestimonials->image,
            'datepublished' => $listtestimonials->datepublished,
            'testimonialcategory' => $listtestimonials->testimonialcategory
            );
    }
    echo json_encode($data);
}
public function forfamiliestestimonialsAction(){
    $listtestimonials= savetest::find(array("testimonialcategory = 'families' and status = 1 order by datepublished desc"));

    foreach($listtestimonials as $listtestimonials) 
    {
        $data[] = array(
            'id' => $listtestimonials->id,
            'title' => stripslashes($listtestimonials->title),
            'status' => $listtestimonials->status,
            'sender' => stripslashes($listtestimonials->sender),
            'age' => $listtestimonials->age,
            'content' => $listtestimonials->content,
            'image' => $listtestimonials->image,
            'datepublished' => $listtestimonials->datepublished,
            'testimonialcategory' => $listtestimonials->testimonialcategory
            );
    }
    echo json_encode($data);
}
}
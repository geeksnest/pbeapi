<?php

namespace Controllers;

use \Models\Users as Users;
use \Models\States as States;
use \Models\Centerimage as Banner;
use \Models\Centerslider as Centerslider;
use \Models\Cities_extended as Cityinfo;
use \Controllers\ControllerBase as CB;
use \Models\Pageimage as Pageimage;
use \Models\Center as Center;
use \Models\Centerfeaturedbanner as Featbanner;
use \Models\Centermap as Centermap;

class CenterController extends \Phalcon\Mvc\Controller {


public function validateTitleAction($centername){
    $validate = Center::find(array("title='".$centername."'"));
   
    echo json_encode(count($validate));
}



 public function userListAction(){
    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT id,first_name as fname,last_name as lname FROM users where task='CS' and id NOT IN(Select manager from center)");
    $stmt->execute();
    $user = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    echo json_encode($user);
}
public function stateListAction(){
    $dbstate = States::find();
    foreach ($dbstate as $m) {
        $data[] = array(
            'state' => $m->state,
            'statecode' => $m->state_code,  
            );
    }
    echo json_encode($data);
}
public function centerInfoAction($state){
    $dbCity = Cityinfo::find("state_code='".$state."'   " );
    foreach ($dbCity as $m) {
        $data[] = array(
            'city' => $m->city,
            'states' => $m->state_code,
            'zip' => $m->zip,
            'county' => $m->county,
            );
    }
    echo json_encode($data);
}

public function uploadcenterBannerAction(){
    $filename = $_POST['imgfilename'];

    $picture = new Banner();
    $picture->assign(array(
        'filename' => "$filename"
        ));

    if (!$picture->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'Images has been uploaded');
      //START Log
      $audit = new CB();
      $audit->auditlog(array(
          "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Add image - ".$filename." ", /*// Maybe some info here (confuse) XD*/
          ));
      //END Audit Log
  }
  echo json_encode($data);
}
public function deleteimageAction($imgid) {
    $img = Banner::findFirst('id="'. $imgid.'"');
    $filename = $img->filename;
    if ($img) {
        if ($img->delete()) {
            $data[]=array('success' => "");
                  //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete image - ".$filename." ", /*// Maybe some info here (confuse) XD*/
              ));
                  //END Audit Log
        }else{
            $data[]=array('error' => '');
        }
    }
    echo json_encode($data);
}
public function centerbannerAction() {
    $getimages = Banner::find(array("order" => "id DESC"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
    }
    echo json_encode($data);
}








public function featuredBannerAction(){
    $filename = $_POST['imgfilename'];
    $picture = new Featbanner();
    $picture->assign(array(
        'filename' => "$filename"
        ));

    if (!$picture->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'Images has been uploaded');
  }
  echo json_encode($data);
}
public function listfeaturedbannerAction() {
    $getimages = Featbanner::find(array("order" => "id DESC"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
    }
    echo json_encode($data);
}
public function deletefeaturedimageAction($imgid) {
    $img = Featbanner::findFirst('id="'. $imgid.'"');
    if ($img) {
        if ($img->delete()) {
            $data[]=array('success' => "");
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data);
}



public function savecenterAction() {
 $request = new \Phalcon\Http\Request();

 if($request->isPost()){

    $title      = $request->getPost('title');
    $slugs      = $request->getPost('slugs');
    $manager    = $request->getPost('manager');
    $email      = $request->getPost('email');
    $region     = $request->getPost('region');
    $district   = $request->getPost('district');
    $state      = $request->getPost('state');
    $city       = $request->getPost('city');
    $zipcode    = $request->getPost('zipcode');
    $address    = $request->getPost('address');
    $metatitle  = $request->getPost('metatitle');
    $desc       = $request->getPost('desc');
    $banner     = $request->getPost('banner');
    $mffrom     = $request->getPost('mffrom');
    $mfto       = $request->getPost('mfto');
    $satfrom    = $request->getPost('satfrom');
    $satto      = $request->getPost('satto');
    $contactnumber     = $request->getPost('contactnumber');
    $banner     = $request->getPost('banner');
    $status     = $request->getPost('status');
    $userid     = $request->getPost('userid');


    $guid = new \Utilities\Guid\Guid();
    $page = new Center();
    $centerid = $guid->GUID();
    $page->assign(array(
        'centerid'      =>$centerid,
        'title'         => $title,
        'email'         => $email,
        'slugs'         => $slugs,
        'manager'       => $manager,
        'region'        => $region,
        'district'      => $district ,
        'state'         =>$state,
        'city'          =>$city,
        'zipcode'       => $zipcode,
        'address'       => $address,
        'metatitle'     => $metatitle,
        'desc'          => $desc,
        'banner'        => $banner,
        'mffrom'        => $mffrom,
        'mfto'          => $mfto,
        'satfrom'       => $satfrom,
        'satto'         => $satto,
        'contactnumber' => $contactnumber,
        'status'        => $status,
        'created_at'    =>date("Y-m-d H:i:s"),
        'updated_at'    =>date("Y-m-d H:i:s"),
        ));

    if (!$page->save()) {
        $errors = array();
        foreach ($page->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {

     $data[]=array('success' => "New Center has been successfully Created!");
       //START Log
     $audit = new CB();
     $audit->auditlog(array(
        "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
        "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
        "title" => "Add Center- ".$title." ", /*// Maybe some info here (confuse) XD*/
        ));
        //END Audit Log

     ////Centermap
     $mapid = new \Utilities\Guid\Guid();
     $acti = new Centermap();
     $acti->assign(array(
        'mapid'   =>$mapid->GUID(),
        'centerid' => $centerid,
        'lat' => $request->getPost('lat'),
        'lon' => $request->getPost('lon'),
        'created_at'  =>date("Y-m-d H:i:s"),
        'updated_at'  =>date("Y-m-d H:i:s")
        ));
     if (!$acti->save()) {
         $errors = array();
         foreach ($acti->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {
        $data[] = array('success' => "Map Save");
    }
    ////Centermap
}

}
echo json_encode($data);

}

public function manageCenterAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Center::find();
    } else {
        $conditions = "title LIKE '%" . $keyword . "%' OR slugs LIKE '%" . $keyword . "%'";
        $Pages = Center::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
     $city = Cityinfo::findFirst('zip="'. $m->zipcode.'"');

     $data[] = array(
        'centerid' => $m->centerid,
        'title' => $m->title,
        'slugs' => $m->slugs,
        'status' => $m->status,
        'metatitle' => $m->metatitle,
        'banner' => $m->banner,
        'address' => $m->address,
        'state' => $m->state,
        'zipcode' => $m->zipcode,
        'contactnumber' => $m->contactnumber,
        'city'=> $city->city,
        );
 }
 $p = array();
 for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}

public function centerDeleteAction($centerid) {
    $data = $pageid;
    $pages = Center::findFirst('centerid="'. $centerid.'"');
    $title = $pages->title;
    if ($pages) {
        if ($pages->delete()) {
            $data[]=array('success' => "Center has been successfully Deleted!");
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Center- ".$title." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
    }
    echo json_encode($data);
}
public function centerdataAction($centerid) {

    $center = Center::findFirst('centerid="'. $centerid.'"');
    $userid = $center->manager;
    $data=array(
        'title'         => $center->title,
        'email'         => $center->email,
        'slugs'         => $center->slugs,
        'manager'       => $center->manager,
        'region'        => $center->region,
        'district'      => $center->district ,
        'state'         => $center->state,
        'city'          => $center->city,
        'zipcode'       => $center->zipcode,
        'address'       => $center->address,
        'metatitle'     => $center->metatitle,
        'desc'          => $center->desc,
        'banner'        => $center->banner,
        'mffrom'        => $center->mffrom,
        'mfto'          => $center->mfto,
        'satfrom'       => $center->satfrom,
        'satto'         => $center->satto,
        'contactnumber' => $center->contactnumber,
        'status'        => $center->status,
        );

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT id,first_name as fname,last_name as lname FROM users where task='CS' and id NOT IN(Select manager from center where manager <> '".$userid."')");
    $stmt->execute();
    $user = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data'=>$data,'users'=>$user));

}

public function updatecenterAction() {
 $request = new \Phalcon\Http\Request();

 if($request->isPost()){

    $centerid   = $request->getPost('centerid');
    $title      = $request->getPost('title');
    $slugs      = $request->getPost('slugs');
    $manager    = $request->getPost('manager');
    $email      = $request->getPost('email');
    $region     = $request->getPost('region');
    $district   = $request->getPost('district');
    $state      = $request->getPost('state');
    $city       = $request->getPost('city');
    $zipcode    = $request->getPost('zipcode');
    $address    = $request->getPost('address');
    $metatitle  = $request->getPost('metatitle');
    $desc       = $request->getPost('desc');
    $banner     = $request->getPost('banner');
    $mffrom     = $request->getPost('mffrom');
    $mfto       = $request->getPost('mfto');
    $satfrom    = $request->getPost('satfrom');
    $satto      = $request->getPost('satto');
    $contactnumber     = $request->getPost('contactnumber');
    $status     = $request->getPost('status');


    $page = Center::findFirst('centerid="'. $centerid.'"');
    $page->assign(array(
        'title'         => $title,
        'slugs'         => $slugs,
        'manager'       => $manager,
        'email'         => $email,
        'region'        => $region,
        'district'      => $district ,
        'state'         =>$state,
        'city'          =>$city,
        'zipcode'       => $zipcode,
        'address'       => $address,
        'metatitle'     => $metatitle,
        'desc'          => $desc,
        'banner'        => $banner,
        'mffrom'        => $mffrom,
        'mfto'          => $mfto,
        'satfrom'       => $satfrom,
        'satto'         => $satto,
        'contactnumber' => $contactnumber,
        'status'        => $status,
        'updated_at'    =>date("Y-m-d H:i:s"),
        ));

    if (!$page->save()) {
        $errors = array();
        foreach ($page->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {
     $data[]=array('success' => "Center has been successfully Updated!");
        //START Log
     $audit = new CB();
     $audit->auditlog(array(
      "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
      "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
      "title" => "Update Center - ".$title." ", /*// Maybe some info here (confuse) XD*/
      ));
        //END Audit Log
 }
}
echo json_encode($data);
}
public function updatemycenterAction() {
 $request = new \Phalcon\Http\Request();

 if($request->isPost()){

    $centerid   = $request->getPost('centerid');
     $email      = $request->getPost('email');
    $title      = $request->getPost('title');
    $slugs      = $request->getPost('slugs');
    $region     = $request->getPost('region');
    $district   = $request->getPost('district');
    $state      = $request->getPost('state');
    $city       = $request->getPost('city');
    $zipcode    = $request->getPost('zipcode');
    $address    = $request->getPost('address');
    $metatitle  = $request->getPost('metatitle');
    $desc       = $request->getPost('desc');
    $banner     = $request->getPost('banner');
    $mffrom     = $request->getPost('mffrom');
    $mfto       = $request->getPost('mfto');
    $satfrom    = $request->getPost('satfrom');
    $satto      = $request->getPost('satto');
    $contactnumber     = $request->getPost('contactnumber');
    $status     = $request->getPost('status');


    $page = Center::findFirst('centerid="'. $centerid.'"');
    $page->assign(array(
        'title'         => $title,
        'slugs'         => $slugs,
         'email'         => $email,
        'region'        => $region,
        'district'      => $district ,
        'state'         =>$state,
        'city'          =>$city,
        'zipcode'       => $zipcode,
        'address'       => $address,
        'metatitle'     => $metatitle,
        'desc'          => $desc,
        'banner'        => $banner,
        'mffrom'        => $mffrom,
        'mfto'          => $mfto,
        'satfrom'       => $satfrom,
        'satto'         => $satto,
        'contactnumber' => $contactnumber,
        'status'        => $status,
        'updated_at'    =>date("Y-m-d H:i:s"),
        ));

    if (!$page->save()) {
        $errors = array();
        foreach ($page->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {
       $data[]=array('success' => "New Center has been successfully Created!");
   }


}
echo json_encode($data);

}
public function changestatusAction($id,$status){
    $getInfo = Center::findFirst('centerid="'. $id .'"');
    $title = $getInfo->title;
    if($status == 'true'){
     $getInfo->status = 'false';
     $getInfo->save();
     $data=array('success' => 'Deactivated');
            //START Log
     $audit = new CB();
     $audit->auditlog(array(
      "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
      "event" => "Deactivated", /*//Example ADD , EdIT , Delete ,View Details etc...*/
      "title" => "Center Name - ".$title." ", /*// Maybe some info here (confuse) XD*/
      ));
            //END Audit Log
 }
 else{
     $getInfo->status = 'true';
     $getInfo->save();
     $data=array('success' => 'Activated');
           //START Log
     $audit = new CB();
     $audit->auditlog(array(
      "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
      "event" => "Activated", /*//Example ADD , EdIT , Delete ,View Details etc...*/
      "title" => "Center Name - ".$title." ", /*// Maybe some info here (confuse) XD*/
      ));
            //END Audit Log
 }
 echo json_encode($data);
}



public function uploadcenterSliderAction($path, $centerid) {
    // $filename = $_POST['imgfilename'];
    // $centerid = $_POST['centerid'];

    $picture = new Centerslider();
    $picture->assign(array(
        'centerid' => $centerid,
        'imagepath' => $path,
        ));

    if (!$picture->save()) {
      $data[]=array('error' => 'Something went wrong saving the data, please try again.');
  } else {
      $data[]=array('success' => 'Images has been uploaded');
      //START Log
      $audit = new CB();
      $audit->auditlog(array(
          "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
          "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
          "title" => "Add Center Slider - ".$path." ", /*// Maybe some info here (confuse) XD*/
          ));         
      //END Audit Log
  }
  echo json_encode($data);
}

public function centersliderAction($centerid) {
    $getimages = Centerslider::find(array("centerid='".$centerid."'", "order" => "id DESC"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->imagepath
                );
        }
    }
    echo json_encode($data);
}
public function deletesliderAction($imgid) {
    $data = $pageid;
    $imgslider = Centerslider::findFirst('id="'. $imgid.'"');
    $filename = $imgslider->$filename;
    if ($imgslider) {
        if ($imgslider->delete()) {
            $data[]=array('success' => "");
             //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Center", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Center Slider IMAGE- ".$filename." ", /*// Maybe some info here (confuse) XD*/
              ));         
            //END Audit Log
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data);
}

public function feInfocenterAction($slug) {

    $center = Center::findFirst('slugs="'. $slug.'"');
    $data=array(
        'centerid'         => $center->centerid,
        'title'         => $center->title,
        'email'         => $center->email,
        'slugs'         => $center->slugs,
        'manager'       => $center->manager,
        'region'        => $center->region,
        'district'      => $center->district ,
        'state'         => $center->state,
        'city'          => $center->city,
        'zipcode'       => $center->zipcode,
        'address'       => $center->address,
        'metatitle'     => $center->metatitle,
        'desc'          => $center->desc,
        'banner'        => $center->banner,
        'mffrom'        => $center->mffrom,
        'mfto'        => $center->mfto,
        'satfrom'        => $center->satfrom,
        'satto'        => $center->satto,
        'contactnumber'        => $center->contactnumber,
        'status'        => $center->status,
        );
    echo json_encode($data);
}
public function centerManagerAction($userid) {

    $center = Users::findFirst('id="'. $userid.'" and task="CS"');
    $data=array(
        'username'         => $center->username,
        'email'         => $center->email,
        'fname'       => $center->first_name,
        'lname'        => $center->last_name,
        'profile'      => $center->profile_pic_name,
        );
    echo json_encode($data);
}
public function eventcenterCenterAction() {

    $center = Center::find();
    foreach ($center as $m) {
        $data[] = array(
            'centerid' => $m->centerid,
            'title' => $m->title,
            'slugs' => $m->slugs,
            );
    }
    echo json_encode($data);
}
public function centeroptionAction(){
    $center = Center::find("status='true'");
    foreach ($center as $m) {
        $data[] = array(
            'title' => $m->title,
            );
    }
    echo json_encode($data);  
}



public function felistcenterAction($num, $page) {
    $Pages = Center::find("status='true'");
    $currentPage = (int) ($page);
        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
     $city = Cityinfo::findFirst('zip="'. $m->zipcode.'"');


     $data[] = array(
        'centerid' => $m->centerid,
        'title' => $m->title,
        'slugs' => $m->slugs,
        'status' => $m->status,
        'banner' => $m->banner,
        'address' => $m->address,
        'state' => $m->state,
        'zipcode' => $m->zipcode,
        'contactnumber' => $m->contactnumber,
        'city'=> $city->city,
        );
 }
 $p = array();
 for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}




}
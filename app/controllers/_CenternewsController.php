<?php

namespace Controllers;
use \Models\Centernews as Centernews;
use \Models\Blog as Blog;
use \Models\Author as Author;
use \Models\Center as Center;
use \Models\Centernewstags as Tags;
use \Models\Centernewscategory as Category;

use \Models\Centernewsslctedcateg as Selctcateg;
use \Models\Centerselectednewstags as SelctTags;
use \Models\Centerfeauredvideo as Featuredvideo;
use \Controllers\ControllerBase as CB;

class CenternewsController extends \Phalcon\Mvc\Controller {


    public function Validatenewstitlesction($centername){
        $validate = Centernews::find(array("title='".$centername."'"));

        echo json_encode(count($validate));
    }

    public function centerNewsction(){
//         $request = new \Phalcon\Http\Request();
//         if($request->isPost()){
//             $centerid = $request->getPost('centerid');
//             $title = $request->getPost('title');
//             $slugs = $request->getPost('slugs');
//             $description = $request->getPost('description');
//             $body = $request->getPost('body');
//             /*$banner = $request->getPost('banner');*/
//             $publish = $request->getPost('date');
//             $author = $request->getPost('author');
//             $publish = $request->getPost('publish');
//             $author = $request->getPost('author');
//             $featuredoption = $request->getPost('featuredoption');
//             $category = $request->getPost('category');
//             $tags = $request->getPost('tag');

//             if($featuredoption=="video"){
//                 $featured =  $request->getPost('video');
//             }else{
//                 $featured =  $request->getPost('featbanner');
//             }
//             $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
//             $dates = explode(" ", $publish);
//             $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
//             $guid = new \Utilities\Guid\Guid();
//             $newsid = $guid->GUID();
//             $page = new Centernews();
//             $page->assign(array(
//                 'newsid'=>$newsid,
//                 'centerid' => $centerid,
//                 'authorid' => $author,
//                 'title' => $title,
//                 'slugs' => $slugs,
//                 'description' => $description,
//                 'body' => $body,
//                 /*'banner' => $banner,*/
//                 'category' => $category,
//                 'featuredoption' => $featuredoption,
//                 'featured' => $featured,
//                 'publish' => $d,
//                 'created_at'=>date("Y-m-d H:i:s"),
//                 'updated_at'=>date("Y-m-d H:i:s"),
//                 ));
//             if (!$page->save()) {
//                 $errors = array();
//                 foreach ($page->getMessages() as $message) {
//                     $errors[] = $message->getMessage();
//                 }
//                 $data[]=array('error' => $errors);
//             } else {
//               $newstags = array(); 
//               $newstags = $tags;
//               foreach($newstags as $newstags){
//                 $tagsofnews = new SelctTags();

//                 $tagsofnews->assign(array(
//                     'newsid' => $newsid,
//                     'tags' => $newstags['id']
//                     ));
//                 if (!$tagsofnews->save())  {
//                  $data[]=array('error' => "Error Saving");
//              } 
//              else 
//              {
//                $data[]=array('success' => "Tags Save");
//                 //START Log
//                $audit = new CB();
//                $audit->auditlog(array(
//                   "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
//                   "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
//                   "title" => "Add Center news- ".$title." ", /*// Maybe some info here (confuse) XD*/
//                   ));
//                 //END Audit Log
//            }
//        }



//        $data[]=array('success' => "New Page has been successfully Created!");


//    }



// }
// echo json_encode($data );

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $centerid       = $request->getPost('centerid');
            $title          = $request->getPost('title');
            $slugs          = $request->getPost('slugs');
            $author         = $request->getPost('author');
            $date           = $request->getPost('date');
            $description    = $request->getPost('summary');
            $body           = $request->getPost('body');
            $status         = $request->getPost('status');
            $featuredtype   = $request->getPost('featuredthumbtype');
            $featuredthumb  = $request->getPost('featuredthumb');
    //META
            $metadesc       = $request->getPost('metadesc');
            $metakeyword    = $request->getPost('metakeyword');
            $metatitle      = $request->getPost('metatitle');
    //ARRAY
            $tags           = $request->getPost('tag');
            $categ          = $request->getPost('category');
    // $featuredoption = $request->getPost('featuredoption');
    // $blogvideo      = $request->getPost('video');
    // $blogbanner     = $request->getPost('featbanner');
    // $featured = ($featuredoption=="video" ? $blogvideo : $blogbanner);
            $guid = new \Utilities\Guid\Guid();
            $_guid = new \Utilities\Guid\Guid();
            $_checkifexist = Centernews::find('newsid="'. $guid->GUID().'"');
            $newsid = (count($_checkifexist)!=0 ? $guid->GUID() : $_guid->GUID());
            $page = new Centernews();
            $page->assign(array(
                'newsid'        => $newsid,
                'centerid'      => $centerid,
                'authorid'      => $author,
                'title'         => $title,
                'slug'          => $slugs,
                'description'   => $description,
                'body'          => $body,
                /*$banner = $request->getPost('banner');*/
                'featuredtype'  => $featuredtype,
                'featured'      => $featuredthumb,
                'publish'       => $date,
                'status'        => $status,
                'metadesc'      => $metadesc,
                'metakeyword'   => $metakeyword,
                'metatitle'     => $metatitle,
                'created_at'    =>date("Y-m-d H:i:s"),
                'updated_at'    =>date("Y-m-d H:i:s")
                ));
            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            } else {
             $data[]=array('success' => "New Center has been successfully Created!");
        // START Log
        $audit = new CB();
        $audit->auditlog(array(
            "module" =>"Blog", //Examaple News, Create Center, Slider, Events etc...
            "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
            "title" => "Add Blog- ".$title." ", /*// Maybe some info here (confuse) XD*/
            ));
        //CATEG 
        foreach($categ as $cat){
            $catnews = new Selctcateg();
            $catnews->assign(array(
                'blogid' => $blogid,
                'categ' => $cat
                ));
            if (!$catnews->save()){
                $errors = array();
                foreach ($page->getMessages() as $message){
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                // $data['success'] = "Success cat";
            }
        }

        //TAGS
        foreach($tags as $tag){
            $gettags = Tags::findFirst("tags='".$tag."'");
            if(!$gettags){
                $newstags = new Tags();
                $newstags->assign(array(
                    'tags' => $tag,
                    'slugs' => str_replace("-", " ", $tag),
                    'created_at'    =>date("Y-m-d H:i:s"),
                    'updated_at'    =>date("Y-m-d H:i:s")
                    ));
                if (!$newstags->save())
                {
                    $data['error'] = "Something went wrong saving the newstags, please try again.";
                }
                else{
                    $chkif = SelctTags::findFirst("tags='".$tag."'");
                    if(!$chkif){
                        $tagnews = new Blogtags();
                        $tagnews->assign(array(
                            'blogid' => $blogid,
                            'tags' => $tag
                            ));
                        if (!$tagnews->save()){
                            $errors = array();
                            foreach ($tagnews->getMessages() as $message){
                                $errors[] = $message->getMessage();
                            }
                            echo json_encode(array('error' => $errors));
                        }else{
                            $data['success'] = "Success cat";
                        }
                    }
                }

            }else{
               $chkif = SelctTags::findFirst("tags='".$tag."'");
               if(!$chkif){
                $tagnews = new SelctTags();
                $tagnews->assign(array(
                    'blogid' => $blogid,
                    'tags' => $tag
                    ));
                if (!$tagnews->save()){
                    $errors = array();
                    foreach ($tagnews->getMessages() as $message){
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{
                    $data['success'] = "Success cat";
                }
            }
        }
    }
         }
     }
     echo json_encode($data);
 }


 public function manageNewsAction($num, $page, $keyword, $centerid) {

    if ($keyword == 'undefined' && $centerid == 'undefined'){
     $Pages = Centernews::find();
 } 
 elseif($keyword == 'undefined' && $centerid != 'undefined')
 {
    $Pages = Centernews::find(array("centerid = '".$centerid."'"));
}
else {
    if($keyword !='undefined' && $centerid !='undefined'){
        $conditions = "centerid = '".$centerid."' and title LIKE '%" . $keyword . "%' OR slugs LIKE '%" . $keyword . "%'";
        $Pages = Centernews::find(array($conditions));
    }else{
        $conditions = "title LIKE '%" . $keyword . "%' OR slugs LIKE '%" . $keyword . "%'";
        $Pages = Centernews::find(array($conditions));
    }

}

$currentPage = (int) ($page);


$paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
        "data" => $Pages,
        "limit" => 10,
        "page" => $currentPage
        )
    );
$page = $paginator->getPaginate();

$data = array();
foreach ($page->items as $m) {
   $getAuthor = Author::findFirst('id="'.$m->authorid.'"');
   $data[] = array(
    'newsid' => $m->newsid,
    'authorname'  => $getAuthor->name,
    'title' => $m->title,
    'slugs' => $m->slugs,
    'publish' => $m->publish
    );
}
$p = array();
for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}
public function newsDeleteAction($newsid) {

    $news = Centernews::findFirst('newsid="'. $newsid.'"');
    $title = $news->title;
    if ($news) {
        if ($news->delete()) {
            $data[]=array('success' => "News has been successfully deleted");
            //START Log
            $audit = new CB();
            $audit->auditlog(array(
              "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
              "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
              "title" => "Delete Center news- ".$title." ", /*// Maybe some info here (confuse) XD*/
              ));
            //END Audit Log
        }
    }else{
        $data[]=array('error' => "Seems the News Cant Be Deleted");
    }   
    echo json_encode($data);
}
public function newseditAction($newsid) {

    $tags = SelctTags::find('newsid="'. $newsid.'"');

    foreach ($tags as $value) {
        $seltags[]= array(
            "id"=>$value->tags,
            );
    }

    $center = Centernews::findFirst('newsid="'. $newsid.'"');
    $data=array(
        'title'         => $center->title,
        'slugs'       => $center->slugs,
        'description'        => $center->description,
        'body'      => $center->body ,
        /*'banner'         => $center->banner,*/
        'city'          => $center->city,
        'publish'       => $center->publish,
        'authorid'       => $center->authorid,
        'centerid'     =>$center->centerid,
        'featuredoption'  => $center->featuredoption,
        'feat'     =>$center->featured,
        'category' =>$center->category,
        'tag'=>$seltags
        );
    echo json_encode(array("data"=> $data));
}
public function centerupdatection() {
    $request = new \Phalcon\Http\Request();
    if($request->isPost()){
        $newsid = $request->getPost('newsid');
        $centerid = $request->getPost('centerid');
        $title = $request->getPost('title');
        $slugs = $request->getPost('slugs');
        $description = $request->getPost('description');
        $body = $request->getPost('body');
        /*$banner = $request->getPost('banner');*/
        $publish = $request->getPost('publish');
        $hiddendate = $request->getPost('hiddendate');
        $authorid = $request->getPost('authorid');
        $featuredoption = $request->getPost('featuredoption');
        $featured = $request->getPost('featured');
        $category = $request->getPost('category');
        $tags = $request->getPost('tag');

        if($featuredoption=="video"){
            $featured =  $request->getPost('video');
        }else{
            $featured =  $request->getPost('featbanner');
        }

        $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
        
        if(strlen($publish) <= 11){
           $d = $hiddendate;
       }else{
        $dates = explode(" ", $publish);
        $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
    }

    $page = Centernews::findFirst('newsid="'. $newsid.'"');
    $page->assign(array(
        'title' => $title,
        'slugs' => $slugs,
        'description' => $description,
        'body' => $body,
        /*'banner' => $banner,*/
        'publish' => $d,
        'authorid' => $authorid,
        'centerid' => $centerid,
        'category' => $category,
        'featuredoption' => $featuredoption,
        'featured' => $featured,
        'updated_at'=>date("Y-m-d H:i:s"),
        ));
    if (!$page->save()) {
        $errors = array();
        foreach ($page->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {

     $conditions = 'newsid="' . $newsid . '"';
     $deletenewstags = SelctTags::find(array($conditions));
     $data = array('error' => 'Not Found');
     if ($deletenewstags) {

        foreach($deletenewstags as $deletenewstags){

            if ($deletenewstags->delete()) {
                $data = array('success' => 'tags Deleted');
            }
        }
    }

    //////////////////////////
    $newstags = array(); 
    $newstags = $tags;
    foreach($newstags as $newstags){
        $tagsofnews = new SelctTags();

        $tagsofnews->assign(array(
            'newsid' => $newsid,
            'tags' => $newstags['id']
            ));
        if (!$tagsofnews->save())  {
           $data[]=array('error' => "Error Saving");
       } 
       else 
       {
         $data[]=array('success' => "Tags Save");
     }
 }
 $data[]=array('success' => "New Page has been successfully Created!");
        //START Log
 $audit = new CB();
 $audit->auditlog(array(
  "module" =>"Center News", /*//Examaple News, Create Center, Slider, Events etc...*/
  "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
  "title" => "Update Center news- ".$title." ", /*// Maybe some info here (confuse) XD*/
  ));
        //END Audit Log
}

}
echo json_encode($data);
}


public function feNewsAction($num, $page, $keyword, $centerid) {

    if ($keyword == 'null' || $keyword == 'undefined') {

     $Pages = Centernews::find(array("centerid = '".$centerid."'"));
 } else {
    $conditions = "centerid = '".$centerid."' and title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
    $Pages = Centernews::find(array($conditions));
}

$currentPage = (int) ($page);


$paginator = new \Phalcon\Paginator\Adapter\Model(
    array(
        "data" => $Pages,
        "limit" => 10,
        "page" => $currentPage
        )
    );
$page = $paginator->getPaginate();

$data = array();
foreach ($page->items as $m) {
    $getAuthor = Author::findFirst('id="'.$m->authorid.'"');
    $data[] = array(
        'newsid' => $m->newsid,
        'slug' => $m->slugs,
        'authorname'  => $getAuthor->name,
        'title' => $m->title,
        'description' => $m->description,
        'body' => $m->body,
        'publish' => $m->publish,
        'featuredoption' => $m->featuredoption,
        'featured' => $m->featured
        );
}
$p = array();
for ($x = 1; $x <= $page->total_pages; $x++) {
    $p[] = array('num' => $x, 'link' => 'page');
}
echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}
public function listauthorAction(){
   $listauthor = Author::find();
   foreach ($listauthor as $listauthor) 
   {
    $data[] = array(
        'id'=>$listauthor->id,
        'name'=>$listauthor->name
        );
}
echo json_encode($data);
}
public function listcenterAction(){
 $listcenter = Center::find("status='true'");
 foreach ($listcenter as $listcenter) 
 {

    $data[] = array(
        'centerid'=>$listcenter->centerid,
        'title'=>$listcenter->title
        );
}
echo json_encode($data);
}
public function readnewsAction($slugnews) {
    $center = Centernews::findFirst('slugs="'. $slugnews.'"');
    $seltags = SelctTags::find('newsid="'.$center->newsid.'"');
    foreach ($seltags as $m) {
        $tags = Tags::find('id="'. $m->tags.'"');
        if(count($tags)!=0){
            $data2[] = array(
                'tags' => $tags[0]->tags,
                'tagslug' => $tags[0]->slugs,
                );
        }
    }
    foreach ($seltags as $m) {
        $tags = Tags::find('id="'. $m->tags.'"');
        if(count($tags)!=0){
            $data3[] = array(
                'tags' => $tags[0]->tags,
                );
        }
    }

    $category = Category::find();
    foreach ($category as $data) {
        $dbcategory[] = array(
            "category"=> $data->categoryname,
            "slugs"=> $data->categoryslugs,
            );
    }


    $getAuthor = Author::findFirst('id="'. $center->authorid.'"');
    $categ = Category::findFirst('categoryid="'. $center->category.'"');
    $data=array(
        'author'      => $center->authorid,
        'authorname'  => $getAuthor->name,
        'authorbio'   => $getAuthor->about,
        'authorpic'   => $getAuthor->image,
        'title'       => $center->title,
        'slugs'       => $center->slugs,
        'description' => $center->description,
        'body'        => $center->body ,
        /*'banner'      => $center->banner,*/
        'city'          => $center->city,
        'publish'       => $center->publish,
        'authorid'       => $center->authorid,
        'centerid'     =>$center->centerid,
        'featuredoption'  => $center->featuredoption,
        'feat'     =>$center->featured,
        'category'     => $categ->categoryname,
        'categoryslugs'     => $categ->categoryslugs,
        );

    echo json_encode(array("data"=> $data, "tags"=> $data2 , "category"=> $dbcategory,"metatags"=>$data3));

}


















public function authorNewsAction($offset, $page,  $authorid) {
    $data = array();
    $centernews = Centernews::find(array("authorid = '".$authorid."'"));
    
    foreach($centernews as $m) {
        $data[] = array(
            'newsid' => $m->newsid,
            'slug' => $m->slugs,
            'title' => $m->title,
            'description' => $m->description,
            'body' => $m->body,
            'publish' => $m->publish,
            'feattype' => 'banner',
            'featured' => $m->banner,
            'type' => 'news',
            'link'=>'/../view/centernews/power-brain-training/find-center/'.$m->slugs
            );
    }

    $blog = Blog::find(array("authorid = '".$authorid."'"));
    foreach($blog as $m) {
        $data[] = array(
            'newsid' => $m->blogid,
            'slug' => $m->slug,
            'title' => $m->title,
            'description' => $m->description,
            'body' => $m->body,
            'publish' => $m->publish,
            'feattype' => $m->featuredtype,
            'featured' => $m->featured,
            'type' => 'blog',
            'link'=>'/../blog/read/'.$m->slug
            );
    }


    echo json_encode(array_slice($data, $offset, $page));
}






















public function addembedAction() {
   $request = new \Phalcon\Http\Request();
   if($request->isPost()){
    $centerid = $request->getPost('centerid');
    $embed = $request->getPost('video');

    //EXPLOSION PAPUTOK
    $byspace = explode(" ", $embed);
    $byslash = explode("/", $byspace[3]);
    $utubevidid = trim(str_replace('"',' ', $byslash[4]));
    //EXPLOSION PAPUTOK

    $guid = new \Utilities\Guid\Guid();
    $guid->GUID();
    $saveembed = new Featuredvideo();
    $saveembed->assign(array(
        'vidid' => $guid->GUID(),
        'centerid' => $centerid,
        'embedid' => $utubevidid,
        'embed' => $embed
        ));
    if (!$saveembed->save()) {
        $errors = array();
        foreach ($saveembed->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $data[]=array('error' => $errors);
    } else {
        $data[]=array('success' => "New Page has been successfully Created!");
    }


}
echo json_encode($data);
}
public function featvideoAction($centerid) {
    $getimages = Featuredvideo::find(array("centerid ='".$centerid."'"));
    if(count($getimages) == 0){
        $data['error']=array('NOIMAGE');
    }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'vidid'=>$getimages->vidid,
                'embedid'=>$getimages->embedid,
                'embed'=>$getimages->embed
                );
        }
    }
    echo json_encode($data);
}
public function deletevideoAction($vidid) {
    $vid = Featuredvideo::findFirst('vidid="'. $vidid.'"');
    if ($vid) {
        if ($vid->delete()) {
            $data[]=array('success' => "");
        }else{
            $data[]=array('error' => '');
        }
    }else{
        $data[]=array('error' => '');
    }
    echo json_encode($data );
}
}